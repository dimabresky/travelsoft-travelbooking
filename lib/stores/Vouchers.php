<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей путевок
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Vouchers extends Highloadblock {

    protected static $storeName = 'vouchers';

    public static function get(array $query = array(), bool $likeArray = true, callable $callback = null) {

        $converter = new \travelsoft\booking\adapters\CurrencyConverter;
        return parent::get($query, $likeArray, function (&$arVoucher) use ($callback, $converter) {

                    if ($callback) {
                        $callback($arVoucher);
                    }

                    $arVoucher["TOTAL_COST"] = $arVoucher["TOTAL_COST_FORMATTED"] = $arVoucher["COST"] = $arVoucher["COST_FORMATTED"] = $arVoucher["TS_COST"] = $arVoucher["TS_COST_FORMATTED"] = $arVoucher["DISCOUNT"] = $arVoucher["DISCOUNT_FORMATTED"] = $arVoucher["PAID"] = $arVoucher["PAID_FORMATTED"] = $arVoucher["TO_PAY"] = $arVoucher["TO_PAY_FORMATTED"] = 0.00;
                    $arVoucher["BOOKINGS_DETAIL"] = array();
                    foreach ($arVoucher["UF_BOOKINGS"] as $booking_id) {
                        $arVoucher["BOOKINGS_DETAIL"][$booking_id] = current(Bookings::get(array("filter" => array("ID" => $booking_id))));
                        if (!empty($arVoucher["BOOKINGS_DETAIL"][$booking_id])) {
                            $arVoucher["COST"] += $arVoucher["BOOKINGS_DETAIL"][$booking_id]["COST_AC"];
                            $arVoucher["TOTAL_COST_WITHOUT_DISCOUNT"] += $arVoucher["BOOKINGS_DETAIL"][$booking_id]["TOTAL_COST_WITHOUT_DISCOUNT_AC"];
                            $arVoucher["TOTAL_COST"] += $arVoucher["BOOKINGS_DETAIL"][$booking_id]["TOTAL_COST_AC"];
                            $arVoucher["TS_COST"] += $arVoucher["BOOKINGS_DETAIL"][$booking_id]["TS_COST_AC"];
                            $arVoucher["DISCOUNT"] += $arVoucher["BOOKINGS_DETAIL"][$booking_id]["DISCOUNT_AC"];
                        }
                    }

                    $AC = \travelsoft\booking\Settings::getAccountingCurrency();
                    $arVoucher["CURRENCY"] = $AC;
                    if ($arVoucher["TOTAL_COST_WITHOUT_DISCOUNT"] > 0) {
                        $arVoucher["TOTAL_COST_WITHOUT_DISCOUNT_FORMATTED"] = $converter->convertWithFormatting($arVoucher["TOTAL_COST_WITHOUT_DISCOUNT"], $AC, $AC);
                    }
                    if ($arVoucher["TOTAL_COST"] > 0) {
                        $arVoucher["TOTAL_COST_FORMATTED"] = $converter->convertWithFormatting($arVoucher["TOTAL_COST"], $AC, $AC);
                    }
                    if ($arVoucher["COST"] > 0) {
                        $arVoucher["COST_FORMATTED"] = $converter->convertWithFormatting($arVoucher["COST"], $AC, $AC);
                    }
                    if ($arVoucher["TS_COST"] > 0) {
                        $arVoucher["TS_COST_FORMATTED"] = $converter->convertWithFormatting($arVoucher["TS_COST"], $AC, $AC);
                    }
                    if ($arVoucher["DISCOUNT"] > 0) {
                        $arVoucher["DISCOUNT_FORMATTED"] = $converter->convertWithFormatting($arVoucher["DISCOUNT"], $AC, $AC);
                    }

                    $arr_payment_history = current(PaymentHistory::get(array(
                                "filter" => array("UF_ORDER_ID" => $arVoucher["ID"]),
                                "select" => array("ID", "UF_PRICE", "UF_CURRENCY", "UF_COURSE_INFO")
                    )));

                    if (isset($arr_payment_history["ID"]) && $arr_payment_history["ID"] > 0) {
                        $arVoucher["PAID"] = $arr_payment_history["CONVERTER"]->convert((float) $arr_payment_history["UF_PRICE"], (string) $arr_payment_history["UF_CURRENCY"], $AC);
                    }

                    if ($arVoucher["PAID"] > 0) {
                        $arVoucher["PAID_FORMATTED"] = $converter->convertWithFormatting($arVoucher["PAID"], $AC, $AC);
                    }

                    // данные для вывода истори платежей на стринице редактирования путевки в CRM
                    $arVoucher["PAYMENT_HISTORY"]["TS_PAID_BYN_FORMATTED"] = "0.00 BYN";
                    $arVoucher["PAYMENT_HISTORY"]["TS_TO_PAY_BYN_FORMATTED"] = "0.00 BYN";
                    if ($arVoucher["TS_COST"] > 0) {
                        $arVoucher["PAYMENT_HISTORY"]["TS_BYN_FORMATTED"] = $converter->convertWithFormatting($arVoucher["TS_COST"], $AC, "BYN");
                    } else {
                        $arVoucher["PAYMENT_HISTORY"]["TS_BYN_FORMATTED"] = "0.00 BYN";
                    }
                    $arVoucher["COST_WITHOUT_TS"] = $arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS"] = $arVoucher["TOTAL_COST"] - $arVoucher["TS_COST"];
                    if ($arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                        $arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS_FORMATTED"] = $converter->convertWithFormatting($arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS"], $AC, $AC);
                    } else {
                        $arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS_FORMATTED"] = "0.00 " . $AC;
                    }
                    $arVoucher["PAYMENT_HISTORY"]["COST_TO_PAY_FORMATTED"] = $arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS_FORMATTED"];
                    $arVoucher["PAYMENT_HISTORY"]["COST_PAID_FORMATTED"] = "0.00 " . $AC;
                    if ($arVoucher["PAID"] > \travelsoft\booking\Settings::FLOAT_NULL) {

                        $ts_to_pay = $arVoucher["PAID"] - $arVoucher["TS_COST"];
                        if ($ts_to_pay >= \travelsoft\booking\Settings::FLOAT_NULL) {
                            if ($arVoucher["TS_COST"] > 0) {
                                $arVoucher["PAYMENT_HISTORY"]["TS_PAID_BYN_FORMATTED"] = $converter->convertWithFormatting($arVoucher["TS_COST"], $AC, "BYN");
                            } else {
                                $arVoucher["PAYMENT_HISTORY"]["TS_PAID_BYN_FORMATTED"] = "0.00 BYN";
                            }

                            $arVoucher["PAYMENT_HISTORY"]["TS_TO_PAY_BYN_FORMATTED"] = "0.00 BYN";
                            $cost_to_pay = $arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS"] - $ts_to_pay;
                            if ($cost_to_pay >= 0) {
                                $arVoucher["PAYMENT_HISTORY"]["COST_TO_PAY_FORMATTED"] = $converter->convertWithFormatting($cost_to_pay, $AC, $AC);
                                $arVoucher["PAYMENT_HISTORY"]["COST_PAID_FORMATTED"] = $converter->convertWithFormatting($ts_to_pay, $AC, $AC);
                            } else {
                                $arVoucher["PAYMENT_HISTORY"]["COST_TO_PAY_FORMATTED"] = "0.00 " . $AC;
                                if ($arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $arVoucher["PAYMENT_HISTORY"]["COST_PAID_FORMATTED"] = $converter->convertWithFormatting($arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS"], $AC, $AC);
                                } else {
                                    $arVoucher["PAYMENT_HISTORY"]["COST_PAID_FORMATTED"] = "0.00 " . $AC;
                                }
                            }
                        } else {
                            $arVoucher["PAYMENT_HISTORY"]["TS_TO_PAY_BYN_FORMATTED"] = $converter->convertWithFormatting(abs($ts_to_pay), $AC, "BYN");
                            $arVoucher["PAYMENT_HISTORY"]["TS_PAID_BYN_FORMATTED"] = $converter->convertWithFormatting($arVoucher["PAID"], $AC, "BYN");
                            if ($arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                                $arVoucher["PAYMENT_HISTORY"]["COST_TO_PAY_FORMATTED"] = $converter->convertWithFormatting($arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS"], $AC, $AC);
                            } else {
                                $arVoucher["PAYMENT_HISTORY"]["COST_TO_PAY_FORMATTED"] = "0.00 " . $AC;
                            }
                            $arVoucher["PAYMENT_HISTORY"]["COST_PAID_FORMATTED"] = "0.00 BYN";
                        }
                    }

                    $arVoucher["TO_PAY"] = $arVoucher["TOTAL_COST"] - $arVoucher["PAID"];
                    if ($arVoucher["TO_PAY"] > 0) {
                        $arVoucher["TO_PAY_FORMATTED"] = $converter->convertWithFormatting($arVoucher["TO_PAY"], $AC, $AC);
                    }
                });
    }

}
