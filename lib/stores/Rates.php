<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей тарифы
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Rates extends Highloadblock {

    protected static $storeName = 'rates';
    
    /**
     * Название по id
     * @param int $id
     * @return string
     */
    public static function nameById (int $id) : string {
        
        return (string)current(self::get(array("filter" => array("ID" => $id), "select" => array("ID", "UF_NAME"))))["UF_NAME"];
        
    }
}
