<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Iblock;

/**
 * Класс для работы с таблицей описания экскурсионных туров
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class ExcursionTourIB extends Iblock{

    protected static $storeName = 'excursionTourIB';
}
