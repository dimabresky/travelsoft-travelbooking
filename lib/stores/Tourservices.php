<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей туруслуги
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Tourservices extends Highloadblock {

    protected static $storeName = 'tourservices';
}