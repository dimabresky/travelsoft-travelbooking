<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей квот для проезда в качестве трансфера
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class TransferQuotas extends Highloadblock {

    protected static $storeName = 'transferQuotas';

}
