<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace travelsoft\booking\stores;

/**
 * Класс для работы с таблицей размещений
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft 
 */
class Placements extends \travelsoft\booking\adapters\Iblock{
    
    static public $storeName = "placements";

}
