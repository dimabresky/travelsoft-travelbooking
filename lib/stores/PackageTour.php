<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей пакетных туров
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class PackageTour extends Highloadblock {

    protected static $storeName = 'packageTour';
    
    public static function get(array $query = array(), bool $likeArray = true, callable $callback = null) {
        
        
            $callback_computed = function (&$arItem) {
                
                $arItem["PLACEMENT_NAME"] = "";
                $arItem["TRAVEL_NAME"] = "";
                $arItem["NAME"] = "";
                $ARR_NAME = array();
                if ($arItem["UF_TRAVEL"] > 0) {
                    $ARR_NAME[] = $arItem["TRAVEL_NAME"] = current(Travel::get(array(
                        "filter" => array("ID" => $arItem["UF_TRAVEL"]),
                        "select" => array("ID", "NAME")
                    )))["NAME"];
                }
                if ($arItem["UF_PLACEMENT"] > 0) {
                    $ARR_NAME[] = $arItem["PLACEMENT_NAME"] = current(Placements::get(array(
                        "filter" => array("ID" => $arItem["UF_PLACEMENT"]),
                        "select" => array("ID", "NAME")
                    )))["NAME"];
                }
                
                $arItem["NAME"] = implode(" + ", $ARR_NAME);
                
            };
        
        return parent::get($query, $likeArray, function (&$arItem) use ($callback, $callback_computed) {
            if ($callback) {
                $callback($arItem);
            }
            $callback_computed($arItem);
        });
    }
    
}
