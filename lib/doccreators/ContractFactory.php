<?php

namespace travelsoft\booking\doccreators;

/**
 * Фабрика для генерации договора с клиентом
 *
 * @author dimabresky
 */
class ContractFactory {

    /**
     * @param array $arr_voucher
     * @param string $path
     * @param string $outputFormat
     */
    public function create(array $arr_voucher, string $path, string $outputFormat = 'docx') {

        $template = \travelsoft\booking\doccreators\TemplateProccessorFactory::create($_SERVER['DOCUMENT_ROOT'] . $path);

        $template->setValue("DATE", date('d.m.Y'));
        $template->setValue("VOUCHER_ID", $arr_voucher["ID"]);

        $tourists_data = "";
        if (!empty($arr_voucher["TOURISTS"])) {

            $cnt = 1;
            $delemiter_str = $outputFormat === "docx" ? "<w:br/>" : "<br>";
            foreach ($arr_voucher["TOURISTS"] as $arr_tourist) {

                $tourists_data .= $cnt . ". " . $arr_tourist["FULL_NAME"] . ", паспорт " . $arr_tourist["SN_PASSPORT"] . ", дата рождения: " . \travelsoft\booking\Utils::dateConvert($arr_tourist["UF_BIRTHDATE"]->toString()) . $delemiter_str;

                $cnt++;
            }
        }

        if (strlen($tourists_data) > 0) {
            $template->setValue("TOURISTS_COUNT", count($arr_voucher["TOURISTS"]));
            $template->setValue("TOURISTS", $tourists_data);
        }

        if ($arr_voucher["UF_CLIENT"] > 0) {

            $arr_user = current(\travelsoft\booking\stores\Users::get(array(
                        "filter" => array("ID" => $arr_voucher["UF_CLIENT"]),
                        "select" => array("ID", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_PHONE", "UF_PASS_SERIES", "UF_PASS_NUMBER", "UF_PASS_ISSUED_BY", "UF_PASS_DATE_ISSUE", "PERSONAL_BIRTHDAY")
            )));

            if ($arr_user["ID"] > 0) {
                if (strlen($arr_user["PERSONAL_PHONE"]) > 0) {
                    $template->setValue("PHONE", $arr_user["PERSONAL_PHONE"]);
                }
                if (strlen($arr_user["FULL_NAME"]) > 0) {
                    $template->setValue("CLIENT_NAME", $arr_user["FULL_NAME"]);
                }
                if (strlen($arr_user["SHORT_NAME"]) > 0) {
                    $template->setValue("SHORT_CLIENT_NAME", $arr_user["SHORT_NAME"]);
                }
                if (strlen($arr_user["PASSPORT"]) > 0) {
                    $template->setValue("PASSPORT", $arr_user["PASSPORT"]);
                }
                if (strlen($arr_user["UF_PASS_ISSUED_BY"]) > 0) {
                    $template->setValue("PASSPORT_ISSUED_BY", $arr_user["UF_PASS_ISSUED_BY"]);
                }
                if (strlen($arr_user["UF_PASS_DATE_ISSUE"]) > 0) {
                    $template->setValue("PASSPORT_DATE_ISSUE", $arr_user["UF_PASS_DATE_ISSUE"]);
                }
                if (strlen($arr_user["PERSONAL_BIRTHDAY"]) > 0) {
                    $template->setValue("BIRTHDATE", $arr_user["PERSONAL_BIRTHDAY"]);
                }
            }
        }

        if ($arr_voucher["UF_MANAGER"] > 0) {

            $arr_user = current(\travelsoft\booking\stores\Users::get(array(
                        "filter" => array("ID" => $arr_voucher["UF_MANAGER"]),
                        "select" => array("ID", "NAME", "SECOND_NAME", "LAST_NAME", "UF_ATT_NUMBER")
            )));

            if ($arr_user["ID"] > 0) {

                if (strlen($arr_user["FULL_NAME"]) > 0) {
                    $template->setValue("MANAGER", $arr_user["FULL_NAME"]);
                }
                if (strlen($arr_user["SHORT_NAME"]) > 0) {
                    $template->setValue("SHORT_MANAGER_NAME", $arr_user["SHORT_NAME"]);
                }
                if (strlen($arr_user["UF_ATT_NUMBER"]) > 0) {
                    $template->setValue("MANAGER_ATT_NUMBER", $arr_user["UF_ATT_NUMBER"]);
                }
            }
        }

        if (!empty($arr_voucher["BOOKINGS_DETAIL"])) {
            $service = current($arr_voucher["BOOKINGS_DETAIL"]);

            /*if (\travelsoft\booking\crm\Validator::isTravelServiceType($service["UF_SERVICE_TYPE"])) {

                $template->setValue("SERVICE_NAME", \travelsoft\booking\Utils::getTravelgroupNameByTravelIdAndTravelType((int) $service["UF_TRAVEL"], $service["UF_SERVICE_TYPE"]));
            } else*/
                
                if (strlen($service["UF_SERVICE_NAME"]) > 0) {
                $template->setValue("SERVICE_NAME", $service["UF_SERVICE_NAME"]);
            }

            if (isset($service["UF_DATE_FROM"])) {
                $template->setValue("DATE_FROM", \travelsoft\booking\Utils::dateConvert($service["UF_DATE_FROM"]->toString()));
            }
            if (isset($service["UF_DATE_TO"])) {
                $template->setValue("DATE_TO", \travelsoft\booking\Utils::dateConvert($service["UF_DATE_TO"]->toString()));
            }
            if (isset($service["UF_DATE_TO_PLACE"])) {
                $template->setValue("DATE_CHECKOUT", \travelsoft\booking\Utils::dateConvert($service["UF_DATE_TO_PLACE"]->toString()));
            }
            if (strlen($service["UF_FOOD"]) > 0) {
                $template->setValue("FOOD_NAME", $service["UF_FOOD"]);
            } else {
                $template->setValue("FOOD_NAME", "Не входит в стоимость");
            }
            if (isset($service["UF_TRAVEL"])) {
                $travel = current(\travelsoft\booking\stores\Travel::get(
                                array("filter" => array("ID" => $service["UF_TRAVEL"]), "select" => array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*"))));

                if (strlen($travel["NAME"]) > 0) {
                    $template->setValue("TRANSFER_NAME", $travel["NAME"]);
                }
                if (strlen($travel["PROPERTIES"]["ROUTE"]["VALUE"]) > 0) {
                    $template->setValue("ROUTE", $travel["NAME"]);
                }
            } else {

                $template->setValue("TRANSFER_NAME", "-");
                $template->setValue("ROUTE", "-");
            }

            if (isset($service["UF_ROOM"])) {

                if (strlen($room_name = current(\travelsoft\booking\stores\Rooms::get(
                                                array("filter" => array("ID" => $service["UF_ROOM"]), "select" => array("ID", "UF_NAME"))))["UF_NAME"]) > 0) {
                    $template->setValue("ROOM_NAME", $room_name);
                }
            }
        }

        $converter = new \travelsoft\booking\adapters\CurrencyConverter;
        if ($arr_voucher["TS_COST"] > \travelsoft\booking\Settings::FLOAT_NULL) {
            $template->setValue("BYN_COST", $converter->format($converter->convert($arr_voucher["TS_COST"], $arr_voucher["CURRENCY"], "BYN")));
        } else {
            $template->setValue("BYN_COST", "0.00");
        }

        $template->setValue("USD_COST", $converter->format($converter->convert($arr_voucher["COST_WITHOUT_TS"], $arr_voucher["CURRENCY"], "USD")));
        $template->setValue("TOTAL_BYN_COST", $converter->format($converter->convert($arr_voucher["COST_WITHOUT_TS"], $arr_voucher["CURRENCY"], "BYN")));
        $template->stream('contract_' . $arr_voucher['ID'] . '.' . $outputFormat);
    }

}
