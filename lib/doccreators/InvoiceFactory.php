<?php

namespace travelsoft\booking\doccreators;

/**
 * Фабрика для генерации счета на оплату
 *
 * @author dimabresky
 */
class InvoiceFactory {

    /**
     * @param array $arr_voucher
     * @param string $path
     * @param string $outputFormat
     */
    public function create(array $arr_voucher, string $path, string $outputFormat = 'docx') {

        $content = \file_get_contents($_SERVER['DOCUMENT_ROOT'] . $path);

        $tourists_table_data = "";
        if (!empty($arr_voucher["TOURISTS"])) {

            $cnt = 1;
            foreach ($arr_voucher["TOURISTS"] as $arr_tourist) {

                $tourists_table_data .= "<tr>";
                $tourists_table_data .= "<td>$cnt</td>";
                $tourists_table_data .= "<td>" . $arr_tourist["FULL_NAME"] . "</td>";
                if ($arr_tourist["UF_MALE"] == "Мужской") {
                    $tourists_table_data .= "<td>М</td>";
                } else {
                    $tourists_table_data .= "<td>Ж</td>";
                }
                $tourists_table_data .= "<td>" . $arr_tourist["SN_PASSPORT"] . "</td>";
                $tourists_table_data .= "<td>" . $arr_tourist["UF_BIRTHDATE"]->toString() . "</td>";
                $tourists_table_data .= "<td>-</td>";
                $tourists_table_data .= "<td>-</td>";
                $tourists_table_data .= "</tr>";
                $cnt++;
            }
        }

        $tmp_file = $_SERVER['DOCUMENT_ROOT'] . "/upload/docs/tmp_.html";
        \file_put_contents($tmp_file, str_replace("\${TOURISTS}", $tourists_table_data, $content));

        $template = \travelsoft\booking\doccreators\TemplateProccessorFactory::create($tmp_file);

        unlink($tmp_file);

        $template->setValue("DATE", date('d.m.Y'));
        $template->setValue("VOUCHER_ID", $arr_voucher["ID"]);
        $template->setValue("YEAR", date("Y"));

        $legal_name = "";
        $legal_address = "";
        $phone = "";
        if ($arr_voucher["UF_CLIENT"] > 0) {

            $arr_user = current(\travelsoft\booking\stores\Users::get(array(
                        "filter" => array("ID" => $arr_voucher["UF_CLIENT"]),
                        "select" => array("ID", "NAME", "SECOND_NAME", "LAST_NAME", "UF_LEGAL_NAME",
                            "UF_LEGAL_ADDRESS", "PERSONAL_PHONE")
            )));

            if ($arr_user["ID"] > 0) {

                if (\travelsoft\booking\adapters\User::isAgentById($arr_user["ID"])) {
                    $legal_name = (string) $arr_user["UF_LEGAL_NAME"];
                    $legal_address = (string) $arr_user["UF_LEGAL_ADDRESS"];
                    $phone = (string) $arr_user["PERSONAL_PHONE"];
                } else {
                    $legal_name = $arr_user["FULL_NAME"];
                    $phone = $arr_user["PERSONAL_PHONE"];
                }
            }
        }

        $template->setValue("LEGAL_NAME", $legal_name);
        $template->setValue("LEGAL_ADDRESS", $legal_address);
        $template->setValue("PHONE", $phone);

        $names = "";
        if (!empty($arr_voucher["BOOKINGS_DETAIL"])) {
            $arr_names = array();
            foreach ($arr_voucher["BOOKINGS_DETAIL"] as $arr_booking) {
                $arr_names[] = $arr_booking["UF_SERVICE_NAME"];
            }
            $names = implode(", ", $arr_names);
        }

        $template->setValue("NAME", $names);

        $pure_cost = $pure_cost_discount = $cost = $ts_pure_cost = $ts_pure_cost_discount = $ts_cost = $total_pure_cost = $total_pure_cost_discount = $total_cost = 0.00;

        $converter = new \travelsoft\booking\adapters\CurrencyConverter();

        if ($arr_voucher["DISCOUNT"]) {

            $pure_cost = round($converter->convert($arr_voucher["COST"], $arr_voucher["CURRENCY"], "BYN"), 2);
            $pure_cost_discount = round($pure_cost * 0.1, 2);
            $cost = round($pure_cost - $pure_cost_discount, 2);

            $total_pure_cost = round($converter->convert($arr_voucher["TOTAL_COST_WITHOUT_DISCOUNT"], $arr_voucher["CURRENCY"], "BYN"), 2);
            $total_pure_cost_discount = round($total_pure_cost * 0.1, 2);
            $total_cost = round($total_pure_cost - $total_pure_cost_discount, 2);
        } else {

            $pure_cost = $cost = round($converter->convert($arr_voucher["COST"], $arr_voucher["CURRENCY"], "BYN"), 2);
            $total_pure_cost = $total_cost = round($converter->convert($arr_voucher["TOTAL_COST_WITHOUT_DISCOUNT"], $arr_voucher["CURRENCY"], "BYN"), 2);
        }

        if ($arr_voucher["TS_COST"]) {

            $ts_pure_cost = round($converter->convert($arr_voucher["TS_COST"], $arr_voucher["CURRENCY"], "BYN"), 2);
            $ts_pure_cost_discount = round($ts_pure_cost * 0.1, 2);
            $ts_cost = round($ts_pure_cost - $ts_pure_cost_discount, 2);
        }

        $template->setValue("PURE_COST", $pure_cost);
        $template->setValue("PURE_COST_DISCOUNT", $pure_cost_discount);
        $template->setValue("COST", $cost);

        $template->setValue("TS_PURE_COST", $ts_pure_cost);
        $template->setValue("TS_PURE_COST_DISCOUNT", $ts_pure_cost_discount);
        $template->setValue("TS_COST", $ts_cost);

        $template->setValue("TOTAL_PURE_COST", $total_pure_cost);
        $template->setValue("TOTAL_PURE_COST_DISCOUNT", $total_pure_cost_discount);
        $template->setValue("TOTAL_COST", $total_cost);

        $template->stream('invoice_' . $arr_voucher['ID'] . '.' . $outputFormat);
    }

}
