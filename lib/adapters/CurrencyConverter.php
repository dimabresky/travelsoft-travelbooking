<?php

namespace travelsoft\booking\adapters;

/**
 * Класс адаптер для валюты
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class CurrencyConverter {

    /**
     * @var \travelsoft\currency\Converter
     */
    protected $_converter;

    /**
     * @param int $courseId
     * @param array $commissions
     * @throws \Exception
     */
    public function __construct(int $courseId = null, array $commissions = null) {

        $this->includeDependency();
        
        if ($courseId > 0 || !empty($commissions)) {
            $currency = \travelsoft\currency\factory\Currency::getInstance(null, $courseId, $commissions);
            $cuContainer = \travelsoft\currency\factory\CuContainer::getInstance($currency);
            $this->_converter = \travelsoft\currency\factory\Converter::getInstance($cuContainer);
        } else {
            $this->_converter = \travelsoft\currency\factory\Converter::getInstance();
        }
    }

    public static function includeDependency() {
        if (!\Bitrix\Main\Loader::includeModule('travelsoft.currency')) {

            throw new \Exception(get_called_class() . ': Module travelsoft.currency not found');
        }
    }

    /**
     * Конвертирует цену в текущую валюту сайта
     * @param float $price
     * @param string $iso
     * @param string $isoOut
     * @return float
     */
    public function convert(float $price, string $iso, string $isoOut = null): float {

        if ($isoOut) {

            $result = $this->_converter->convert($price, $iso, $isoOut)->getResultLikeArray();
        } else {

            $result = $this->_converter->convert($price, $iso)->getResultLikeArray();
        }

        return $result['price'];
    }

    /**
     * Возвращает отформатированную цену
     * @param float $price
     * @param string $iso
     * @return string
     */
    public function convertWithFormatting(float $price, string $iso, string $isoOut = null): string {

        if ($isoOut) {

            return $this->_converter->convert($price, $iso, $isoOut)->getResult();
        } else {

            return $this->_converter->convert($price, $iso)->getResult();
        }
    }
    
    /**
     * @param float $value
     * @return string
     */
    public function format($value): string {
        return (string)number_format(
                        $value, \travelsoft\currency\Settings::formatDecimal(), \travelsoft\currency\Settings::formatDecPoint(), \travelsoft\currency\Settings::formatSSep() ? " " : ""
        );
    }

    /**
     * Возвращает список валют
     * @return array
     */
    public static function getAcceptableISO() {
        self::includeDependency();
        $currencies = \travelsoft\currency\stores\Currencies::getAcceptableISO(array("filter" => array("UF_ACTIVE" => 1)));
        $res_currencies = array();
        foreach ($currencies as $currency) {
            $res_currencies[$currency["UF_ISO"]] = array("ISO" => $currency["UF_ISO"], "TITLE" => $currency["UF_ISO"]);
        }


        return $res_currencies;
    }
    
    /**
     * @return int
     */
    public static function getCurrentCourseId () {
        return \travelsoft\currency\Settings::currentCourseId();
    }
    
    /**
     * @return array
     */
    public static function getCommissions () {
        return \travelsoft\currency\Settings::commissions();
    }
}
