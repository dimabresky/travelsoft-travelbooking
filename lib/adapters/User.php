<?php

namespace travelsoft\booking\adapters;

/**
 * User entity adapter
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class User {
    
    public static function checkPasswordAgainstPolicy (string $password, array $groups = array()) {
        global $USER;
        return $USER->CheckPasswordAgainstPolicy($password, $USER->GetGroupPolicy($groups));
    }
    
    /**
     * Return current user id
     * @global \CUser $USER
     * @return int
     */
    static public function id() : int{
        
        global $USER;
        
        return (int)$USER->GetID();
        
    }
    
    /**
     * Return array of groups id for current user
     * @global \CUser $USER
     * @return array
     */
    static public function groups() : array {
        
        global $USER;
        
        return $USER->GetUserGroupArray();
    }
    
    /**
     * Return array of groups id by user id
     * @global \CUser $USER
     * @param $user_id
     * @return array
     */
    static public function groupsById(int $user_id) : array {
        
        global $USER;
        
        return $USER->GetUserGroup($user_id);
    }
    
    /**
     * Проверка текущего пользователя на принадлежность к группе агент
     * @return bool
     */
    static public function haveAgentGroup () : bool {
        return in_array(\travelsoft\booking\Settings::agentsUGroup(), self::groups());
    }
    
    /**
     * Check agent user
     * @return boolean
     */
    static public function isAgent () : bool {
        
        if (self::haveAgentGroup()) {
            $arr_user = \travelsoft\booking\stores\Users::getById(self::id(), array("ID", "UF_AGENT_WAITING"));
            if ($arr_user["ID"] > 0) {
                return 0 === intVal($arr_user["UF_AGENT_WAITING"]);
            }
        }
        return false;
        
    }
    
    /**
     * Проверка пользователя на принадлежность к группе агент по id
     * @param int $user_id
     * @return bool
     */
    static public function haveAgentGroupById (int $user_id) : bool {
        return in_array(\travelsoft\booking\Settings::agentsUGroup(), self::groupsById($user_id));
    }
    
    /**
     * Check agent user by id
     * @param int $user_id
     * @return boolean
     */
    static public function isAgentById (int $user_id) : bool {
        
        if (self::haveAgentGroupById($user_id)) {
            $arr_user = \travelsoft\booking\stores\Users::getById($user_id, array("ID", "UF_AGENT_WAITING"));
            if ($arr_user["ID"] > 0) {
                return 0 === intVal($arr_user["UF_AGENT_WAITING"]);
            }
        }
        return false;
    
    }
    
    /**
     * Check admin user
     * @return boolean
     */
    static public function isAdmin () : bool {
        
        global $USER;
        return $USER->IsAdmin();
    
    }
    
    /**
     * Return current user phone
     * @return string
     */
    static public function phone () : string {
        return (string)current(\travelsoft\booking\stores\Users::get(array(
            "filter" => array("ID" => self::id()),
            "select" => array("PERSONAL_PHONE")
        )))["PERSONAL_PHONE"];
    }
    
    /**
     * Return user phone by id
     * @param int id
     * @return string
     */
    static public function phoneById (int $id) : string {
        return (string)current(\travelsoft\booking\stores\Users::get(array(
            "filter" => array("ID" => $id),
            "select" => array("PERSONAL_PHONE")
        )))["PERSONAL_PHONE"];
    }
    
    /**
     * Return user email by id
     * @param int id
     * @return string
     */
    static public function emailById (int $id) : string {
        return (string)current(\travelsoft\booking\stores\Users::get(array(
            "filter" => array("ID" => $id),
            "select" => array("EMAIL")
        )))["EMAIL"];
    }
    
    /**
     * Return current user email
     * @param int id
     * @return string
     */
    static public function email () : string {
        global $USER;
        return (string)$USER->GetEmail();
    }
    
    /**
     * Проверка существования пользователя по email
     * @param string $email
     * @return boolean
     */
    static public function existsByEmail (string $email) {
        $arr_user = current(\travelsoft\booking\stores\Users::get(array("filter" => array("EMAIL" => $email), "select" => array("ID"))));
        if (isset($arr_user["ID"]) && $arr_user["ID"] > 0) {
            return true;
        }
        return  false;
    }
    
    /**
     * Отсылка информации о регистрации пользователю
     * @global \CUser $USER
     * @param int $user_id
     */
    static public function sendUserInfo (int $user_id) {
        global $USER;
        $site_id = defined("SITE_ID") ? SITE_ID : "s1";
        $USER->SendUserInfo($user_id, $site_id, "Вы зарегистрированы в системе как клиент");
    }
    
}
