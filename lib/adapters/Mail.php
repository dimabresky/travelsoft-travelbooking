<?php

namespace travelsoft\booking\adapters;

use travelsoft\booking\Settings;

/**
 *  Класс адаптер отправки почтовых уведомлений
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Mail {

    protected static $_commonMailParameters = array(
        "EVENT_NAME" => Settings::MAIL_EVENT_TYPE,
        "LID" => "s1",
        "DUPLICATE" => "N"
    );

    /**
     * Отправка почтового уведомления клиенту при создании бронировки
     * @param array $fields
     */
    public static function sendNewBookingNotificationForClient(array $fields) {

        \Bitrix\Main\Mail\Event::send(array_merge(array(
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => Settings::clientBookingMailId()
                        ), self::$_commonMailParameters));
    }

    /**
     * Отправка почтового уведомления менеджеру при создании бронировки
     * @param array $fields
     */
    public static function sendNewBookingNotificationForManager(array $fields) {

        \Bitrix\Main\Mail\Event::send(array_merge(array(
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => Settings::managerBookingMailId()
                        ), self::$_commonMailParameters));
    }
    
    /**
     * Отправка письма пользователю, что он зарегистрирован как агент
     * @param array $fields
     */
    public static function sendAgentRegisterNotification(array $fields) {
        
        \Bitrix\Main\Mail\Event::send(array_merge(array(
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => Settings::agentRegisterMailId()
                        ), self::$_commonMailParameters));
    }
    
    /**
     * Отправка письма менеджеру, что пользователь зарегистрирован как агент
     * @param array $fields
     */
    public static function sendAgentRegisterNotificationForManager(array $fields) {
        
        \Bitrix\Main\Mail\Event::send(array_merge(array(
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => Settings::agentRegisterMailIdForManager()
                        ), self::$_commonMailParameters));
    }
    
    /**
     * Отправка письма пользователю, что он активирован как агент
     * @param array $fields
     */
    public static function sendAgentActivated (array $fields) {
        \Bitrix\Main\Mail\Event::send(array_merge(array(
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => Settings::agentActivatedMailId()
                        ), self::$_commonMailParameters));
    }
    
    /**
     * Отправка письма пользователю, что сменился статус путевки
     * @param array $fields
     */
    public static function sendChangeStatusNotification (array $fields) {
        \Bitrix\Main\Mail\Event::send(array_merge(array(
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => Settings::voucherStatusChangedMailId()
                        ), self::$_commonMailParameters));
    }
}
