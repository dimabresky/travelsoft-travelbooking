<?php

namespace travelsoft\booking\adapters;

/**
 * UserFieldManager adapter
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class UserFieldsManager {
    
    /**
     * @global \CUserTypeManager $USER_FIELD_MANAGER
     * @param string $relate
     * @param array $fields
     * @return array
     */
    public static function getFieldsWithReadyData ($relate, $fields) {
        
        global $USER_FIELD_MANAGER;
        
        return $USER_FIELD_MANAGER->getUserFieldsWithReadyData($relate, $fields, LANGUAGE_ID);
        
    }
    
    /**
     * @global \CUserTypeManager $USER_FIELD_MANAGER
     * @param string $relate
     * @param array $fields
     * @return array
     */
    public static function editFormAddFields ($relate, $fields) {
        
        global $USER_FIELD_MANAGER;
        
        $USER_FIELD_MANAGER->EditFormAddFields($relate, $fields);
        
        return $fields;
    }
    
    /**
     * Возвращает html для редактирования поля
     * @param bool $is_form_request
     * @param array $field
     * @return string
     */
    public static function getEditFormHtml (bool $is_form_request, array $field) {
        global $USER_FIELD_MANAGER;
        return $USER_FIELD_MANAGER->GetEditFormHtml($is_form_request, $_POST[$field['FIELD_NAME']], $field);
    }
}
