<?php

namespace travelsoft\booking\abstraction;

/**
 * Интерфейс для класса калькулятор
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
abstract class Calculator {
    
    /**
     * @var array
     */
    protected $_result = array();
    
    /**
     * @var \travelsoft\booking\adapters\CurrencyConverter
     */
    protected $_converter = null;
    
    abstract public function calculating ();
    abstract public function min ();
    abstract public function addTourservice ();
    abstract public function addMarkup ();
    abstract public function applyDiscount ();
    
    public function __construct() {
        $this->_converter = new \travelsoft\booking\adapters\CurrencyConverter;
    }
    
    /**
     * @return array
     */
    public function get () : array {
        return $this->_result;
    }
}
