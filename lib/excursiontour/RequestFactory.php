<?php

namespace travelsoft\booking\excursiontour;

/**
 * Класс-фабрика для класса запроса
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class RequestFactory extends \travelsoft\booking\RequestFactory {

    /**
     * @param array $request
     * @return \travelsoft\booking\Request
     */
    public static function create(array $request) {

        $oRequest = parent::create($request);

        if (is_array($request["excursions_id"]) && !empty($request["excursions_id"])) {

            $oRequest->excursions_id = array_map(function ($id) {
                return intVal($id);
            }, $request["excursions_id"]);
        }

        return $oRequest;
    }

}
