<?php

namespace travelsoft\booking\transfer;

/**
 * Класс запроса на поиск проездов
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Request extends \travelsoft\booking\Request{
    
    /**
     * @var boolean
     */
    public $tickets_buy = true;
}
