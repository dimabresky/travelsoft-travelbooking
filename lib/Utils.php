<?php

namespace travelsoft\booking;

/**
 * Класс утилит
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Utils {

    /**
     * Возвращает результат конвертации массива в строку
     * @param array $arFields
     * @return string
     */
    public static function ats(array $arFields): string {
        return base64_encode(gzcompress(serialize($arFields), 9));
    }

    /**
     * Возвращает результат конвертации строки в массив
     * @param string $str
     * @return array
     */
    public static function sta(string $str): array {
        return (array) unserialize(gzuncompress(base64_decode($str)));
    }

    /**
     * Конвертирует дату к формату в соответствии с константой travelsoft\booking\Settings::DATE_FORMAT
     * @param string $date
     * @return string
     */
    public static function dateConvert(string $date) {
        return (new \DateTime($date))->format(Settings::DATE_FORMAT);
    }

    /**
     * Возвращает количество ночей
     * @param string $date_from
     * @param string $date_to
     * @return int
     */
    public static function getNightsDuration(string $date_from, string $date_to): int {

        return intVal((strtotime($date_to) - strtotime($date_from)) / 86400);
    }

    /**
     * Возвращает количество дней
     * @param string $date_from
     * @param string $date_to
     * @return int
     */
    public static function getDaysDuration(string $date_from, string $date_to): int {

        return intVal((strtotime($date_to) - strtotime($date_from)) / 86400) + 1;
    }

    /**
     * Получение калькулятора для расчета цен
     * @param string $service_type
     * @param array $request
     * @return \travelsoft\booking\abstraction\Calculator
     * @throws Exception
     */
    public static function getCalculator(string $service_type, array $request): abstraction\Calculator {

        $namespace_part = strtolower($service_type);

        $calculatorFactoryClass = "\\travelsoft\\booking\\" . $namespace_part . "\\CalculatorFactory";
        if (!class_exists($calculatorFactoryClass)) {
            (new Logger())->write("Class " . $calculatorFactoryClass . " not found");
            throw new \Exception("Error by price calculation");
        }

        $requestFactoryClass = "\\travelsoft\\booking\\" . $namespace_part . "\\RequestFactory";
        if (!class_exists($requestFactoryClass)) {
            (new Logger())->write("Class " . $requestFactoryClass . " not found");
            throw new \Exception("Error by price calculation");
        }

        return $calculatorFactoryClass::create($requestFactoryClass::create($request));
    }

    /**
     * Возвращает массив расчитанных цен
     * @param string $service_type
     * @param array $request
     * @return array
     */
    public static function getCalculationResult(string $service_type, array $request): array {

        return self::getCalculator($service_type, $request)->calculating()->addTourservice()->addMarkup()->applyDiscount()->get();
    }

    /**
     * Возвращает массив минимальных расчитанных цен
     * @param string $service_type
     * @param array $request
     * @return array
     */
    public static function getMinCalculationResult(string $service_type, array $request): array {

        return self::getCalculator($service_type, $request)->calculating()->addTourservice()->addMarkup()->applyDiscount()->min()->get();
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForPlacements() {
        $result = array();
        $request = self::makePlacementsSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "placements", "/travelsoft/booking/offers/placements", Settings::CACHE_TIME);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("placements", $request);
            });
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForPackageTour() {
        $result = array();
        $request = self::makePackageTourSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "packageTour", "/travelsoft/booking/offers/packageTour", Settings::CACHE_TIME);

        if (empty($result = $cache->get())) {

            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("packageTour", $request);
            });
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForTransfer() {
        $result = array();
        $request = self::makeTransferSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "transfer", "/travelsoft/booking/offers/transfer", Settings::CACHE_TIME);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("transfer", $request);
            });
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForExcursionTour() {
        $result = array();
        $request = self::makeExcursionTourSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "excursionTour", "/travelsoft/booking/offers/excursionTour", Settings::CACHE_TIME);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("excursionTour", $request);
            });
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForRooms() {

        $result = array();
        $request = self::makeRoomsSearchRequest();

        $cache = new adapters\Cache(serialize($request) . "rooms", "/travelsoft/booking/offers/rooms", Settings::CACHE_TIME);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("rooms", $request);
            });
        }

        return $result;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeRoomsSearchRequest(array $services_id = null) {

        $request = self::makeCommonSearchRequest($services_id);

        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeTransferSearchRequest(array $services_id = null) {

        $request = self::makeCommonSearchRequest($services_id);

        if (@$_GET['travelbooking']["tickets_buy"]) {
            $request["tickets_buy"] = true;
        }

        if (isset($_GET["travelbooking"]["date"])) {
            $request["date_from"] = $_GET["travelbooking"]["date"];
            $request["date_to"] = $_GET["travelbooking"]["date"];
        }
        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeExcursionTourSearchRequest(array $services_id = null) {

        $request = self::makeCommonSearchRequest($services_id);

        if (isset($_GET["travelbooking"])) {
            if (isset($_GET["travelbooking"]["excursions_id"])) {

                $excursions_id = array_filter((array) $_GET["travelbooking"]["excursions_id"], function ($excursions_id) {
                    return $excursions_id > 0;
                });

                if (!empty($excursions_id)) {
                    $request["excursions_id"] = array_values($excursions_id);
                } else {
                    $request["excursions_id"] = [];
                }
            }

            if (isset($_GET["travelbooking"]["date"])) {
                $request["date_from"] = $_GET["travelbooking"]["date"];
                $request["date_to"] = $_GET["travelbooking"]["date"];
            }
        }

        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeTransferbackSearchRequest(array $services_id = null) {

        return self::makeTransferSearchRequest($services_id);
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makePlacementsSearchRequest(array $services_id = null) {

        $request = self::makeCommonSearchRequest($services_id);

        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makePackageTourSearchRequest(array $services_id = null) {

        $request = self::makeCommonSearchRequest($services_id);

        if (!empty($_GET['travelbooking'])) {

            $request["rooms_id"] = (array) @$_GET['travelbooking']["rooms_id"];
            $request["placements_id"] = (array) @$_GET['travelbooking']["placements_id"];
            $request["transfers_id"] = (array) @$_GET['travelbooking']["transfers_id"];
            $request["placements_rates_id"] = (array) @$_GET['travelbooking']["placements_rates_id"];
            $request["transfers_rates_id"] = (array) @$_GET['travelbooking']["transfers_rates_id"];
            $request["children_age"] = (array) @$_GET['travelbooking']["children_age"];
        } else {

            $request["services_id"] = array();
            $request["rooms_id"] = array();
            $request["placements_id"] = array();
            $request["transfers_id"] = array();
            $request["placements_rates_id"] = array();
            $request["transfers_rates_id"] = array();
        }

        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeCommonSearchRequest(array $services_id = null) {
        $request = \travelsoft\booking\Settings::commonSearchRequest();
        if (isset($_GET['travelbooking'])) {
            if (isset($_GET['travelbooking']["services_id"]) && is_array($_GET['travelbooking']["services_id"])) {
                $request["services_id"] = $_GET['travelbooking']["services_id"];
            }
            if (@$_GET['travelbooking']["date_from"]) {
                $request["date_from"] = $_GET['travelbooking']["date_from"];
            }
            if (@$_GET['travelbooking']["date_to"]) {
                $request["date_to"] = $_GET['travelbooking']["date_to"];
            }
            if (@$_GET['travelbooking']["children"] > 0) {
                $request["children"] = intVal($_GET['travelbooking']["children"]);
            }

            if (@$_GET['travelbooking']["adults"] > 0) {
                $request["adults"] = intVal($_GET['travelbooking']["adults"]);
            }
        } else {

            $request["services_id"] = array();
            $request["rates_id"] = array();
        }

        if (empty($request["services_id"]) && $services_id) {

            $services_id = array_filter($services_id, function ($service_id) {
                return $service_id > 0;
            });

            if (!empty($services_id)) {

                $request["services_id"] = array_values($services_id);
            } else {
                $request["services_id"] = array();
            }
        }

        return $request;
    }

    /**
     * @param array $common_parameters
     * @param array $object_id
     * @return array
     */
    public static function makeHighlightDatesParameters(array $common_parameters = [], int $object_id = 0) {

        if (!empty($common_parameters)) {

            if (isset($common_parameters["CALENDAR_FROM"])) {
                $common_parameters["CALENDAR_FROM"]["SERVICES_ID"] = \travelsoft\booking\Utils::getServicesIdByServiceTypeAndSearchObejctId($common_parameters["CALENDAR_FROM"]["SERVICE_TYPE"], $object_id);
            }

            if (isset($common_parameters["CALENDAR_TO"])) {

                $common_parameters["CALENDAR_TO"]["SERVICES_ID"] = \travelsoft\booking\Utils::getServicesIdByServiceTypeAndSearchObejctId($common_parameters["CALENDAR_TO"]["SERVICE_TYPE"], $object_id);
            }
        }

        return $common_parameters;
    }

    /**
     * @param int $placement_id
     * @return array
     */
    public static function getRoomsByPlacement(int $placement_id) {

        $arRooms = [];

        $cache = new adapters\Cache("getRoomsByPlacement_" . $placement_id, Settings::getServicesTypes()["rooms"]["cache_dir"]);

        if (empty($arRooms = $cache->get())) {

            $arRooms = $cache->caching(function () use ($cache, $placement_id) {

                $cache->setTagCache("highloadblock_" . Settings::roomsStoreId());
                return stores\Rooms::get(array("filter" => array("UF_PLACEMENT" => $placement_id)));
            });
        }

        return $arRooms;
    }

    /**
     * @param int $description_id
     * @return array
     */
    public static function getPackageTourByDescription(int $description_id) {

        $arPackageTour = [];

        $cache = new adapters\Cache("getPackageTourByDescriptionn_" . $description_id, Settings::getServicesTypes()["packageTour"]["cache_dir"]);

        if (empty($arPackageTour = $cache->get())) {

            $arPackageTour = $cache->caching(function () use ($cache, $description_id) {

                $cache->setTagCache("highloadblock_" . Settings::travelStoreId());
                $cache->setTagCache("highloadblock_" . Settings::packageTourStoreId());
                $arPackageTour = current(stores\PackageTour::get(array("filter" => array("UF_TOUR" => $description_id))));
                return $arPackageTour ?: [];
            });
        }

        return $arPackageTour;
    }

    /**
     * @param int $description_id
     * @return array
     */
    public static function getExcursionTourByDescription(int $description_id) {

        $arExcursion = [];

        $cache = new adapters\Cache("getExcursionTourByDescription_" . $description_id, Settings::getServicesTypes()["excursionTour"]["cache_dir"]);

        if (empty($arExcursion = $cache->get())) {

            $arExcursion = $cache->caching(function () use ($cache, $description_id) {

                $cache->setTagCache("iblock_id_" . Settings::excursionTourIBStoreId());
                $cache->setTagCache("highloadblock_" . Settings::excursionTourStoreId());
                return current(stores\ExcursionTour::get(array("filter" => array("UF_TOUR" => $description_id))));
            });
        }

        return $arExcursion;
    }

    /**
     * @param int $service_id
     * @return array
     */
    public static function getExcursionTourDescriptionByServiceId(int $service_id) {

        $arExcursion = [];

        $cache = new adapters\Cache("getExcursionTourDescriptionByServiceId_" . $service_id, Settings::getServicesTypes()["excursionTour"]["cache_dir"]);

        if (empty($arExcursion = $cache->get())) {

            $arExcursion = $cache->caching(function () use ($cache, $service_id) {

                $cache->setTagCache("iblock_id_" . Settings::excursionTourIBStoreId());
                $cache->setTagCache("highloadblock_" . Settings::excursionTourStoreId());
                return current(stores\ExcursionTour::get(array("filter" => array("ID" => $service_id))));
            });
        }

        return $arExcursion;
    }

    /**
     * Массив в base64 строку
     * @param array $data
     * @return string
     */
    public static function base64encode(array $data): string {

        return base64_encode(serialize($data));
    }

    /**
     * base64 строку в массив
     * @param string $str
     * @return array
     */
    public static function base64decode(string $str): array {
        return unserialize(base64_decode($str));
    }

    /**
     * @param string $service_type
     * @param string $code
     * @return int
     */
    public static function getObjectIdByCode(string $service_type, string $code): int {

        $cache = new adapters\Cache(md5($service_type . $code), "/travelsoft/booking/objects");

        if (empty($arObject = $cache->get())) {
            $arObject = $cache->caching(function () use ($service_type, $code) {

                $arObject = [];

                $className = ucfirst($service_type);

                switch ($service_type) {
                    case "packageTour":
                        $className = "Travel";
                        break;
                    case "excursionTour":
                        $className = "ExcursionTourIB";
                        break;
                    case "rooms":
                        $className = "Placements";
                        break;
                    case "transfer":
                    case "transferback":
                        return 0;
                }

                $class = "\\travelsoft\\booking\\stores\\" . $className;

                $arObject = current($class::get(array(
                            "filter" => array("CODE" => $code),
                            "select" => array("ID")
                )));

                return $arObject;
            });
        }

        return (int) @$arObject["ID"];
    }

    /**
     * @param int|array $pid
     * @param int|array $tid
     * @param int|array $tbid
     * @return array
     */
    public static function getPackageTours($pid = null, $tid = null, $tbid = null) {

        $cache = new adapters\Cache(md5("getPackageTours_" . serialize(func_get_args())), Settings::getServicesTypes()["packageTour"]["cache_dir"]);

        if (empty($arPackageTours = $cache->get())) {
            $arPackageTours = $cache->caching(function () use ($pid, $tid, $tbid, $cache) {
                $cache->setTagCache("highloadblock_" . Settings::packageTourStoreId());
                $arr_filter = array();
                if ($pid > 0 || is_array($pid)) {
                    $arr_filter["UF_PLACEMENT"] = $pid;
                }
                if ($tid > 0 || is_array($tid)) {
                    $arr_filter["UF_TRANSFER"] = $tid;
                }
                if ($tbid > 0 || is_array($tbid)) {
                    $arr_filter["UF_TRANSFER_BACK"] = $tbid;
                }
                return stores\PackageTour::get(array(
                            "filter" => $arr_filter
                ));
            });
        }

        return $arPackageTours;
    }

    /**
     * @param int $packageTourDescription_id
     * @return array
     */
    public static function getTransferByPackageTourDescription(int $packageTourDescription_id) {

        $arPackageTour = self::getPackageTourByDescription($packageTourDescription_id);

        $arTransfer = [];

        $cache = new adapters\Cache("getTransferByPackageTourDescription_" . $packageTourDescription_id, Settings::getServicesTypes()["packageTour"]["cache_dir"]);

        if (empty($arTransfer = $cache->get()) && !empty($arPackageTour)) {
            $arTransfer = $cache->caching(function () use ($arPackageTour, $cache) {
                $cache->setTagCache("iblock_" . Settings::travelStoreId());
                $cache->setTagCache("highloadblock_" . Settings::packageTourStoreId());
                $cache->setTagCache("highloadblock_" . Settings::busesStoreId());
                return stores\Buses::getById($arPackageTour["UF_TRANSFER"]);
            });
        }

        return $arTransfer;
    }

    /**
     * @param int $packageTourDescription_id
     * @return array
     */
    public static function getTransferbackByPackageTourDescription(int $packageTourDescription_id) {

        $arPackageTour = self::getPackageTourByDescription($packageTourDescription_id);

        $arTransferback = [];

        $cache = new adapters\Cache("getTransferbackByPackageTourDescription_" . $packageTourDescription_id, Settings::getServicesTypes()["packageTour"]["cache_dir"]);

        if (empty($arTransferback = $cache->get()) && !empty($arPackageTour)) {
            $arTransferback = $cache->caching(function () use ($arPackageTour, $cache) {
                $cache->setTagCache("iblock_" . Settings::travelStoreId());
                $cache->setTagCache("highloadblock_" . Settings::packageTourStoreId());
                $cache->setTagCache("highloadblock_" . Settings::busesStoreId());
                return stores\Buses::getById($arPackageTour["UF_TRANSFER_BACK"]);
            });
        }

        return $arTransferback;
    }

    /**
     * @param string $service_type
     * @param int $object_id
     * @return array
     */
    public static function getServicesIdByServiceTypeAndSearchObejctId(string $service_type, int $object_id) {
        $arServicesId = [];
        switch ($service_type) {

            case "placements":
            case "rooms":

                $arServicesId = \array_values(\array_map(function (array $item) {
                            return (int) $item["ID"];
                        }, self::getRoomsByPlacement($object_id)));
                break;

            case "excursionTour":

                $excursion = self::getExcursionTourByDescription($object_id);
                if (!empty($excursion)) {
                    $arServicesId[] = $excursion["ID"];
                }
                break;

            case "transfer":

                $transfer = self::getTransferByPackageTourDescription($object_id);
                if (!empty($transfer)) {
                    $arServicesId[] = $transfer["ID"];
                }

                break;

            case "transferback":

                $transferback = self::getTransferbackByPackageTourDescription($object_id);
                if (!empty($transferback)) {
                    $arServicesId[] = $transferback["ID"];
                }
                break;

            case "rates":

                $arServicesId = @stores\Rates::getById($object_id, ["ID", "UF_SERVICES"])["UF_SERVICES"];

                break;
        }

        return $arServicesId;
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreasePlacementsNumberOfSold(array $data) {
        self::decreaseRoomsNumberOfSold($data);
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increasePlacementsNumberOfSold(array $data) {
        self::increaseRoomsNumberOfSold($data);
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseRoomsNumberOfSold(array $data) {
        if (isset($data["UF_ROOM"]) && $data["UF_ROOM"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_ROOM"],
                    "UF_SERVICE_TYPE" => "rooms",
                    "><UF_DATE" => array(
                        $data["UF_DATE_FROM_PLACE"],
                        adapters\Date::createFromTimestamp(intVal($data["UF_DATE_TO_PLACE"]->getTimestamp() - 86400))
                    )
                ),
                "order" => array("UF_DATE" => "ASC"),
                "select" => array("ID", "UF_SOLD_NUMBER"))
            ) as $arr_quota) {

                $sold = $arr_quota["UF_SOLD_NUMBER"] - 1;

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold || 0
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseRoomsNumberOfSold(array $data) {
        if (isset($data["UF_ROOM"]) && $data["UF_ROOM"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_ROOM"],
                    "UF_SERVICE_TYPE" => "rooms",
                    "><UF_DATE" => array(
                        $data["UF_DATE_FROM_PLACE"],
                        adapters\Date::createFromTimestamp(intVal($data["UF_DATE_TO_PLACE"]->getTimestamp() - 86400))
                    )
                ),
                "order" => array("UF_DATE" => "ASC"),
                "select" => array("ID", "UF_SOLD_NUMBER"))
            ) as $arr_quota) {

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + 1
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseExcursionTourNumberOfSold(array $data) {
        if (isset($data["UF_SERVICE"]) && $data["UF_SERVICE"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_SERVICE"],
                    "UF_SERVICE_TYPE" => "excursionTour",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold || 0
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseExcursionTourNumberOfSold(array $data) {
        if (isset($data["UF_SERVICE"]) && $data["UF_SERVICE"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_SERVICE"],
                    "UF_SERVICE_TYPE" => "excursionTour",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseTransferNumberOfSold(array $data) {
        if (isset($data["UF_TRANSFER"]) && $data["UF_TRANSFER"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER"],
                    "UF_SERVICE_TYPE" => "transfer",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold || 0
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseTransferNumberOfSold(array $data) {
        if (isset($data["UF_TRANSFER"]) && $data["UF_TRANSFER"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER"],
                    "UF_SERVICE_TYPE" => "transfer",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseTransferNumberOfSoldTickets(array $data) {

        if (isset($data["UF_TRANSFER"]) && $data["UF_TRANSFER"] > 0) {
            foreach (stores\TransferQuotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER"],
                    "UF_SERVICE_TYPE" => "transfer",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                stores\TransferQuotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseTransferNumberOfSoldTickets(array $data) {
        if (isset($data["UF_TRANSFER"]) && $data["UF_TRANSFER"] > 0) {
            foreach (stores\TransferQuotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER"],
                    "UF_SERVICE_TYPE" => "transfer",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);

                stores\TransferQuotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold || 0
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseTransferbackNumberOfSold(array $data) {

        if (isset($data["UF_TRANSFER_BACK"]) && $data["UF_TRANSFER_BACK"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER_BACK"],
                    "UF_SERVICE_TYPE" => "transferback",
                    "UF_DATE" => array(
                        $data["UF_DATE_BACK_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold || 0
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseTransferbackNumberOfSold(array $data) {

        if (isset($data["UF_TRANSFER_BACK"]) && $data["UF_TRANSFER_BACK"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER_BACK"],
                    "UF_SERVICE_TYPE" => "transferback",
                    "UF_DATE" => array(
                        $data["UF_DATE_BACK_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseTransferbackNumberOfSoldTickets(array $data) {
        if (isset($data["UF_TRANSFER_BACK"]) && $data["UF_TRANSFER_BACK"] > 0) {
            foreach (stores\TransferQuotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER_BACK"],
                    "UF_SERVICE_TYPE" => "transferback",
                    "UF_DATE" => array(
                        $data["UF_DATE_BACK_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                stores\TransferQuotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseTransferbackNumberOfSoldTickets(array $data) {
        if (isset($data["UF_TRANSFER_BACK"]) && $data["UF_TRANSFER_BACK"] > 0) {
            foreach (stores\TransferQuotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER_BACK"],
                    "UF_SERVICE_TYPE" => "transferback",
                    "UF_DATE" => array(
                        $data["UF_DATE_BACK_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {

                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);

                stores\TransferQuotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold || 0
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreasePackageTourNumberOfSold(array $data) {
        self::decreasePlacementsNumberOfSold($data);
        self::decreaseTransferNumberOfSold($data);
        self::decreaseTransferbackNumberOfSold($data);
    }

    /**
     * @param array $data массив полей таблицы путевок
     */
    public static function increasePackageTourNumberOfSold(array $data) {
        self::increasePlacementsNumberOfSold($data);
        self::increaseTransferNumberOfSold($data);
        self::increaseTransferbackNumberOfSold($data);
    }

    /**
     * Создание ссылки для запроса на поиск цен
     * @param string $source_link
     * @return string
     */
    public static function createRequestLink(string $source_link) {

        $request_link = $source_link;

        $arr_query_string = array();
        
        if ($_GET["travelbooking"]) {

            foreach ($_GET["travelbooking"] as $k => $v) {

                if (is_array($v)) {

                    foreach ($v as $vv) {

                        $arr_query_string[] = "travelbooking[$k][]=$vv";
                    }
                } else {

                    $arr_query_string[] = "travelbooking[$k]=$v";
                }
            }
        }

        if (!empty($arr_query_string)) {
            $request_link .= "?s2o=Y&" . implode("&", $arr_query_string);
        }

        return $request_link;
    }

    /**
     * Очистка связанных данных при удалении номера
     * @param int $id
     */
    public static function clearForRooms(int $id) {
        self::_clearForService("rooms", $id);
    }

    /**
     * Очистка связанных данных при удалении экскурсионного тура
     * @param int $id
     */
    public static function clearForExcursionTour(int $id) {
        self::_clearForService("excursionTour", $id);
    }

    /**
     * Очистка связанных данных при удалении проезда
     * @param int $id
     */
    public static function clearForTransfer(int $id) {
        self::_clearForService("transfer", $id);
    }

    /**
     * Очистка связанных данных при удалении проезда
     * @param int $id
     */
    public static function clearForTransferBack(int $id) {
        self::_clearForService("tansferback", $id);
    }

    /**
     * Очистка связанных данных при удалении проживания
     * @param int $id
     */
    public static function clearForPlacements(int $id) {
        foreach (stores\Rooms::get(array(
            "filter" => array("UF_PLACEMENT" => $id),
            "select" => array("ID")
        )) as $arr_room) {
            stores\Rooms::delete($arr_room["ID"]);
        }
    }

    /**
     * @param string $serviceType
     * @param int $id
     */
    protected static function _clearForService(string $serviceType, int $id) {

        // очистка квот
        foreach (stores\Quotas::get(array(
            "filter" => array("UF_SERVICE_ID" => $id, "UF_SERVICE_TYPE" => $serviceType),
            "select" => array('ID')
        )) as $arr_quota) {
            stores\Quotas::delete($arr_quota["ID"]);
        }

        // очистка продолжительности
        foreach (stores\Duration::get(array(
            "filter" => array("UF_SERVICE_ID" => $id, "UF_SERVICE_TYPE" => $serviceType),
            "select" => array('ID')
        )) as $arr_duration) {
            stores\Duration::delete($arr_duration["ID"]);
        }

        // очистка цен
        foreach (stores\Prices::get(array(
            "filter" => array("UF_SERVICE_ID" => $id, "UF_SERVICE_TYPE" => $serviceType),
            "select" => array('ID')
        )) as $arr_prices) {
            stores\Prices::delete($arr_prices["ID"]);
        }

        // отвязываем услугу от тарифов
        foreach (stores\Rates::get(array(
            "filter" => array("UF_SERVICES" => array($id), "UF_SERVICES_TYPE" => $serviceType),
            "select" => array("ID", "UF_SERVICES")
        )) as $arr_rate) {
            $key = array_search($id, $arr_rate["UF_SERVICES"]);
            if ($key !== false) {
                unset($arr_rate["UF_SERVICES"][$key]);
            }
            stores\Rates::update($arr_rate["ID"], array("UF_SERVICES" => $arr_rate["UF_SERVICES"]));
        }
    }

    /**
     * Отправка json-строки
     * @global \CMain $APPLICATION
     * @param string $body
     */
    public static function sendJsonResponse(string $body) {

        global $APPLICATION;

        \header('Content-Type: application/json; charset=' . \SITE_CHARSET);

        $APPLICATION->RestartBuffer();

        echo $body;

        die();
    }

    /**
     * @param array $parameters
     */
    public static function includeSearchFormComponent(array $parameters = []) {

        $_paraemeters = [
            "component_name" => "travelsoft:booking.search_form",
            "component_template" => $parameters["component_template"],
            "component_parameters" => [
                "SERVICE_TYPE" => is_callable($parameters["service_type"]) ? $parameters["service_type"]() : $parameters["service_type"],
                "HIGHLIGHT_FOR" => \travelsoft\booking\Utils::makeHighlightDatesParameters(
                        is_callable($parameters["highlight_dates_parameters"]) ? $parameters["highlight_dates_parameters"]() : $parameters["highlight_dates_parameters"], \travelsoft\booking\Utils::getObjectIdByCode(is_callable($parameters["service_type"]) ? $parameters["service_type"]() : $parameters["service_type"], $parameters["element_code"]))
            ]
        ];

        adapters\ComponentIncluder::includeComponent($_paraemeters);
    }

    /**
     * @param array $parameters
     */
    public static function includeOffersComponent(array $parameters = []) {

        $service_type = is_callable($parameters["service_type"]) ? $parameters["service_type"]() : $parameters["service_type"];

        $_paraemeters = [
            "component_name" => "travelsoft:booking.offers",
            "component_template" => $parameters["component_template"],
            "component_parameters" => [
                "SERVICE_TYPE" => $service_type,
                "BOOKING_URL" => $parameters["booking_url"],
                "SERVICES_ID" => self::getServicesIdByServiceTypeAndSearchObejctId($service_type, self::getObjectIdByCode($service_type, $parameters["element_code"]))
            ]
        ];

        adapters\ComponentIncluder::includeComponent($_paraemeters);
    }

    /**
     * @param array $parameters
     * @return array
     */
    public static function getHTMLSearchOffersFormAndOffersListOnDetailPage(array $parameters = []) {

        $arHTML = [
            "SEARCH_PLACEMENTS_OFFERS_RESULT_HTML" => "",
            "SEARCH_FORM_HTML" => ""
        ];

        ob_start();

        self::includeSearchFormComponent(\array_merge($parameters, ["component_template" => $parameters["search_form_component_template"]]));

        $arHTML["SEARCH_FORM_HTML"] = ob_get_clean();

        ob_start();

        self::includeOffersComponent(\array_merge($parameters, ["component_template" => $parameters["offers_component_template"]]));

        $arHTML["SEARCH_PLACEMENTS_OFFERS_RESULT_HTML"] = ob_get_clean();

        return $arHTML;
    }

    /**
     * Расчет цены "от"
     * @param callable $before должен вернуть массив вида [тип услуг => дополнительные параметры поиска (для $_GET['travelbooking'])]
     * @param callable $after (задать, если необходимо) дополнительная обработка результата. Принимает массив результата расчета и тип объекта.
     * 
     * @return array $result
     */
    public static function getCaluculationsPriceFrom(callable $before, callable $after = null) {

        $arData = $before();

        $result = [];

        if (!isset($_GET['travelbooking'])) {
            $_GET['travelbooking'] = [];
        }

        foreach ($arData as $service_type => $data) {
            
            $method = "getMinCalculationResultFor" . ucfirst($service_type);

            if (method_exists("\\travelsoft\\booking\\Utils", $method) && !empty($data)) {

                $_GET["travelbooking"] = \array_merge($_GET["travelbooking"], $data);

                $result[$service_type] = self::$method();
            }
            
            unset($_GET["travelbooking"]["services_id"]);
            unset($_GET["travelbooking"]["exursions_id"]);
        }


        if ($after) {
            $after($result);
        }
        
        return $result;
    }

}
