<?php

namespace travelsoft\booking;

/**
 * Класс методов расчета цен
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class CalculationTypes {

    /**
     * Расчет цен по типу "Взрослый на основном месте"
     * @param array $data
     * @return array
     */
    public static function adultOnMainPlace(array $data): array {

        if ($data["seating"]["main_places"]["adults"] > 0 && !empty($data["prices"])) {

            foreach ($data["prices"] as $arr_price) {
                $data["gross"] += $arr_price["gross"] * $data["seating"]["main_places"]["adults"];
                $data["netto"] += $arr_price["netto"] * $data["seating"]["main_places"]["adults"];
            }

            $data["seating"]["main_places"]["adults"] = 0;
        }

        return $data;
    }

    /**
     * Расчет цен по типу "Взрослый на доп. месте"
     * @param array $data
     * @return array
     */
    public static function adultOnAddPlace(array $data): array {
        if ($data["seating"]["additional_places"]["adults"] > 0 && !empty($data["prices"])) {

            foreach ($data["prices"] as $arr_price) {
                $data["gross"] += $arr_price["gross"] * $data["seating"]["additional_places"]["adults"];
                $data["netto"] += $arr_price["netto"] * $data["seating"]["additional_places"]["adults"];
            }

            $data["seating"]["additional_places"]["adults"] = 0;
        }

        return $data;
    }

    /**
     * Расчет цен по типу "Проезд взрослый"
     * @param array $data
     * @return array
     */
    public static function adultTransfer(array $data) {

        if ($data["seating"]["adults"] > 0 && !empty($data["prices"])) {

            $data["gross"] += $data["prices"]["gross"] * $data["seating"]["adults"];
            $data["netto"] += $data["prices"]["netto"] * $data["seating"]["adults"];

            $data["seating"]["adults"] = 0;
        }
        return $data;
    }

    /**
     * Расчет цен по типу "Проезд ребенок"
     * @param array $data
     * @return array
     */
    public static function childrenTransfer(array $data) {

        if ($data['seating']['children'] > 0 && !empty($data['prices'])) {
            $children = $data['seating']['children'];
            $ages = $data['seating']['children_age'];

            do {

                $age_key = key($ages);
                $age = current($ages);
                next($age);

                if ($age >= $data['min_age'] && $age <= $data['max_age']) {

                    $data['gross'] += $data['prices']["gross"];
                    $data['netto'] += $data['prices']["netto"];

                    $data['seating']['children'] --;
                    unset($data['seating']['children_age'][$age_key]);
                }

                $children--;
            } while ($children > 0);
        }

        return $data;
    }
    
    /**
     * Расчет цен по типу "Экскурсия - взрослый"
     * @param array $data
     * @return array
     */
    public static function excursionAdult(array $data) {

        if ($data["seating"]["adults"] > 0 && !empty($data["prices"])) {

            $data["gross"] += $data["prices"]["gross"] * $data["seating"]["adults"];
            $data["netto"] += $data["prices"]["netto"] * $data["seating"]["adults"];

            $data["seating"]["adults"] = 0;
        }
        return $data;
    }

    /**
     * Расчет цен по типу "Экскурсия - ребенок"
     * @param array $data
     * @return array
     */
    public static function excursionChildren(array $data) {

        if ($data['seating']['children'] > 0 && !empty($data['prices'])) {
            $children = $data['seating']['children'];
            $ages = $data['seating']['children_age'];

            do {

                $age_key = key($ages);
                $age = current($ages);
                next($age);

                if ($age >= $data['min_age'] && $age <= $data['max_age']) {

                    $data['gross'] += $data['prices']["gross"];
                    $data['netto'] += $data['prices']["netto"];

                    $data['seating']['children'] --;
                    unset($data['seating']['children_age'][$age_key]);
                }

                $children--;
            } while ($children > 0);
        }

        return $data;
    }
    
    /**
     * Одноместное размещение
     * @param array $data
     * @return array
     */
    public static function singleOccupancy (array $data) : array {
        return self::adultOnMainPlace($data);
    }
    
    /**
     * Расчет цен по типу "Ребенок на доп. месте"
     * @param array $data
     * @return array
     */
    public static function childrenOnAddPlace(array $data): array {

        return self::_childrenCommonCalculateByAgeForRooms("additional_places", $data);
    }
    
    /**
     * Расчет цен по типу "Добавить к стоимости(проживание)"
     * @param array $data
     * @return array
     */
    public static function addToCostPlacements(array $data) {
        foreach ($data["prices"] as $arr_price) {
            $data["gross"] += $arr_price["gross"];
            $data["netto"] += $arr_price["netto"];
        }
        return $data;
    }
    
    /**
     * Расчет цен по типу "Добавить к стоимости(проезд)"
     * @param array $data
     * @return array
     */
    public static function addToCostTransfer(array $data) {
        
            $data["gross"] += $data["prices"]["gross"];
            $data["netto"] += $data["prices"]["netto"];
            return $data;
    }


    /**
     * Расчет цен по типу "Ребенок на основном месте"
     * @param array $data
     * @return array
     */
    public static function childrenOnMainPlace(array $data): array {
        return self::_childrenCommonCalculateByAgeForRooms("main_places", $data);
    }

    /**
     * Расчет цен по типу "Ребенок без места"
     * @param array $data
     * @return array
     */
    public static function childrenWithoutPlace(array $data): array {
        
        return self::_childrenCommonCalculateByAgeForRooms("without_places", $data);
    }

    /**
     * Производит расчет стоимости по детям
     * @param string $placeType
     * @param array $data
     * @param array
     */
    protected static function _childrenCommonCalculateByAgeForRooms(string $placeType, array $data): array {

        if ($data['seating'][$placeType]['children'] > 0 && !empty($data['prices'])) {
            $children = $data['seating'][$placeType]['children'];
            $ages = $data['seating'][$placeType]['children_age'];
            
            do {

                $age_key = key($ages);
                $age = current($ages);
                next($ages);
                $children--;
                if ($age >= $data['min_age'] && $age <= $data['max_age']) {

                    foreach ($data['prices'] as $arr_price) {
                        $data['gross'] += $arr_price["gross"];
                        $data['netto'] += $arr_price["netto"];
                    }

                    $data['seating'][$placeType]['children'] --;
                    unset($data['seating'][$placeType]['children_age'][$age_key]);
                }

                
            } while ($children > 0);
        }

        return $data;
    }

}
