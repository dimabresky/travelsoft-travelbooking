<?php

namespace travelsoft\booking\rooms;

/**
 * Класс запроса на поиск номеров
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Request extends \travelsoft\booking\Request {}
