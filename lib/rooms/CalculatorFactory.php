<?php

namespace travelsoft\booking\rooms;

/**
 * Класс-фабрика Calculator
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class CalculatorFactory {

    public static function create(\travelsoft\booking\Request $request) {

        if (!empty($request->services_id)) {

            // уменьшаем дату расчета на одни сутки для расчета корректной стоимости
            $date_to = date(\travelsoft\booking\Settings::DATE_FORMAT, strtotime($request->date_to) - 86400);

            $duration = \travelsoft\booking\Utils::getNightsDuration((string) $request->date_from, (string) $request->date_to);

            if ($duration <= 0) {
                $duration = 1;
            }

            $request->services_id = array_unique($request->services_id);

            // получаем продолжительность по проживанию
            // фильтруем проживание по продолжительности
            $arr_durations = \travelsoft\booking\stores\Duration::get(array(
                        "filter" => array(
                            "UF_SERVICE_ID" => $request->services_id,
                            "UF_SERVICE_TYPE" => "rooms",
                            "><UF_DATE" => array(
                                \travelsoft\booking\adapters\Date::create($request->date_from),
                                \travelsoft\booking\adapters\Date::create($date_to)
                            ),
                            array(
                                "LOGIC" => "OR",
                                array("UF_DURATION" => array($duration)),
                                array("UF_DURATION" => false)
                            )
                        )
            ));

            $arr_services_id = $request->services_id;
            if (!empty($arr_durations)) {

                foreach ($request->services_id as $key => $service_id) {

                    foreach ($arr_durations as $arr_duration) {

                        if ($arr_duration["UF_SERVICE_ID"] == $service_id) {

                            if (!empty($arr_duration["UF_DURATION"]) && !in_array($duration, $arr_duration["UF_DURATION"])) {
                                unset($arr_services_id[$key]);
                            }

                            break;
                        }
                    }
                }
                
                $arr_services_id = array_values($arr_services_id);
            }

            rsort($request->children_age);
            
            if (!empty($arr_services_id)) {

                $arr_quotas = \travelsoft\booking\stores\Quotas::get(array(
                            "filter" => array(
                                "UF_SERVICE_ID" => $arr_services_id,
                                "UF_SERVICE_TYPE" => "rooms",
                                "><UF_DATE" => array(
                                    \travelsoft\booking\adapters\Date::create($request->date_from),
                                    \travelsoft\booking\adapters\Date::create($date_to)
                                )
                            ),
                            "order" => array("UF_DATE" => "ASC")
                ));

                $arr_services_id = $arr_group_quotas = array();
                // производим фильтрацию размещений по квотам
                foreach ($arr_quotas as $arr_quota) {
                    $arr_group_quotas[$arr_quota["UF_SERVICE_ID"]][] = $arr_quota;
                }

                foreach ($arr_group_quotas as $service_id => $arr_quotas) {

                    $involve = true;
                    foreach ($arr_quotas as $arr_quota) {
                        if ($arr_quota["UF_QUOTA"] - $arr_quota["UF_SOLD_NUMBER"] <= 0 || $arr_quota["UF_STOP"] == 1) {
                            $involve = false;
                            break;
                        }
                    }
                    if ($involve) {
                        $arr_services_id[] = $arr_quota["UF_SERVICE_ID"];
                    }
                }

                if (!empty($arr_services_id)) {

                    // производим рассадку людей
                    foreach (\travelsoft\booking\stores\Rooms::get(array(
                        "filter" => array(
                            "ID" => $arr_services_id,
                        ),
                        "select" => array("ID", "UF_MAIN_PLACES", "UF_ADD_PLACES", "UF_TOTAL_COST_CALC", "UF_CAPACITY_CALC")
                    )) as $arr) {

                        $seating_data[$arr["ID"]] = array(
                            'main_places' => array(
                                'adults' => 0,
                                'children' => 0,
                                'children_age' => array()
                            ),
                            'additional_places' => array(
                                'adults' => 0,
                                'children' => 0,
                                'children_age' => array()
                            ),
                            'without_places' => array(
                                'adults' => 0,
                                'children' => 0,
                                'children_age' => array()
                            )
                        );

                        $deltaMainPlaces = $arr['UF_MAIN_PLACES'] - $request->adults;

                        if ($deltaMainPlaces >= 0) {

                            $seating_data[$arr["ID"]]['main_places']['adults'] = $request->adults;

                            if ($request->children > 0) {

                                if ($deltaMainPlaces == 0) {

                                    $deltaAdditPlaces = $arr["UF_ADD_PLACES"] - $request->children;

                                    if ($deltaAdditPlaces >= 0) {
                                        $seating_data[$arr["ID"]]['additional_places']['children'] = $request->children;
                                        $seating_data[$arr["ID"]]['additional_places']['children_age'] = $request->children_age;
                                    } else {

                                        $ages = $request->children_age;
                                        $seating_data[$arr["ID"]]['additional_places']['children'] = $arr["UF_ADD_PLACES"];
                                        for ($i = 0; $i < $arr["UF_ADD_PLACES"]; $i++) {
                                            $seating_data[$arr["ID"]]['additional_places']['children_age'][] = $ages[$i];
                                            unset($ages[$i]);
                                        }

                                        $seating_data[$arr["ID"]]['without_places']['children'] = abs($deltaAdditPlaces);
                                        $seating_data[$arr["ID"]]['without_places']['children_age'] = array_values($ages);
                                    }
                                } else {

                                    $deltaAdditPlaces = $deltaMainPlaces - $request->children;

                                    if ($deltaAdditPlaces >= 0) {

                                        $seating_data[$arr["ID"]]['main_places']['children'] = $request->children;
                                        $seating_data[$arr["ID"]]['main_places']['children_age'] = $request->children_age;
                                    } else {

                                        $delta = $deltaMainPlaces;
                                        $ages = $request->children_age;
                                        $seating_data[$arr["ID"]]['main_places']['children'] = $delta;
                                        for ($i = 0; $i < $delta; $i++) {
                                            $seating_data[$arr["ID"]]['main_places']['children_age'][] = $ages[$i];
                                            unset($ages[$i]);
                                        }

                                        $deltaAdditPlaces = abs($deltaAdditPlaces);
                                        if ($arr["UF_ADD_PLACES"] >= $deltaAdditPlaces) {
                                            $seating_data[$arr["ID"]]['additional_places']['children'] = $deltaAdditPlaces;
                                            $seating_data[$arr["ID"]]['additional_places']['children_age'] = array_values($ages);
                                        } else {
                                            if ($arr["UF_ADD_PLACES"] > 0) {
                                                $seating_data[$arr["ID"]]['additional_places']['children'] = $arr["UF_ADD_PLACES"];
                                                for ($i = 0; $i < $arr["UF_ADD_PLACES"]; $i++) {
                                                    $seating_data[$arr["ID"]]['additional_places']['children_age'][] = $ages[$i];
                                                    unset($ages[$i]);
                                                }

                                                $seating_data[$arr["ID"]]['without_places']['children'] = $deltaAdditPlaces - $arr["UF_ADD_PLACES"];
                                                $seating_data[$arr["ID"]]['without_places']['children_age'] = array_values($ages);
                                            } else {
                                                $seating_data[$arr["ID"]]['without_places']['children'] = $deltaAdditPlaces;
                                                $seating_data[$arr["ID"]]['without_places']['children_age'] = array_values($ages);
                                            }
                                        }
                                    }
                                }
                            }
                        } else {

                            $seating_data[$arr["ID"]]['main_places']['adults'] = $arr['UF_MAIN_PLACES'];

                            $deltaAdditPlaces = $arr['UF_ADD_PLACES'] + $deltaMainPlaces;

                            if ($deltaAdditPlaces >= 0) {

                                $seating_data[$arr["ID"]]['additional_places']['adults'] = abs($deltaMainPlaces);

                                if ($request->children > 0) {

                                    if ($deltaAdditPlaces == 0) {
                                        $seating_data[$arr["ID"]]['without_places']['children'] = $request->children;
                                        $seating_data[$arr["ID"]]['without_places']['children_age'] = $request->children_age;
                                    } else {

                                        $deltaChildren = $deltaAdditPlaces - $request->children;
                                        if ($deltaChildren >= 0) {
                                            $seating_data[$arr["ID"]]['additional_places']['children'] = $request->children;
                                            $seating_data[$arr["ID"]]['additional_places']['children_age'] = $request->children_age;
                                        } else {
                                            $delta = $request->children + $deltaChildren;
                                            $ages = $request->children_age;
                                            $seating_data[$arr["ID"]]['additional_places']['children'] = $delta;
                                            for ($i = 0; $i < $delta; $i++) {
                                                $seating_data[$arr["ID"]]['additional_places']['children_age'][] = $ages[$i];
                                                unset($ages[$i]);
                                            }

                                            $seating_data[$arr["ID"]]['without_places']['children'] = abs($deltaChildren);
                                            $seating_data[$arr["ID"]]['without_places']['children_age'] = array_values($ages);
                                        }
                                    }
                                }
                            }
                        }

                        $total_adults = intVal($request->adults);
                        $seating_adults = intVal($seating_data[$arr["ID"]]["main_places"]["adults"] + $seating_data[$arr["ID"]]["additional_places"]["adults"]);

                        // проверяем удалось ли всех рассадить
                        // если остались дети, то предпологаем,
                        // что они идут без места и рассаживаем их на основные места
                        // если ок, то проживание учавсвует в расчете цен
                        if ($total_adults !== $seating_adults) {

                            unset($seating_data[$arr["ID"]]);
                        } else {

                            // если номер покупает количество человек меньше, чем основных мест, то
                            // общая стоимость номера включает основные
                            $seating_main_total = intVal($seating_data[$arr["ID"]]["main_places"]["adults"] + $seating_data[$arr["ID"]]["main_places"]["children"]);
                            if ($seating_main_total < $arr['UF_MAIN_PLACES']) {
                                // дополняем основные места взрослыми, для продажи по полной стоимости за номер 
                                $seating_data[$arr["ID"]]["main_places"]["adults"] += $arr['UF_MAIN_PLACES'] - $seating_main_total;
                            }

                            if (intVal($arr["UF_TOTAL_COST_CALC"]) !== 1) {
                                //  общая стоимость номера включает доп. места
                                $seating_additional_total = intVal($seating_data[$arr["ID"]]["additional_places"]["adults"] + $seating_data[$arr["ID"]]["additional_places"]["children"]);
                                if ($seating_additional_total < $arr['UF_ADD_PLACES']) {
                                    // дополняем доп места взрослыми, для продажи по полной стоимости за номер 
                                    $seating_data[$arr["ID"]]["additional_places"]["adults"] += $arr['UF_ADD_PLACES'] - $seating_additional_total;
                                }
                            }
                        }

                        // фильтруем номера по вместимости максисум больше указанной в запросе на 1
                        if (intVal($arr["UF_CAPACITY_CALC"]) === 1) {
                            // общая вместимость считается по основным местам
                            if (($arr["UF_MAIN_PLACES"] - $seating_data[$arr["ID"]]["additional_places"]["adults"] - $seating_data[$arr["ID"]]["main_places"]["adults"] - $seating_data[$arr["ID"]]["additional_places"]["children"] - $seating_data[$arr["ID"]]["main_places"]["children"]) > 1) {
                                unset($seating_data[$arr["ID"]]);
                            }
                        } else {
                            // общая вместимость считается по основным местам + доп. местам
                            if (($arr["UF_MAIN_PLACES"] + $arr["UF_ADD_PLACES"]) - ($seating_data[$arr["ID"]]["additional_places"]["adults"] + $seating_data[$arr["ID"]]["main_places"]["adults"] + $seating_data[$arr["ID"]]["additional_places"]["children"] + $seating_data[$arr["ID"]]["main_places"]["children"]) > 1) {
                                unset($seating_data[$arr["ID"]]);
                            }
                        }
                    }

                    $arr_services_id = array_keys($seating_data);
                    if (!empty($arr_services_id)) {

                        $arr_price_types_id = $arr_rates_id = $prices_data = array();

                        $arr_filter = array(
                            "UF_SERVICE_ID" => $arr_services_id,
                            "UF_SERVICE_TYPE" => "rooms",
                            "><UF_DATE" => array(
                                \travelsoft\booking\adapters\Date::create($request->date_from),
                                \travelsoft\booking\adapters\Date::create($date_to)
                            )
                        );

                        if (!empty($request->rates_id)) {
                            $arr_filter["UF_RATE_ID"] = $request->rates_id;
                        }

                        $arr_grouped_price_types_by_services_rates = array();
                        // получаем данные для расчета цен
                        foreach (\travelsoft\booking\stores\Prices::get(array(
                            "filter" => $arr_filter,
                            "order" => array("UF_DATE" => "ASC")
                        )) as $arr) {

                            if (!$prices_data[$arr["UF_SERVICE_ID"]][$arr["UF_RATE_ID"]]) {
                                $prices_data[$arr["UF_SERVICE_ID"]][$arr["UF_RATE_ID"]] = array(
                                    "date_from" => $request->date_from,
                                    "date_to" => $request->date_to,
                                    "prices" => null,
                                    "seating" => $seating_data[$arr["UF_SERVICE_ID"]],
                                    "price_types" => null,
                                    "calc_types" => null,
                                    "currency" => null
                                );
                            }

                            $prices_data[$arr["UF_SERVICE_ID"]][$arr["UF_RATE_ID"]]["prices"][$arr["UF_PRICE_TYPE_ID"]][] = array(
                                "gross" => $arr["UF_GROSS"],
                                "netto" => $arr["UF_NETTO"]
                            );
                        }

                        // фильтруем по пустым значениям цены
                        foreach ($prices_data as $service_id => $arr_) {
                            foreach ($arr_ as $rate_id => $arr__) {
                                foreach ($arr__["prices"] as $ptid => $arr_prices) {

                                    $involve = true;
                                    foreach ($arr_prices as $arr_price) {
                                        if ($arr_price["gross"] <= 0) {
                                            $involve = false;
                                            break;
                                        }
                                    }
                                    if (!$involve) {
                                        unset($prices_data[$service_id][$rate_id]["prices"][$ptid]);
                                    } else {
                                        $arr_price_types_id[] = $ptid;
                                    }
                                }
                                if (empty($prices_data[$service_id][$rate_id]["prices"])) {
                                    unset($prices_data[$service_id][$rate_id]);
                                } else {

                                    $arr_rates_id[] = $rate_id;
                                }
                            }
                            if (empty($prices_data[$service_id])) {
                                unset($prices_data[$service_id]);
                            }
                        }

                        // учитываем особенности по одноместному размещению
                        // если в запросе количество человек равно 1,
                        // то оствляем только цены за одноместное размещение
                        if ($request->adults + $request->children === 1 && !empty($prices_data)) {
                            $arr_tmp_prices_data = array();
                            foreach ($prices_data as $service_id => &$arr_) {
                                foreach ($arr_ as $rate_id => &$arr__) {
                                    if (isset($arr__["prices"][\travelsoft\booking\Settings::getSingleOccupancyPriceTypeId()])) {
                                        $arr_tmp_prices_data = $arr__["prices"][\travelsoft\booking\Settings::getSingleOccupancyPriceTypeId()];
                                        $arr__["prices"] = array(
                                            \travelsoft\booking\Settings::getSingleOccupancyPriceTypeId() => $arr_tmp_prices_data
                                        );
                                        // приводим в порядок рассадку по местам принудительно при одноместном размещении
                                        $arr__["seating"]["main_places"]["adults"] = 1;
                                        $arr__["seating"]["main_places"]["children"] = 0;
                                        $arr__["seating"]["additional_places"]["adults"] = 0;
                                        $arr__["seating"]["additional_places"]["children"] = 0;
                                        $arr__["seating"]["without_places"]["adults"] = 0;
                                        $arr__["seating"]["without_places"]["children"] = 0;
                                    }
                                }
                            }
                        } else {
                            foreach ($prices_data as $service_id => &$arr_) {
                                foreach ($arr_ as $rate_id => &$arr__) {
                                    if (isset($arr__["prices"][\travelsoft\booking\Settings::getSingleOccupancyPriceTypeId()])) {
                                        unset($arr__["prices"][\travelsoft\booking\Settings::getSingleOccupancyPriceTypeId()]);
                                    }
                                }
                            }
                        }

                        if (!empty($prices_data)) {

                            // получение валюты
                            $arr_rates = \travelsoft\booking\stores\Rates::get(array(
                                        "filter" => array("ID" => $arr_rates_id),
                                        "select" => array("UF_CURRENCY", "ID")
                            ));

                            // получение данных по типам цен
                            $arr_price_types = \travelsoft\booking\stores\PriceTypes::get(array(
                                        "filter" => array(
                                            "ID" => $arr_price_types_id
                                        ),
                                        "select" => array("UF_MAX_AGE", "UF_MIN_AGE", "UF_CALC_TYPE", "ID")
                            ));

                            foreach ($prices_data as $service_id => &$arr_) {
                                foreach ($arr_ as $rate_id => &$arr__) {
                                    if (isset($arr_rates[$rate_id])) {
                                        $arr__["currency"] = $arr_rates[$rate_id]["UF_CURRENCY"];
                                    }
                                    foreach (array_keys($arr__["prices"]) as $pt_id) {
                                        if (isset($arr_price_types[$pt_id])) {
                                            $arr__["price_types"][$pt_id] = $arr_price_types[$pt_id];
                                            $arr_calc_types_id[] = $arr_price_types[$pt_id]["UF_CALC_TYPE"];
                                        }
                                    }
                                }
                            }

                            if ($arr_calc_types_id) {
                                $arr_calc_types = \travelsoft\booking\stores\CalculationTypes::get(array(
                                            "filter" => array("ID" => array_unique($arr_calc_types_id))
                                ));

                                foreach ($prices_data as $service_id => &$arr_) {
                                    foreach ($arr_ as $rate_id => &$arr__) {
                                        foreach ($arr__["price_types"] as &$arr___) {
                                            $arr__["calc_types"][$arr___["UF_CALC_TYPE"]] = $arr_calc_types[$arr___["UF_CALC_TYPE"]]["UF_METHOD"];
                                        }
                                    }
                                }
                            }

                            return new Calculator($prices_data);
                        }
                    }
                }
            }
        }

        return new Calculator(array());
    }

}
