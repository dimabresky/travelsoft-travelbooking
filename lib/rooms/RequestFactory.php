<?php

namespace travelsoft\booking\rooms;

/**
 * Класс-фабрика для класса запроса
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class RequestFactory extends \travelsoft\booking\RequestFactory{

    /**
     * @param array $request
     * @return \travelsoft\booking\Request
     */
    public static function create(array $request) {

        $oRequest = parent::create($request);

        return $oRequest;
    }

}
