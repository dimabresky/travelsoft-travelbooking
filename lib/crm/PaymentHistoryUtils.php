<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с платежами
 *
 * @author dimabresky
 */
class PaymentHistoryUtils extends Utils {

    /**
     * Возвращает HTML полей для формы редактирования истории платежей
     * @param array $data
     * @return string
     */
    public static function getPaymentHistoryEditFieldsContent(array $data): string {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::paymentHistoryStoreId(), $data);

        $isFormRequest = self::isEditFormRequest();

        $content = '';
        
        foreach ($arUserFields as $arUserField) {

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            switch ($arUserField['FIELD_NAME']) {

                case "UF_PRICE":

                    $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ":", '<input type="text" name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField['VALUE'] . '">', true);

                    break;

                case 'UF_CURRENCY':

                    $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ":", self::getCurrencySelect($arUserField["FIELD_NAME"], $arUserField["VALUE"]), true);

                    break;

                case "UF_CASH_DESK_ID":

                    $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ":", \SelectBoxFromArray($arUserField["FIELD_NAME"], self::getReferencesSelectData(
                                                    stores\CashDesks::get(
                                                            array('select' => array('ID', 'UF_NAME'))), "UF_NAME", "ID"), $arUserField["VALUE"], "", '', false, "payment_history_form"), true);

                    break;

                case "UF_PAYMENT_TYPE":

                    $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ":", \SelectBoxFromArray($arUserField["FIELD_NAME"], self::getReferencesSelectData(
                                                    \travelsoft\booking\stores\PaymentsTypes::get(
                                                            array('select' => array('ID', 'UF_NAME'))), "UF_NAME", "ID"), $arUserField["VALUE"], "", '', false, "payment_history_form"), true);
                    break;

                case "UF_ORDER_ID":

                    $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ":", \SelectBoxFromArray($arUserField["FIELD_NAME"], self::getReferencesSelectData(
                                                    \travelsoft\booking\stores\Vouchers::get(
                                                            array('select' => array('ID'))), "ID", "ID"), $arUserField["VALUE"], "", '', false, "payment_history_form"), true);

                    break;

                case "UF_CREATER":

                    if ($arUserField['VALUE'] > 0) {

                        $content .= self::getEditFieldHtml(
                                        $arUserField['EDIT_FORM_LABEL'] . ':', \travelsoft\booking\stores\Users::getFullUserNameWithEmailByFields(\travelsoft\booking\stores\Users::getById($arUserField['VALUE'])));
                    }
                    break;
                case "UF_DATE_CREATE":

                    if ($arUserField['VALUE'] > 0) {

                        $content .= self::getEditFieldHtml(
                                        $arUserField['EDIT_FORM_LABEL'] . ':', $arUserField['VALUE']);
                    }
                    break;
                case "UF_COURSE_INFO":
                    break;

                default:
                    $content .= \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml($isFormRequest, $arUserField);
            }
        }

        return $content;
    }
    
    public static function processingPaymentHistoryEditForm() {

        $url = Settings::PAYMENT_HISTORY_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {
            
            $data = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::paymentHistoryStoreId(), array());

            if ($data['UF_PRICE'] <= 0) {

                $arErrors[] = 'Укажите внесенную сумму';
            }

            if (!in_array($data['UF_CURRENCY'], array_keys(\travelsoft\booking\adapters\CurrencyConverter::getAcceptableISO()))) {

                $arErrors[] = 'Укажите валюту';
            }

            if ($data['UF_CASH_DESK_ID'] <= 0) {

                $arErrors[] = 'Укажите кассу';
            }

            if ($data['UF_PAYMENT_TYPE'] <= 0) {

                $arErrors[] = 'Укажите тип платежа';
            }

            if ($data['UF_ORDER_ID'] <= 0) {

                $arErrors[] = 'Укажите номер путевки';
            }

            if (empty($arErrors)) {

                if ($_REQUEST['ID'] > 0) {

                    $ID = intval($_REQUEST['ID']);

                    $result = \travelsoft\booking\stores\PaymentHistory::update($ID, $data);
                } else {

                    $data['UF_COURSE_INFO'] = \travelsoft\booking\Utils::ats(
                                    array(
                                        'COURSE_ID' => \travelsoft\booking\adapters\CurrencyConverter::getCurrentCourseId(),
                                        'COMMISSIONS' => \travelsoft\booking\adapters\CurrencyConverter::getCommissions())
                    );
                    $data['UF_CREATER'] = $GLOBALS['USER']->GetID();
                    $data['UF_DATE_CREATE'] = date('d.m.Y H:i:s');
                    $result = \travelsoft\booking\stores\PaymentHistory::add($data);
                }

                if ($result) {

                    LocalRedirect($url);
                }
            }
        }


        return array('errors' => $arErrors, 'result' => $result);
    }

    /**
     * Возвращает фильтр для выборки списка истории платежей
     * @return array
     */
    public static function getPaymentHistoryFilter(): array {

        if (strlen($_REQUEST['CANCEL']) > 0) {
            \LocalRedirect($GLOBALS['APPLICATION']->GetCurPageParam("", array(
                        'UF_DATE_CREATE',
                        'UF_ORDER_ID',
                        'UF_CREATER',
                        'UF_CASH_DESK_ID',
                        'UF_PAYMENT_TYPE',
                        'UF_CURRENCY',
                        'CANCEL'
            )));
        }

        $filter = array();

        if (strlen($_REQUEST['UF_DATE_CREATE'][0]) > 0 && strlen($_REQUEST['UF_DATE_CREATE'][1]) > 0) {

            $filter['><UF_DATE_CREATE'] = array(
                \travelsoft\booking\adapters\Date::create($_REQUEST['UF_DATE_CREATE'][0]),
                \travelsoft\booking\adapters\Date::create($_REQUEST['UF_DATE_CREATE'][1])
            );
        } elseif (strlen($_REQUEST['UF_DATE_CREATE'][0]) > 0) {

            $filter['>=UF_DATE_CREATE'] = \travelsoft\booking\adapters\Date::create($_REQUEST['UF_DATE_CREATE'][0]);
        } elseif (strlen($_REQUEST['UF_DATE_CREATE'][1]) > 0) {

            $filter['<=UF_DATE_CREATE'] = \travelsoft\booking\adapters\Date::create($_REQUEST['UF_DATE_CREATE'][1]);
        }

        $intFields = array(
            'UF_ORDER_ID',
            'UF_CREATER',
            'UF_CASH_DESK_ID',
            'UF_PAYMENT_TYPE'
        );

        foreach ($intFields as $field) {
            if ($_REQUEST[$field] > 0) {
                $filter[$field] = $_REQUEST[$field];
            }
        }

        if (strlen($_REQUEST['UF_CURRENCY']) > 0) {

            $filter['UF_CURRENCY'] = $_REQUEST['UF_CURRENCY'];
        }

        return $filter;
    }
}
