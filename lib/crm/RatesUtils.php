<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с тарифами
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class RatesUtils extends Utils {

    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::RATES_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::ratesStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::ratesStoreId(), array());
            
            $arFields = array_keys($arUserFieldsData);
            
            foreach ($arFields as $field) {

                if ($field === "UF_NAME") {
                    Validator::stringLessThenTwo($arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_SERVICES_TYPE") {
                    Validator::strIsEmpty((string) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_CURRENCY") {
                    Validator::issetKeyArray((string) $arSave[$field], (new \travelsoft\booking\adapters\CurrencyConverter)->getAcceptableISO(), $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_PRICE_TYPES") {
                    Validator::numericNotEmpty((int) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
            }
                        
            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\Rates::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\Rates::add($arSave);
                }
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getEditFormFields($arData) {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::ratesStoreId(), $arData);

        $fields = array();
        
        $type = "placements";
        foreach ($arUserFields as $arUserField) {

            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            if ($arUserField['FIELD_NAME'] === "UF_SERVICES_TYPE") {

                if (strlen($arUserField['VALUE']) && $arUserField['VALUE'] !== "placements") {
                    $type = $arUserField['VALUE'];
                }

                $serviceTypes = \travelsoft\booking\Settings::getServicesTypesWithout(array("packageTour"));

                $formField["view"] = \SelectBoxFromArray("UF_SERVICES_TYPE", self::getReferencesSelectData($serviceTypes, "name", "type"), $type, "", 'data-services-target="select[name=\'UF_SERVICES[]\']" id="UF_SERVICES_TYPE" class="select2"');


                $formField["required"] = true;
            } elseif ($arUserField['FIELD_NAME'] === "UF_SERVICES") {

                $method = "get" . ucfirst($type) . "SelectView";
                $formField["view"] = self::$method("UF_SERVICES[]", $arUserField["VALUE"], true);
                
                foreach (\travelsoft\booking\crm\Utils::getBookingServices(
                        array_keys(\travelsoft\booking\Settings::getServicesTypesWithout(array("packageTour")))) as $type => $arr) {
                    
                    $jsBookingServices[$type] = \travelsoft\booking\crm\Utils::getReferencesSelectData($arr, "NAME", "ID");
                }
                
                $formField["view"] .= "<script>var jsBookingServices = " . json_encode($jsBookingServices) . "</script>";

                $formField["required"] = false;
            } elseif ($arUserField['FIELD_NAME'] === "UF_CURRENCY") {

                $formField["view"] = \SelectBoxFromArray("UF_CURRENCY", self::getReferencesSelectData((new \travelsoft\booking\adapters\CurrencyConverter)->getAcceptableISO(), "TITLE", "ISO"), $arUserField["VALUE"], "", 'id="UF_CURRENCY" class="select2"');

                $formField["required"] = true;
            } elseif ($arUserField['FIELD_NAME'] === "UF_PRICE_TYPES") {
                
                $formField["view"] = \SelectBoxMFromArray("UF_PRICE_TYPES[]", self::getReferencesSelectData(self::getPriceTypes(), "UF_NAME", "ID"), $arUserField["VALUE"], "", "", "", 'id="UF_PRICE_TYPES" class="select2"');

                $formField["required"] = true;
            } elseif ($arUserField['FIELD_NAME'] === "UF_FOOD") {
                
                $formField["view"] = self::getFoodSelectView("UF_FOOD", $arUserField["VALUE"], false);
            } else {

                $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
                $formField["required"] = true;
            }

            $fields[] = $formField;
        }
        return $fields;
    }
    
    
    /**
     * Устанавливает в массив информацию по размещению
     * @param array $arPlacements
     * @param int $id
     */
    public static function setPlacement (&$arPlacements, $id) {
        if (!isset($arPlacements[$id])) {
            $arPlacements[$id] = current(\travelsoft\booking\stores\Placements::get(array(
                "filter" => array("ID" => $id),
                "select" => array("ID", "NAME")
            )));
        }
    }

}
