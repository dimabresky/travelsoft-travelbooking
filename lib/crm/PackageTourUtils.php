<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с пакетными турами
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class PackageTourUtils extends Utils {

    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::PACKAGE_TOUR_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::packageTourStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::packageTourStoreId(), array());
            
            $arFields = array_keys($arUserFieldsData);
            
            foreach ($arFields as $field) {
                
                if ($field === "UF_TOUR") {
                    Validator::numericNotEmpty((int) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_PLACEMENT") {
                    Validator::numericNotEmpty((int) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_TRANSFER" || $field === "UF_TRANSFER_BACK") {
                    Validator::numericNotEmpty((int) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_TRANS_RATE" || $field === "UF_TRANS_BACK_RATE") {
                    Validator::numericNotEmpty((int) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_TRAVEL_DAYS") {
                    $arSave["UF_TRAVEL_DAYS"] = intVal($arSave["UF_TRAVEL_DAYS"]);
                }
            }
                        
            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\PackageTour::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\PackageTour::add($arSave);
                }
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getEditFormFields($arData) {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::packageTourStoreId(), $arData);

        $fields = array();
        
        foreach ($arUserFields as $code => $arUserField) {
            
            if ($code === "UF_TRANS_RATE" || $code === "UF_TRANS_BACK_RATE") {
                continue;
            }
            
            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            if ($arUserField['FIELD_NAME'] === "UF_TRANSFER" || $arUserField['FIELD_NAME'] === "UF_TRANSFER_BACK") {

                $formField["view"] = self::getTransferSelectView($arUserField['FIELD_NAME'], $arUserField["VALUE"]);
                $formField["required"] = true;

            } elseif ($arUserField['FIELD_NAME'] === "UF_PLACEMENT") {

                $formField["view"] = self::getPlacementsSelectView("UF_PLACEMENT", $arUserField["VALUE"]);

                $formField["required"] = true;
            } elseif ($arUserField['FIELD_NAME'] === "UF_TOUR") {
                $formField["view"] = self::getTravelSelectView("UF_TOUR", $arUserField["VALUE"]);

                $formField["required"] = true;
            } else {
                
                $formField["view"] = '<input type="text" name="'.$arUserField['FIELD_NAME'].'" value="'.$arUserField["VALUE"].'">';

                $formField["required"] = false;
            }
            
            $fields[] = $formField;
            
            if ($arUserField['FIELD_NAME'] === "UF_TRANSFER") {
                
                $fields[] = array(
                    "label" => $arUserFields["UF_TRANS_RATE"]["EDIT_FORM_LABEL"],
                    "view" => self::getRatesSelectView("UF_TRANS_RATE", $arUserFields["UF_TRANS_RATE"]['VALUE'], false, "transfer"),
                    "required" => true
                );
                
            } elseif ($arUserField['FIELD_NAME'] === "UF_TRANSFER_BACK") {
                
                $fields[] = array(
                    "label" => $arUserFields["UF_TRANS_BACK_RATE"]["EDIT_FORM_LABEL"],
                    "view" => self::getRatesSelectView("UF_TRANS_BACK_RATE", $arUserFields["UF_TRANS_BACK_RATE"]['VALUE'], false, "transferback"),
                    "required" => true
                );
            }
            
        }
        return $fields;
    }

}
