<?php

namespace travelsoft\booking\crm;

/**
 * Валидатор полей для crm
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Validator {
    
    /**
     * Строка >= 2 символов ?
     * @param string $value
     * @param string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public static function stringLessThenTwo(string $value, string $fieldName, array &$arErrors) {

        if (strlen($value) < 2) {

            $arErrors[] = 'Поле "' . $fieldName . '" должно быть не менее 2-ух символов';
            return false;
        }
        return true;
    }
    
    /**
     * Проверка телефона
     * @param string $value
     * @param string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public static function checkPhone(string $value, string $fieldName, array &$arErrors) {

        if (preg_match(\travelsoft\booking\Settings::PHONE_REG, $value) !== 1) {

            $arErrors[] = 'Поле "' . $fieldName . '" должно соответствовать формату (+XXX XX XXXXXXX)';
            return false;
        }
        return true;
    }

    /**
     * Проверка email
     * @param mixed string $value
     * @param mixed string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public static function checkEmail(string $value, string $fieldName, array &$arErrors) {

        if (!check_email($value)) {

            $arErrors[] = 'Правильно заполните поле "' . $fieldName . '"';
            return false;
        }
        return true;
    }

    /**
     * Проверка email на уникальность
     * @param string $value
     * @param int $userId
     * @param array $arErrors
     * @return boolean
     */
    public static function checkEmailOnUniq(string $value, int $userId = null, array &$arErrors) {

        $arUsers = Users::get(array('filter' => array('EMAIL' => $value, "!ID" => $userId), 'select' => array('ID')));

        if (!empty($arUsers)) {
            $arErrors[] = 'Такой email уже зарегистрирован в системе';
            return false;
        }
        return true;
    }
    
    /**
     * Числовое поле > 0 ?
     * @param int $value
     * @param string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public static function numericNotEmpty (int $value, string $fieldName, array &$arErrors) {
        
        if ($value <= 0) {

            $arErrors[] = 'Поле "' . $fieldName . '" не может быть пустым';
            return false;
        }
        return true;
    }
    
    /**
     * Числовое поле >= 0 ?
     * @param $value
     * @param string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public static function numericMoreOrEqualZero ($value, string $fieldName, array &$arErrors) {
        
        if ($value < 0) {

            $arErrors[] = 'Поле "' . $fieldName . '" не может быть пустым';
            return false;
        }
        return true;
    }
    
    
    
    /**
     * Пуста ли строка ?
     * @param string $value
     * @param string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public static function strIsEmpty (string $value, string $fieldName, array &$arErrors) {
        
        if (!strlen($value)) {
            $arErrors[] = 'Поле "' . $fieldName . '" не может быть пустым';
            return false;
        }
        return true;
    }
    
    /**
     * Установлен ли ключ массива ?
     * @param string $value
     * @param array $arr
     * @param string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public function issetKeyArray(string $value, array $arr, string $fieldName, array &$arErrors) {
        if (!isset($arr[$value])) {
            $arErrors[] = 'Поле "' . $fieldName . '" не может быть пустым';
        
            return false;
        }
        return true;
    }
    
    /**
     * Пустой ли массив ?
     * @param array $value
     * @param string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public function arrayIsEmpty (array $value, string $fieldName, array &$arErrors) {
        $value = array_filter($value, function ($val) {return $val > 0;});
        if (empty($value)) {
            $arErrors[] = 'Поле "' . $fieldName . '" не может быть пустым';
            return false;
        }
        return true;
    }
    
    /**
     * Проверка на валидность даты
     * @param string $value
     * @param string $fieldName
     * @param array $arErrors
     * @return boolean
     */
    public function checkDate (string $value, string $fieldName, array &$arErrors) {
        
        if (preg_match(\travelsoft\booking\Settings::DATE_REG, $value) !== 1) {

            $arErrors[] = 'Поле "' . $fieldName . '" должно соответствовать формату DD.MM.YYYY';
            return false;
        }
        
        return true;
    }
    
    /**
     * Проверка пароля
     * @param string $password
     * @param string $confirm_password
     * @param array $arErrors
     * @param array $groups
     * @return boolean
     */
    public function checkPassword (string $password, string $confirm_password, array &$arErrors, array $groups = array()) {
        if ($password !== $confirm_password) {
            $arErrors[] = "Введенный пароль не соответвует введенному подтверждению пароля";
        }

        $err = \travelsoft\booking\adapters\User::checkPasswordAgainstPolicy($password, $groups);
        if (!empty($err)) {
            $arErrors[] = implode("<br>", $err);
        }
        
        return empty($arErrors);
    }
    
    
    /**
     * @param string $service_type
     * @return boolean
     */
    public static function isTransferServiceType(string $service_type = null) {

        return in_array($service_type, array("transfer", "transferback"));
    }
}
