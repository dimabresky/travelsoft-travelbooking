<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с номерным фондом
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class RoomsUtils extends Utils {
    
    static public function getFilterList () {
        
        if (strlen($_REQUEST['CANCEL']) > 0) {
            \LocalRedirect($GLOBALS['APPLICATION']->GetCurPageParam("", array(
                        'UF_PLACEMENT',
                        'CANCEL'
            )));
        }

        $filter = array();
        if ($_REQUEST['UF_PLACEMENT'] > 0) {
            $filter['UF_PLACEMENT'] = $_REQUEST['UF_PLACEMENT'];
        }

        return $filter;
    }
    
    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::ROOMS_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {
            
            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::roomsStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::roomsStoreId(), array());

            foreach ($arSave as $field => $value) {

                if ($field === "UF_NAME") {
                    Validator::stringLessThenTwo($value, $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_PLACEMENT" || $field === "UF_MAIN_PLACES") {
                    Validator::numericNotEmpty((int)$value, $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_ADD_PLACES") {
                    Validator::numericMoreOrEqualZero((int)$value, $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
            }

            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\Rooms::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\Rooms::add($arSave);
                }
                
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getEditFormFields($arData) {
//        dm($arData);
        $arData["UF_CAPACITY_CALC"] = intVal($arData["UF_CAPACITY_CALC"]);
        $arData["UF_TOTAL_COST_CALC"] = intVal($arData["UF_TOTAL_COST_CALC"]);
        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::roomsStoreId(), $arData);

        $fields = array();

        foreach ($arUserFields as $arUserField) {

            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            if ($arUserField["FIELD_NAME"] === "UF_DESCRIPTION") {

                $LHE = new \CHTMLEditor;
                ob_start();
                $LHE->Show(array(
                    'name' => $arUserField["FIELD_NAME"],
                    'id' => $arUserField["FIELD_NAME"],
                    'inputName' => $arUserField["FIELD_NAME"],
                    'content' => $arUserField['VALUE'],
                    'width' => '100%',
                    'minBodyWidth' => 350,
                    'normalBodyWidth' => 555,
                    'height' => '75',
                    'bAllowPhp' => false,
                    'limitPhpAccess' => false,
                    'autoResize' => true,
                    'autoResizeOffset' => 40,
                    'useFileDialogs' => false,
                    'saveOnBlur' => true,
                    'showTaskbars' => false,
                    'showNodeNavi' => false,
                    'askBeforeUnloadPage' => true,
                    'bbCode' => false,
                    'siteId' => SITE_ID,
                    'controlsMap' => array(
                        array('id' => 'Bold', 'compact' => true,
                            'sort' => 80),
                        array('id' => 'Italic', 'compact' => true,
                            'sort' => 90),
                        array('id' => 'Underline', 'compact' => true,
                            'sort' => 100),
                        array('id' => 'Strikeout', 'compact' => true,
                            'sort' => 110),
                        array('id' => 'RemoveFormat', 'compact' => true,
                            'sort' => 120),
                        array('id' => 'Color', 'compact' => true,
                            'sort' => 130),
                        array('id' => 'FontSelector', 'compact' => false,
                            'sort' => 135),
                        array('id' => 'FontSize', 'compact' => false,
                            'sort' => 140),
                        array('separator' => true, 'compact' => false,
                            'sort' => 145),
                        array('id' => 'OrderedList', 'compact' => true,
                            'sort' => 150),
                        array('id' => 'UnorderedList', 'compact' => true,
                            'sort' => 160),
                        array('id' => 'AlignList', 'compact' => false,
                            'sort' => 190),
                        array('separator' => true, 'compact' => false,
                            'sort' => 200),
                        array('id' => 'InsertLink', 'compact' => true,
                            'sort' => 210),
                        array('id' => 'InsertImage', 'compact' => false,
                            'sort' => 220),
                        array('id' => 'InsertVideo', 'compact' => true,
                            'sort' => 230),
                        array('id' => 'InsertTable', 'compact' => false,
                            'sort' => 250),
                        array('separator' => true, 'compact' => false,
                            'sort' => 290),
                        array('id' => 'Fullscreen', 'compact' => false,
                            'sort' => 310),
                        array('id' => 'More', 'compact' => true,
                            'sort' => 400)
                    ),
                ));
                $formField["view"] = ob_get_clean();
                $formField["required"] = false;
            } elseif ($arUserField["FIELD_NAME"] === "UF_PLACEMENT") {

                $formField["view"] = self::getPlacementsSelectView("UF_PLACEMENT", $arUserField["VALUE"]);
                $formField["required"] = true;
            } elseif ($arUserField["USER_TYPE_ID"] === "string" || $arUserField["USER_TYPE_ID"] === "integer") {

                $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
                $formField["required"] = true;
            } elseif ($arUserField["FIELD_NAME"] === "UF_CAPACITY_CALC" || $arUserField["FIELD_NAME"] === "UF_TOTAL_COST_CALC") {

                $formField["view"] = \SelectBoxFromArray($arUserField["FIELD_NAME"], array(
                    "REFERENCE" => array_values($arUserField["SETTINGS"]["LABEL"]),
                    "REFERENCE_ID" => array_keys($arUserField["SETTINGS"]["LABEL"])), $arUserField["VALUE"], "", 'id="' . $arUserField["FIELD_NAME"] . '"');
                $formField["required"] = false;
            } else {
                $formField["view"] = \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml(Utils::isEditFormRequest(), $arUserField);
                $formField["label"] = "";
                $formField["required"] = false;
            }

            $fields[] = $formField;
        }
        return $fields;
    }

}
