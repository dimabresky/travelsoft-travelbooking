<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с экскурсионными турами
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class ExcursionTourUtils extends Utils {

    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::EXCURSION_TOUR_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::excursionTourStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::excursionTourStoreId(), array());

            $arFields = array_keys($arUserFieldsData);

            foreach ($arFields as $field) {
                
                if ($field === "UF_TOUR") {
                    Validator::numericNotEmpty((int) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_NAME") {
                    Validator::strIsEmpty((string) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
            }
            
            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\ExcursionTour::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\ExcursionTour::add($arSave);
                }
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getEditFormFields($arData) {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::excursionTourStoreId(), $arData);

        $fields = array();
        foreach ($arUserFields as $fieldName => $arUserField) {
                        
            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            if ($arUserField['FIELD_NAME'] === "UF_TOUR") {
                $formField["view"] = self::getExcursionTourIBSelectView("UF_TOUR", $arUserField["VALUE"]);

                $formField["required"] = true;
            } else {
            
                $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
                $formField["required"] = true;
            }

            $fields[] = $formField;
        }
        return $fields;
    }

}
