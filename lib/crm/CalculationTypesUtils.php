<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с типами расчета
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class CalculationTypesUtils extends Utils {

    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::CALCULATION_TYPES_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::calculationTypesStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::calculationTypesStoreId(), array());

            foreach ($arSave as $field => $value) {

                if ($field === "UF_NAME" || $field === "UF_METHOD") {
                    Validator::stringLessThenTwo($value, $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
            }

            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\CalculationTypes::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\CalculationTypes::add($arSave);
                }
                
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getEditFormFields($arData) {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::calculationTypesStoreId(), $arData);

        $fields = array();

        foreach ($arUserFields as $arUserField) {

            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }


            $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
            $formField["required"] = true;

            $fields[] = $formField;
        }
        return $fields;
    }
    
}
