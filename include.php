<?php

$classes = array(
    "travelsoft\\booking\\abstraction\\Calculator" => "lib/abstraction/Calculator.php",
    "travelsoft\\booking\\abstraction\\Store" => "lib/abstraction/Store.php",
    "travelsoft\\booking\\abstraction\\CalculatorContainer" => "lib/abstraction/CalculatorContainer.php",
    "travelsoft\\booking\\abstraction\\TemplateProcessor" => "lib/abstraction/TemplateProcessor.php",
    "travelsoft\\booking\\adapters\\ComponentIncluder" => "lib/adapters/ComponentIncluder.php",
    "travelsoft\\booking\\adapters\\Date" => "lib/adapters/Date.php",
    "travelsoft\\booking\\adapters\\Mail" => "lib/adapters/Mail.php",
    "travelsoft\\booking\\adapters\\User" => "lib/adapters/User.php",
    "travelsoft\\booking\\adapters\\Cache" => "lib/adapters/Cache.php",
    "travelsoft\\booking\\adapters\\UserFieldsManager" => "lib/adapters/UserFieldsManager.php",
    "travelsoft\\booking\\adapters\\Highloadblock" => "lib/adapters/Highloadblock.php",
    "travelsoft\\booking\\adapters\\Iblock" => "lib/adapters/Iblock.php",
    "travelsoft\\booking\\adapters\\Mail" => "lib/adapters/Mail.php",
    "travelsoft\\booking\\adapters\\CurrencyConverter" => "lib/adapters/CurrencyConverter.php",
    "travelsoft\\booking\\adapters\\Date" => "lib/adapters/Date.php",
    "travelsoft\\booking\\adapters\\DocxTemplateProcessor" => "lib/adapters/DocxTemplateProcessor.php",
    "travelsoft\\booking\\adapters\\HTMLTemplateProcessor" => "lib/adapters/HTMLTemplateProcessor.php",
    "travelsoft\\booking\\stores\\Food" => "lib/stores/Food.php",
    "travelsoft\\booking\\stores\\ExcursionTourIB" => "lib/stores/ExcursionTourIB.php",
    "travelsoft\\booking\\stores\\ExcursionTour" => "lib/stores/ExcursionTour.php",
    "travelsoft\\booking\\stores\\Markup" => "lib/stores/Markup.php",
    "travelsoft\\booking\\stores\\Vouchers" => "lib/stores/Vouchers.php",
    "travelsoft\\booking\\stores\\CalculationTypes" => "lib/stores/CalculationTypes.php",
    "travelsoft\\booking\\stores\\Bookings" => "lib/stores/Bookings.php",
    "travelsoft\\booking\\stores\\Rates" => "lib/stores/Rates.php",
    "travelsoft\\booking\\stores\\TransferQuotas" => "lib/stores/TransferQuotas.php",
    "travelsoft\\booking\\stores\\PackageTour" => "lib/stores/PackageTour.php",
    "travelsoft\\booking\\stores\\Placements" => "lib/stores/Placements.php",
    "travelsoft\\booking\\stores\\Rooms" => "lib/stores/Rooms.php",
    "travelsoft\\booking\\stores\\Documents" => "lib/stores/Documents.php",
    "travelsoft\\booking\\stores\\PriceTypes" => "lib/stores/PriceTypes.php",
    "travelsoft\\booking\\stores\\Prices" => "lib/stores/Prices.php",
    "travelsoft\\booking\\stores\\Quotas" => "lib/stores/Quotas.php",
    "travelsoft\\booking\\stores\\Statuses" => "lib/stores/Statuses.php",
    "travelsoft\\booking\\stores\\Buses" => "lib/stores/Buses.php",
    "travelsoft\\booking\\stores\\Tourists" => "lib/stores/Tourists.php",
    "travelsoft\\booking\\stores\\Duration" => "lib/stores/Duration.php",
    "travelsoft\\booking\\stores\\Discounts" => "lib/stores/Discounts.php",
    "travelsoft\\booking\\stores\\PaymentsTypes" => "lib/stores/PaymentsTypes.php",
    "travelsoft\\booking\\stores\\PaymentHistory" => "lib/stores/PaymentHistory.php",
    "travelsoft\\booking\\stores\\Users" => "lib/stores/Users.php",
    "travelsoft\\booking\\stores\\Travel" => "lib/stores/Travel.php",
    "travelsoft\\booking\\stores\\Tourservices" => "lib/stores/Tourservices.php",
    "travelsoft\\booking\\Settings" => "lib/Settings.php",
    "travelsoft\\booking\\EventsHandlers" => "lib/EventsHandlers.php",
    "travelsoft\\booking\\Utils" => 'lib/Utils.php',
    "travelsoft\\booking\\Logger" => 'lib/Logger.php',
    "travelsoft\\booking\\Request" => 'lib/Request.php',
    "travelsoft\\booking\\RequestFactory" => 'lib/RequestFactory.php',
    "travelsoft\\booking\\rooms\\RequestFactory" => 'lib/rooms/RequestFactory.php',
    "travelsoft\\booking\\rooms\\Request" => 'lib/rooms/Request.php',
    "travelsoft\\booking\\transfer\\RequestFactory" => 'lib/transfer/RequestFactory.php',
    "travelsoft\\booking\\transfer\\Request" => 'lib/transfer/Request.php',
    "travelsoft\\booking\\transferback\\RequestFactory" => 'lib/transferback/RequestFactory.php',
    "travelsoft\\booking\\transferback\\Request" => 'lib/transferback/Request.php',
    "travelsoft\\booking\\placements\\RequestFactory" => 'lib/placements/RequestFactory.php',
    "travelsoft\\booking\\placements\\Request" => 'lib/placements/Request.php',
    "travelsoft\\booking\\packagetour\\RequestFactory" => 'lib/packagetour/RequestFactory.php',
    "travelsoft\\booking\\packagetour\\Request" => 'lib/packagetour/Request.php',
    "travelsoft\\booking\\excursiontour\\RequestFactory" => 'lib/excursiontour/RequestFactory.php',
    "travelsoft\\booking\\excursiontour\\Request" => 'lib/excursiontour/Request.php',
    "travelsoft\\booking\\CalculationTypes" => 'lib/CalculationTypes.php',
    "travelsoft\\booking\\rooms\\CalculatorFactory" => 'lib/rooms/CalculatorFactory.php',
    "travelsoft\\booking\\rooms\\Calculator" => 'lib/rooms/Calculator.php',
    "travelsoft\\booking\\placements\\CalculatorFactory" => 'lib/placements/CalculatorFactory.php',
    "travelsoft\\booking\\placements\\Calculator" => 'lib/placements/Calculator.php',
    "travelsoft\\booking\\transfer\\CalculatorFactory" => 'lib/transfer/CalculatorFactory.php',
    "travelsoft\\booking\\transfer\\Calculator" => 'lib/transfer/Calculator.php',
    "travelsoft\\booking\\transferback\\CalculatorFactory" => 'lib/transferback/CalculatorFactory.php',
    "travelsoft\\booking\\transferback\\Calculator" => 'lib/transferback/Calculator.php',
    "travelsoft\\booking\\packagetour\\CalculatorFactory" => 'lib/packagetour/CalculatorFactory.php',
    "travelsoft\\booking\\packagetour\\Calculator" => 'lib/packagetour/Calculator.php',
    "travelsoft\\booking\\excursiontour\\CalculatorFactory" => 'lib/excursiontour/CalculatorFactory.php',
    "travelsoft\\booking\\excursiontour\\Calculator" => 'lib/excursiontour/Calculator.php',
    "travelsoft\\booking\\transfer\\CalculatorContainer" => 'lib/transfer/CalculatorContainer.php',
    "travelsoft\\booking\\transferback\\CalculatorContainer" => 'lib/transferback/CalculatorContainer.php',
    "travelsoft\\booking\\placements\\CalculatorContainer" => 'lib/placements/CalculatorContainer.php',
    "travelsoft\\booking\\Basket" => 'lib/Basket.php',
    
    // doccreators
    "travelsoft\\booking\\doccreators\\TemplateProccessorFactory" => "lib/doccreators/TemplateProccessorFactory.php",
    "travelsoft\\booking\\doccreators\\InvoiceFactory" => "lib/doccreators/InvoiceFactory.php",
    "travelsoft\\booking\\doccreators\\ContractFactory" => "lib/doccreators/ContractFactory.php"
);

if (ADMIN_SECTION === true) {

    $classes["travelsoft\\booking\\crm\\Utils"] = "lib/crm/Utils.php";
    $classes["travelsoft\\booking\\crm\\AddPricesUtils"] = "lib/crm/AddPricesUtils.php";
    $classes["travelsoft\\booking\\crm\\BookingsUtils"] = "lib/crm/BookingsUtils.php";
    $classes["travelsoft\\booking\\crm\\RoomsUtils"] = "lib/crm/RoomsUtils.php";
    $classes["travelsoft\\booking\\crm\\MarkupUtils"] = "lib/crm/MarkupUtils.php";
    $classes["travelsoft\\booking\\crm\\CashDeskUtils"] = "lib/crm/CashDeskUtils.php";
    $classes["travelsoft\\booking\\crm\\PaymentTypesUtils"] = "lib/crm/PaymentTypesUtils.php";
    $classes["travelsoft\\booking\\crm\\PaymentHistoryUtils"] = "lib/crm/PaymentHistoryUtils.php";
    $classes["travelsoft\\booking\\crm\\DiscountsUtils"] = "lib/crm/DiscountsUtils.php";
    $classes["travelsoft\\booking\\crm\\ClientUtils"] = "lib/crm/ClientUtils.php";
    $classes["travelsoft\\booking\\crm\\DocumentsUtils"] = "lib/crm/DocumentsUtils.php";
    $classes["travelsoft\\booking\\crm\\TourservicesUtils"] = "lib/crm/TourservicesUtils.php";
    $classes["travelsoft\\booking\\crm\\CalculationTypesUtils"] = "lib/crm/CalculationTypesUtils.php";
    $classes["travelsoft\\booking\\crm\\PriceTypesUtils"] = "lib/crm/PriceTypesUtils.php";
    $classes["travelsoft\\booking\\crm\\PackageTourUtils"] = "lib/crm/PackageTourUtils.php";
    $classes["travelsoft\\booking\\crm\\RatesUtils"] = "lib/crm/RatesUtils.php";
    $classes["travelsoft\\booking\\crm\\BusesUtils"] = "lib/crm/BusesUtils.php";
    $classes["travelsoft\\booking\\crm\\ExcursionTourUtils"] = "lib/crm/ExcursionTourUtils.php";
    $classes["travelsoft\\booking\\crm\\VouchersUtils"] = "lib/crm/VouchersUtils.php";
    $classes["travelsoft\\booking\\crm\\TouristsUtils"] = "lib/crm/TouristsUtils.php";
    $classes["travelsoft\\booking\\crm\\stores\\CashDesks"] = "lib/crm/stores/CashDesks.php";
    $classes["travelsoft\\booking\\crm\\Settings"] = "lib/crm/Settings.php";
    $classes["travelsoft\\booking\\crm\\Validator"] = "lib/crm/Validator.php";
}

CModule::AddAutoloadClasses("travelsoft.travelbooking", $classes);
