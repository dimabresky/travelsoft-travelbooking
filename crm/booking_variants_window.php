<?php
require_once 'header.php';

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

function getNearestAvailableCalculations() {

    $arr_calculations = array();
    $searchMethod = "make" . ucfirst($_GET["service_type"]) . "SearchRequest";
    $current_duration = travelsoft\booking\Utils::getNightsDuration($_GET["travelbooking"]["date_from"], $_GET["travelbooking"]["date_to"]);
    $source_timestamp_from = strtotime($_GET["travelbooking"]["date_from"]);
    for ($d = 0; $d <= 21; $d++) {

        $timestamp_from = $source_timestamp_from + (86400 * $d);
        for ($n = $current_duration; $n <= ($current_duration + 5); $n++) {

            $_GET["travelbooking"]["date_from"] = date(
                    \travelsoft\booking\Settings::DATE_FORMAT, $timestamp_from);
            $_GET["travelbooking"]["date_to"] = date(travelsoft\booking\Settings::DATE_FORMAT, $timestamp_from + (86400 * $n));

            $arr_calculations = travelsoft\booking\Utils::getCalculationResult(
                            $_GET["service_type"], \travelsoft\booking\Utils::$searchMethod());
            if (!empty($arr_calculations)) {
                return $arr_calculations;
            }
        }
    }
    return $arr_calculations;
}

$APPLICATION->SetTitle('Варианты бронирования');

if ($_GET["service_type"]) {

    $showNotify = false;

    $makeSearchRequestMethod = "make" . ucfirst($_GET["service_type"]) . "SearchRequest";
    $arr_calculations = travelsoft\booking\Utils::getCalculationResult(
                    $_GET["service_type"], \travelsoft\booking\Utils::$makeSearchRequestMethod());

    if (empty($arr_calculations) && !travelsoft\booking\crm\Validator::isTransferServiceType($_GET["service_type"])) {
        $showNotify = !empty($arr_calculations = getNearestAvailableCalculations());
    }

    if (!empty($arr_calculations)) {
        if ($showNotify) {
            CAdminMessage::ShowMessage(array(
                "MESSAGE" => "По данным параметрам поиска не удалось найти подходящие предложения. Возможно Вам будут интересны следующие варианты:",
                "TYPE" => "OK"
            ));
        }
        $converter = new \travelsoft\booking\adapters\CurrencyConverter();
        $arr_rooms = $arr_excursion_tour = $arr_rates = $arr_food = array();
        $VC = \travelsoft\booking\Settings::getVoucherCurrency();
        ?>
        <style>
            #booking-table {
                border-collapse: collapse;
            }
            #booking-table td,  #booking-table th{
                border-bottom: 1px solid #000;
                padding: 10px;
            }
        </style>
        
        <table id="booking-table">

            <?
            switch ($_GET["service_type"]) {

                case "packageTour":
                    ?>
                    <thead>
                        <tr>
                            <th>Номер</th>
                            <th>Тариф номера</th>
                            <th>Общая стоимость</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($arr_calculations as $arr_pt_grouped) {
                            foreach ($arr_pt_grouped as $arr_calculation) {
                                if (!isset($arr_rooms[$arr_calculation["room_id"]])) {
                                    $arr_rooms[$arr_calculation["room_id"]] = current(\travelsoft\booking\stores\Rooms::get(
                                                    array("filter" => array("ID" => $arr_calculation["room_id"]), "select" => array("ID", "UF_NAME"))));
                                }

                                if (!isset($arr_rates[$arr_calculation["placement_rate_id"]])) {
                                    $arr_rates[$arr_calculation["placement_rate_id"]] = current(travelsoft\booking\stores\Rates::get(
                                                    array(
                                                        "filter" => array("ID" => $arr_calculation["placement_rate_id"]),
                                                        "select" => array("UF_NAME", "ID", "UF_FOOD")
                                    )));
                                    if ($arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"] > 0 && !isset($arr_food[$arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]])) {
                                        $arr_food[$arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]] = current(\travelsoft\booking\stores\Food::get(
                                                        array(
                                                            "filter" => array("ID" => $arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]),
                                                            "select" => array("ID", "NAME")
                                        )));
                                    }
                                }

                                $cost = $converter->convertWithFormatting($arr_calculation["result_price"], $arr_calculation["currency"], $VC);

                                $booking_data = array(
                                    "UF_COST" => $converter->format((float) $converter->convert($arr_calculation["gross"], $arr_calculation["currency"], $VC)),
                                    "UF_NETTO" => $converter->format((float) $converter->convert($arr_calculation["netto"], $arr_calculation["currency"], $VC)),
                                    "UF_CURRENCY" => $VC,
                                    "UF_DATE_FROM_PLACE" => $arr_calculation["date_from"],
                                    "UF_DATE_TO_PLACE" => $arr_calculation["date_to"],
                                    "UF_PLACEMENT_RATE" => $arr_calculation["placement_rate_id"],
                                    "UF_TRANS_RATE" => $arr_calculation["transfer_rate_id"],
                                    "UF_TRANS_BACK_RATE" => $arr_calculation["transferback_rate_id"],
                                    "UF_DATE_FROM_TRANS" => $arr_calculation["date_from_transfer"],
                                    "UF_DATE_BACK_TRANS" => $arr_calculation["date_back_transfer"],
                                    "UF_FOOD" => isset($arr_food[$arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]]["NAME"]) ? $arr_food[$arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]]["NAME"] : ""
                                );
                                $booking_data["UF_TS_PRICE"] = 0.00;
                                $booking_data["UF_TS_CURRENCY"] = "BYN";
                                if ($arr_calculation["tourservice_for_adults"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] += $arr_calculation["tourservice_for_adults"];
                                }
                                if ($arr_calculation["tourservice_for_children"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] += $arr_calculation["tourservice_for_children"];
                                }
                                $booking_data["UF_TS_CURRENCY"] = "BYN";
                                if ($booking_data["UF_TS_PRICE"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] = $converter->format((float) $converter->convert($booking_data["UF_TS_PRICE"], $arr_calculation["tourservice_currency"], "BYN"));
                                }
                                $booking_data["UF_MARKUP_PRICE"] = 0.00;
                                $booking_data["UF_MARKUP_CURRENCY"] = "BYN";
                                if ($arr_calculation["markup_price"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_MARKUP_PRICE"] = $converter->format((float) $converter->convert($arr_calculation["markup_price"], $arr_calculation["markup_currency"], $VC));
                                    $booking_data["UF_MARKUP_CURRENCY"] = $VC;
                                }
                                $booking_data["UF_DISCOUNT"] = 0.00;
                                if ($arr_calculation["discount_price"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_DISCOUNT"] = $converter->format((float) $converter->convert($arr_calculation["discount_price"], $arr_calculation["currency"], $VC));
                                }
                                ?>

                                <tr>
                                    <td><?= $arr_rooms[$arr_calculation["room_id"]]["UF_NAME"] ?></td>
                                    <td><ul>
                                            <li>Выезд: <b><?= $arr_calculation["date_from_transfer"] ?></b></li>
                                            <li>Заселение: <b><?= $arr_calculation["date_from"] ?></b></li>
                                            <li>Выселение: <b><?= $arr_calculation["date_to"] ?></b></li>
                                            <li>Выезд обратно: <b><?= $arr_calculation["date_back_transfer"] ?></b></li>
                                            <li>Ночей: <b><?= \travelsoft\booking\Utils::getNightsDuration($arr_calculation["date_from"], $arr_calculation["date_to"]) ?></b></li>
                                            <li>Питание: <b><?= $arr_food[$arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]]["NAME"] ?: "Без питания" ?></b></li>
                                        </ul></td>
                                    <td><b><?= $cost ?></b></td>
                                    <td><input class="adm-btn-save" value="Бронировать" type="button" onclick='CRMUtils.booking(<?= json_encode($booking_data) ?>)' type="button" name="__"></td>
                                </tr>
                                <?
                            }
                        }
                        ?></tbody><?
                    break;

                case "placements":
                    ?>
                    <thead>
                        <tr>
                            <th>Номер</th>
                            <th>Тариф номера</th>
                            <th>Общая стоимость</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody><?
                        foreach ($arr_calculations as $arr_grouped_by_placement) {

                            foreach ($arr_grouped_by_placement as $room_id => $arr_grouped_by_rooms) {

                                foreach ($arr_grouped_by_rooms as $rate_id => $arr_calculation) {

                                    if (!isset($arr_rooms[$room_id])) {
                                        $arr_rooms[$room_id] = current(\travelsoft\booking\stores\Rooms::get(
                                                        array("filter" => array("ID" => $room_id), "select" => array("ID", "UF_NAME"))));
                                    }

                                    if (!isset($arr_rates[$rate_id])) {
                                        $arr_rates[$rate_id] = current(travelsoft\booking\stores\Rates::get(
                                                        array(
                                                            "filter" => array("ID" => $rate_id),
                                                            "select" => array("UF_NAME", "ID", "UF_FOOD")
                                        )));
                                        if ($arr_rates[$rate_id]["UF_FOOD"] > 0 && !isset($arr_food[$arr_rates[$rate_id]["UF_FOOD"]])) {
                                            $arr_food[$arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]] = \travelsoft\booking\stores\Food::get(
                                                            array(
                                                                "filter" => array("ID" => $arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]),
                                                                "select" => array("ID", "NAME")
                                            ));
                                        }
                                    }

                                    $cost = $converter->convertWithFormatting($arr_calculation["result_price"], $arr_calculation["currency"], $VC);

                                    $booking_data = array(
                                        "UF_COST" => $converter->format((float) $converter->convert($arr_calculation["gross"], $arr_calculation["currency"], $VC)),
                                        "UF_NETTO" => $converter->format((float) $converter->convert($arr_calculation["netto"], $arr_calculation["currency"], $VC)),
                                        "UF_CURRENCY" => $VC,
                                        "UF_DATE_FROM_PLACE" => $arr_calculation["date_from"],
                                        "UF_DATE_TO_PLACE" => $arr_calculation["date_to"],
                                        "UF_PLACEMENT_RATE" => $rate_id,
                                        "UF_FOOD" => isset($arr_food[$arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]]["NAME"]) ? $arr_food[$arr_rates[$arr_calculation["placement_rate_id"]]["UF_FOOD"]]["NAME"] : ""
                                    );
                                    
                                    $booking_data["UF_TS_PRICE"] = 0.00;
                                    if ($arr_calculation["tourservice_for_adults"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                        $booking_data["UF_TS_PRICE"] += $arr_calculation["tourservice_for_adults"];
                                    }
                                    if ($arr_calculation["tourservice_for_children"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                        $booking_data["UF_TS_PRICE"] += $arr_calculation["tourservice_for_children"];
                                    }
                                    $booking_data["UF_TS_CURRENCY"] = "BYN";
                                    if ($booking_data["UF_TS_PRICE"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                        $booking_data["UF_TS_PRICE"] = $converter->format((float) $converter->convert($booking_data["UF_TS_PRICE"], $arr_calculation["tourservice_currency"], "BYN"));
                                    }
                                    $booking_data["UF_MARKUP_CURRENCY"] = "BYN";
                                    $booking_data["UF_MARKUP_PRICE"] = 0.00;
                                    if ($arr_calculation["markup_price"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                        $booking_data["UF_MARKUP_PRICE"] = $converter->format((float) $converter->convert($arr_calculation["markup_price"], $arr_calculation["markup_currency"], $VC));
                                        $booking_data["UF_MARKUP_CURRENCY"] = $VC;
                                    }
                                    $booking_data["UF_DISCOUNT"] = 0.00;
                                    if ($arr_calculation["discount_price"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                        $booking_data["UF_DISCOUNT"] = $converter->format((float) $converter->convert($arr_calculation["discount_price"], $arr_calculation["currency"], $VC));
                                    }
                                    ?>

                                    <tr>
                                        <td><?= $arr_rooms[$room_id]["UF_NAME"] ?></td>
                                        <td><ul>
                                                <li>Заселение: <?= $arr_calculation["date_from"] ?></li>
                                                <li>Выселение: <?= $arr_calculation["date_to"] ?></li>
                                                <li>Ночей: <?= \travelsoft\booking\Utils::getNightsDuration($arr_calculation["date_from"], $arr_calculation["date_to"]) ?></li>
                                                <li>Питание: <?= $arr_food[$arr_rates[$rate_id]["UF_FOOD"]]["NAME"] ?: "Без питания" ?></li>
                                            </ul></td>
                                        <td><?= $cost ?></td>
                                        <td><input class="adm-btn-save" type="button" onclick='CRMUtils.booking(<?= json_encode($booking_data) ?>)' type="button" name="__" value="Бронировать"></td>
                                    </tr>

                                    <?
                                }
                            }
                        }
                        ?>
                    </tbody><?
                    break;

                case "transfer":
                case "transferback":
                    ?>
                    <thead>
                        <tr>
                            <th>Проезд</th>
                            <th>Выезд</th>
                            <th>Общая стоимость</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody><?
                        foreach ($arr_calculations as $transfer_id => $arr_grouped_by_rates) {

                            foreach ($arr_grouped_by_rates as $rate_id => $arr_calculation) {

                                if (!isset($arr_rates[$rate_id])) {

                                    $arr_rates[$rate_id] = current(travelsoft\booking\stores\Rates::get(
                                                    array(
                                                        "filter" => array("ID" => $rate_id),
                                                        "select" => array("UF_NAME", "ID")
                                    )));
                                }

                                $cost = $converter->convertWithFormatting($arr_calculation["result_price"], $arr_calculation["currency"], $VC);
                                $booking_data = array(
                                    "UF_COST" => $converter->format((float) $converter->convert($arr_calculation["gross"], $arr_calculation["currency"], $VC)),
                                    "UF_NETTO" => $converter->format((float) $converter->convert($arr_calculation["netto"], $arr_calculation["currency"], $VC)),
                                    "UF_CURRENCY" => $VC,
                                    "UF_TRANS_RATE" => $rate_id,
                                );
                                $booking_data["UF_TS_PRICE"] = 0.00;
                                if ($arr_calculation["tourservice_for_adults"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] += $arr_calculation["tourservice_for_adults"];
                                }
                                if ($arr_calculation["tourservice_for_children"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] += $arr_calculation["tourservice_for_children"];
                                }
                                $booking_data["UF_TS_CURRENCY"] = "BYN";
                                if ($booking_data["UF_TS_PRICE"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] = $converter->format((float) $converter->convert($booking_data["UF_TS_PRICE"], $arr_calculation["tourservice_currency"], "BYN"));
                                }
                                $booking_data["UF_MARKUP_CURRENCY"] = "BYN";
                                $booking_data["UF_MARKUP_PRICE"] = 0.00;
                                if ($arr_calculation["markup_price"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_MARKUP_PRICE"] = $converter->format((float) $converter->convert($arr_calculation["markup_price"], $arr_calculation["markup_currency"], $VC));
                                    $booking_data["UF_MARKUP_CURRENCY"] = $VC;
                                }
                                $booking_data["UF_DISCOUNT"] = 0.00;
                                if ($arr_calculation["discount_price"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_DISCOUNT"] = $converter->format((float) $converter->convert($arr_calculation["discount_price"], $arr_calculation["currency"], $VC));
                                }
                                ?>

                                <tr>
                                    <td><?= $arr_rates[$rate_id]["UF_NAME"] ?></td>
                                    <td><?= $arr_calculation["date"] ?>
                                    <td><?= $cost ?></td>
                                    <td><input class="adm-btn-save" type="button" onclick='CRMUtils.booking(<?= json_encode($booking_data) ?>)' type="button" name="__" value="Бронировать"></td>
                                </tr>

                                <?
                            }
                        }
                        ?></tbody><?
                    break;
                    
                case "excursionTour":
                    ?>
                    <thead>
                        <tr>
                            <th>Экскурсионный тур</th>
                            <th>Дата</th>
                            <th>Общая стоимость</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody><?
                        foreach ($arr_calculations as $excursion_tour_id => $arr_grouped_by_rates) {
                            
                            if (!isset($arr_excursion_tour[$excursion_tour_id])) {
                                $arr_excursion_tour[$excursion_tour_id] = travelsoft\booking\stores\ExcursionTour::getById($excursion_tour_id, ["ID", "UF_NAME"]);
                            }
                            
                            
                            foreach ($arr_grouped_by_rates as $rate_id => $arr_calculation) {

                                if (!isset($arr_rates[$rate_id])) {

                                    $arr_rates[$rate_id] = current(travelsoft\booking\stores\Rates::get(
                                                    array(
                                                        "filter" => array("ID" => $rate_id),
                                                        "select" => array("UF_NAME", "ID")
                                    )));
                                }

                                $cost = $converter->convertWithFormatting($arr_calculation["result_price"], $arr_calculation["currency"], $VC);
                                $booking_data = array(
                                    "UF_COST" => $converter->format((float) $converter->convert($arr_calculation["gross"], $arr_calculation["currency"], $VC)),
                                    "UF_NETTO" => $converter->format((float) $converter->convert($arr_calculation["netto"], $arr_calculation["currency"], $VC)),
                                    "UF_CURRENCY" => $VC,
                                    "UF_EXCUR_TOUR_RATE" => $rate_id,
                                );
                                $booking_data["UF_TS_PRICE"] = 0.00;
                                if ($arr_calculation["tourservice_for_adults"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] += $arr_calculation["tourservice_for_adults"];
                                }
                                if ($arr_calculation["tourservice_for_children"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] += $arr_calculation["tourservice_for_children"];
                                }
                                $booking_data["UF_TS_CURRENCY"] = "BYN";
                                if ($booking_data["UF_TS_PRICE"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_TS_PRICE"] = $converter->format((float) $converter->convert($booking_data["UF_TS_PRICE"], $arr_calculation["tourservice_currency"], "BYN"));
                                }
                                $booking_data["UF_MARKUP_CURRENCY"] = "BYN";
                                $booking_data["UF_MARKUP_PRICE"] = 0.00;
                                if ($arr_calculation["markup_price"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_MARKUP_PRICE"] = $converter->format((float) $converter->convert($arr_calculation["markup_price"], $arr_calculation["markup_currency"], $VC));
                                    $booking_data["UF_MARKUP_CURRENCY"] = $VC;
                                }
                                $booking_data["UF_DISCOUNT"] = 0.00;
                                if ($arr_calculation["discount_price"] >= \travelsoft\booking\Settings::FLOAT_NULL) {
                                    $booking_data["UF_DISCOUNT"] = $converter->format((float) $converter->convert($arr_calculation["discount_price"], $arr_calculation["currency"], $VC));
                                }
                                ?>

                                <tr>
                                    <td><?= $arr_excursion_tour[$excursion_tour_id]["UF_NAME"] ?></td>
                                    <td><?= $arr_calculation["date"] ?>
                                    <td><?= $cost ?></td>
                                    <td><input class="adm-btn-save" type="button" onclick='CRMUtils.booking(<?= json_encode($booking_data) ?>)' type="button" name="__" value="Бронировать"></td>
                                </tr>

                                <?
                            }
                        }
                        ?></tbody><?
                    break;
            }
            ?>
        </table>
        <script>

            CRMUtils = {
                booking: function (data) {

                    var pw = window.opener;
                    var pd = pw.document;
                    var jq = pw.jQuery;
                    
                    pd.querySelector("#UF_COST").value = data.UF_COST;
                    pd.querySelector("#UF_COST").disabled = false;
                    pd.querySelector("#UF_CURRENCY").value = data.UF_CURRENCY;
                    pd.querySelector("#UF_NETTO").value = data.UF_NETTO;
                    pd.querySelector("#UF_NETTO").disabled = false;
                    pd.querySelector("#UF_TS_PRICE").value = data.UF_TS_PRICE;
                    pd.querySelector("#UF_TS_PRICE").disabled = false;
                    pd.querySelector("#UF_TS_CURRENCY").value = data.UF_TS_CURRENCY;
                    pd.querySelector("#UF_MARKUP_PRICE").value = data.UF_MARKUP_PRICE;
                    pd.querySelector("#UF_MARKUP_PRICE").disabled = false;
                    pd.querySelector("#UF_MARKUP_CURRENCY").value = data.UF_MARKUP_CURRENCY;
                    pd.querySelector("#UF_DISCOUNT").value = data.UF_DISCOUNT;
                    pd.querySelector("#UF_DISCOUNT").disabled = false;

                    jq("#UF_DATE_FROM_PLACE").remove();
                    jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_DATE_FROM_PLACE" name="UF_DATE_FROM_PLACE" value="${data.UF_DATE_FROM_PLACE}">`);
                    jq("#UF_DATE_TO_PLACE").remove();
                    jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_DATE_TO_PLACE" name="UF_DATE_TO_PLACE" value="${data.UF_DATE_TO_PLACE}">`);
                    jq("#UF_PLACEMENT_RATE").remove();
                    jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_PLACEMENT_RATE" name="UF_PLACEMENT_RATE" value="${data.UF_PLACEMENT_RATE}">`);
                    jq("#VARIAN_IS_CHOOSED").remove();
                    jq("#VOUCHER_edit_table").before(`<input type="hidden" id="VARIAN_IS_CHOOSED" name="VARIAN_IS_CHOOSED" value="1">`);
        <? if ($_GET["service_type"] === "packageTour"): ?>

                        jq("#UF_TRANS_RATE").remove();
                        jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_TRANS_RATE" name="UF_TRANS_RATE" value="${data.UF_TRANS_RATE}">`);
                        jq("#UF_DATE_FROM_TRANS").remove();
                        jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_DATE_FROM_TRANS" name="UF_DATE_FROM_TRANS" value="${data.UF_DATE_FROM_TRANS}">`);
                        jq("#UF_DATE_BACK_TRANS").remove();
                        jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_DATE_BACK_TRANS" name="UF_DATE_BACK_TRANS" value="${data.UF_DATE_BACK_TRANS}">`);
                        jq("#UF_TRANS_BACK_RATE").remove();
                        jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_TRANS_BACK_RATE" name="UF_TRANS_BACK_RATE" value="${data.UF_TRANS_BACK_RATE}">`);
        <? elseif($_GET["service_type"] === "excursionTour"):?>
                        jq("#UF_EXCUR_TOUR_RATE").remove();
                        jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_EXCUR_TOUR_RATE" name="UF_EXCUR_TOUR_RATE" value="${data.UF_EXCUR_TOUR_RATE}">`);
        <?elseif (travelsoft\booking\crm\Validator::isTransferServiceType($_GET["service_type"])): ?>
                        jq("#UF_TRANS_RATE").remove();
                        jq("#VOUCHER_edit_table").before(`<input type="hidden" id="UF_TRANS_RATE" name="UF_TRANS_RATE" value="${data.UF_TRANS_RATE}">`);
        <? endif ?>
        <? if (!\travelsoft\booking\crm\Validator::isTransferServiceType($_GET["service_type"]) && $_GET["service_type"] !== "excursionTour"): ?>
                        pd.querySelector("#UF_FOOD").value = data.UF_FOOD || "";
        <? endif ?>
                    window.close();
                }
            }
        </script>

        <?
    } else {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => "По данным параметрам поиска не удалось найти подходящие предложения. Пожалуйста, измените параметры поиска.",
            "TYPE" => "ERROR"
        ));
    }
} else {

    CAdminMessage::ShowMessage(array(
        "MESSAGE" => "Недостаточно данных для поиска",
        "TYPE" => "ERROR"
    ));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");

