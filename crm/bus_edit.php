<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\crm\Utils;

require_once 'header.php';

?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Добавление автобуса';
    if ($ID > 0) {

        $arBuses = current(\travelsoft\booking\stores\Buses::get(array('filter' => array('ID' => $ID))));

        if (!$arBuses['ID']) {

            throw new Exception('Автобус с ID="' . $ID . '" не найден');
        }

        $title = 'Редактирование автобуса #' . $arBuses['ID'];
    }

    $APPLICATION->SetTitle($title);

    $arResult = travelsoft\booking\crm\BusesUtils::processingEditForm();

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }
    
    Utils::showEditForm(array(
        'action' => $APPLICATION->GetCurPageParam(),
        'name' => 'bus_form',
        'id' => 'bus_form',
        'tabs' => array(
            array(
                "DIV" => "BUS_FIELD",
                "TAB" => 'Поля для добавления/редактирования автобуса',
                "FIELDS" => travelsoft\booking\crm\BusesUtils::getEditFormFields($arBuses)
            )
        ),
        'buttons' => array(
            array(
                'class' => 'adm-btn-save',
                'name' => 'SAVE',
                'value' => 'Сохранить'
            ),
            array(
                'name' => 'CANCEL',
                'value' => 'Отменить'
            )
        )
    ));
    
} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

