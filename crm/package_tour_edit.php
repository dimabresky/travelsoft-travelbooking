<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\crm\Utils;

require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS."/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/common.js?".randString(7)."'></script>");
?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Добавление пакетного тура';
    if ($ID > 0) {

        $arPackageTour = current(\travelsoft\booking\stores\PackageTour::get(array('filter' => array('ID' => $ID))));

        if (!$arPackageTour['ID']) {

            throw new Exception('Пакетный тур с ID="' . $ID . '" не найден');
        }

        $title = 'Редактирование пакетного тура #' . $arPackageTour['ID'];
    }

    $APPLICATION->SetTitle($title);

    $arResult = travelsoft\booking\crm\PackageTourUtils::processingEditForm();

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }
    
    Utils::showEditForm(array(
        'action' => $APPLICATION->GetCurPageParam(),
        'name' => 'package_tour_form',
        'id' => 'package_tour_form',
        'tabs' => array(
            array(
                "DIV" => "PACKAGE_TOUR",
                "TAB" => 'Поля для заполнения пакетного тура',
                "FIELDS" => travelsoft\booking\crm\PackageTourUtils::getEditFormFields($arPackageTour)
            )
        ),
        'buttons' => array(
            array(
                'class' => 'adm-btn-save',
                'name' => 'SAVE',
                'value' => 'Сохранить'
            ),
            array(
                'name' => 'CANCEL',
                'value' => 'Отменить'
            )
        )
    ));
    
} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

