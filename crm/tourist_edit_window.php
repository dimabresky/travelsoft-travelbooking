<?php
require_once 'header.php';

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

use travelsoft\booking\crm\Utils;

$APPLICATION->SetTitle('Заполнение данных по туристу');

?>
<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>
<?

$arResult =  \travelsoft\booking\crm\TouristsUtils::processingTouristEditForm(false);

if ($arResult['result'] > 0) {

    $arTourist = current(travelsoft\booking\stores\Tourists::get(array('filter' => array('ID' => $arResult['result']), 'select' => array('ID', 'UF_NAME', 'UF_LAST_NAME', 'UF_SECOND_NAME'))));
    ?>
    <script>

        var value = Number("<?= $arTourist['ID'] ?>");
        var text = "<?=
    implode(' ', array_filter(array($arTourist['UF_NAME'], $arTourist['UF_SECOND_NAME'], $arTourist['UF_LAST_NAME']), function ($item) {
                return strlen($item) > 0;
            }))
    ?>";

        if (typeof window.opener.touristChildWindowData === 'object') {

            window.opener.touristChildWindowData.touristSelect.append('<option selected="" value="' + value + '">' + text + '</option>');
            window.opener.touristChildWindowData.touristSelect.trigger('change');
            window.opener.touristChildWindowData.initSelect2();
        }
        
        window.close();
    </script>
    <?
}

if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }

    Utils::showEditForm(array(
    'action' => $APPLICATION->GetCurPageParam(),
    'name' => 'tourist_form',
    'id' => 'tourist_form',
    'tabs' => array(
        array(
            "DIV" => "MAIN_TOURIST_DATA",
            "TAB" => 'Основные данные по туристу',
            '__CONTENT' => \travelsoft\booking\crm\TouristsUtils::getTouristFieldsContent($arTourist, 'MAIN_TOURIST_DATA')
        )
    ),
    'buttons' => array(
        array(
            'class' => 'adm-btn-save',
            'name' => 'SAVE',
            'value' => 'Сохранить'
        )
    )
));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");

