<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Markup;
use Bitrix\Main\Entity\ExpressionField;
use travelsoft\booking\crm\Settings;

require_once 'header.php';

$sort = new CAdminSorting(Settings::MARKUP_HTML_TABLE_ID, "ID", "DESC");
$list = new CAdminList(Settings::MARKUP_HTML_TABLE_ID, $sort);

if ($arMarkupId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arMarkupId = array_keys(Markup::get(array('select' => array('ID'))));
    }

    foreach ($arMarkupId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                Markup::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}

$getParams = array("order" => array($by => $order));

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        Settings::MARKUP_HTML_TABLE_ID, array('nPageSize' => 20)
        ));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {

    $totalCount = Markup::get(array('select' => array(new ExpressionField('CNT', 'COUNT(1)'))), false)->fetch();

    $totalCount = (int) $totalCount['CNT'];

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arMarkup = Markup::get($getParams);

$dbResult = new CAdminResult($arMarkup, Settings::MARKUP_HTML_TABLE_ID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SERVICE_TYPE",
        "content" => "Тип услуг",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SERVICES",
        "content" => "Услуги",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_PRICE",
        "content" => "Цена",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_CURRENCY",
        "content" => "Валюта",
        "align" => "center",
        "default" => true
    ),
));

$object = array();
while ($arMarkup = $dbResult->Fetch()) {

    $row = &$list->AddRow($arMarkup["ID"], $arMarkup);
    
    if (!empty($arMarkup["UF_SERVICES"])) {
        $arServicesNames = array();
        if (!isset($object[$arMarkup["UF_SERVICE_TYPE"]])) {
            $object[$arMarkup["UF_SERVICE_TYPE"]] = current(travelsoft\booking\crm\Utils::getBookingServices(
                    array($arMarkup["UF_SERVICE_TYPE"])));
        }
        
        foreach ($arMarkup["UF_SERVICES"] as $service_id) {
            
            if ($object[$arMarkup["UF_SERVICE_TYPE"]][$service_id]["NAME"]) {
                $arServicesNames[] = $object[$arMarkup["UF_SERVICE_TYPE"]][$service_id]["NAME"];
            } elseif ($object[$arMarkup["UF_SERVICE_TYPE"]][$service_id]["UF_NAME"]) {
                $arServicesNames[] = $object[$arMarkup["UF_SERVICE_TYPE"]][$service_id]["UF_NAME"];
            }
        }
        
        if (!empty($arServicesNames)) {
            $row->AddViewField("UF_SERVICES", implode(", ", $arServicesNames));
        }
    }
    
    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => $list->ActionRedirect("travelsoft_crm_booking_markup_edit.php?ID=" . $arMarkup["ID"])
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить наценку')) " . $list->ActionDoGroup($arMarkup["ID"], "delete")
        )
    ));
}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
        'TEXT' => "Создать наценку",
        'TITLE' => "Создание наценки",
        'LINK' => 'travelsoft_crm_booking_markup_edit.php?lang=' . LANG,
        'ICON' => 'btn_new'
)));

$list->CheckListMode();

$APPLICATION->SetTitle("Список наценок");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
