<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Documents;
use travelsoft\booking\stores\Orders;

require_once 'header.php';

function attemptSendErros (array $errors) {
    if (!empty($errors)) {
        throw new \Exception(implode('<br>', $errors));
    }
}

try {
    
    $errors = array();
    
    if (0 >= $_GET['VOUCHER_ID']) {
        $errors[] = 'Не указан id путевки';
    }
    
    if (0 >= $_GET['DOC_TPL_ID']) {
        $errors[] = 'Не указан id шаблона документа';
    }
    
    if (0 >= strlen($_GET['DOCFORMAT'])) {
        $errors[] = 'Не указан формат получаемого документа';
    }
    
    attemptSendErros($errors);
    
    $arr_voucher = \travelsoft\booking\stores\Vouchers::getById((int)$_GET['VOUCHER_ID']);
    
    if (!$arr_voucher['ID']) {
        $errors[] = 'Путевка с ID="'+$arr_voucher['ID']+'" не найдена';
    }
    
    foreach ($arr_voucher["UF_TOURISTS"] as $tid) {
        
    }
    
    $arr_voucher["BOOKINGS_DETAIL"] = travelsoft\booking\stores\Bookings::get(array("filter" => array("ID" => $arr_voucher["UF_BOOKINGS"][0])));
    
    foreach ($arr_voucher["BOOKINGS_DETAIL"] as $arr_booking) {
        foreach ($arr_booking["UF_TOURISTS"] as $tid) {
            $arr_voucher["TOURISTS"][$tid] = \travelsoft\booking\stores\Tourists::getById($tid);
        }
        
    }
    
    $dbDoc = Documents::getById((int)$_GET['DOC_TPL_ID']);
    
    if (!$dbDoc['ID'] || !$dbDoc['UF_TEMPLATE']) {
        $errors[] = 'Шаблон документа не найден';
    }
    
    attemptSendErros($errors);

    $file = CFile::GetFileArray($dbDoc['UF_TEMPLATE']);
    
    $document = $dbDoc['UF_CLASS']::create($arr_voucher, $file['SRC'], $_GET['DOCFORMAT']);
    
    
} catch (\Exception $e) {
    
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin_after.php");

