<?php

require_once 'header.php';

if ($_REQUEST["service_type"] === "packageTour") {

    $arr_placements = $arr_transfer = array();
    foreach (\travelsoft\booking\stores\PackageTour::get() as $arr_package_tour) {
        if (!isset($arr_placements[$arr_package_tour["UF_PLACEMENT"]])) {
            $arr_placements[$arr_package_tour["UF_PLACEMENT"]] = current(\travelsoft\booking\stores\Placements::get(
                            array(
                                "filter" => array("ID" => $arr_package_tour["UF_PLACEMENT"]),
                                "select" => array("ID", "NAME")
            )));
        }
        if (!isset($arr_transfer[$arr_package_tour["UF_TRANSFER"]])) {
            $arr_transfer[$arr_package_tour["UF_TRANSFER"]] = current(\travelsoft\booking\stores\Buses::get(
                            array(
                                "filter" => array("ID" => $arr_package_tour["UF_TRANSFER"]),
                                "select" => array("ID", "UF_NAME")
            )));
        }
        
        $response[] = array(
            "id" => $arr_package_tour["ID"],
            "name" => $arr_transfer[$arr_package_tour["UF_TRANSFER"]]["UF_NAME"] . " + " . $arr_placements[$arr_package_tour["UF_PLACEMENT"]]["~NAME"]
        );
    }

    \travelsoft\booking\crm\Utils::sendJsonResponse(json_encode($response));
} elseif ($_REQUEST["service_type"] === "placements") {

    foreach (\travelsoft\booking\stores\Placements::get() as $arr_placement) {

        $response[] = array(
            "id" => $arr_placement["ID"],
            "name" => $arr_placement["~NAME"]
        );
    }

    \travelsoft\booking\crm\Utils::sendJsonResponse(json_encode($response));
} elseif ($_REQUEST["service_type"] === "transfer" || $_REQUEST["service_type"] === "transferback") {
    foreach (\travelsoft\booking\stores\Buses::get() as $arr_transfer) {

        $response[] = array(
            "id" => $arr_transfer["ID"],
            "name" => $arr_transfer["UF_NAME"]
        );
    }

    \travelsoft\booking\crm\Utils::sendJsonResponse(json_encode($response));
} elseif ($_REQUEST["service_type"] === "excursionTour") {
    foreach (\travelsoft\booking\stores\ExcursionTour::get() as $arr_excursion_tour) {

        $response[] = array(
            "id" => $arr_excursion_tour["ID"],
            "name" => $arr_excursion_tour["UF_NAME"]
        );
    }

    \travelsoft\booking\crm\Utils::sendJsonResponse(json_encode($response));
} else {
    
    \travelsoft\booking\crm\Utils::sendJsonResponse(json_encode(array()));
}