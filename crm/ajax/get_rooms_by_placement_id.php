<?php

require_once 'header.php';

if ($_REQUEST["id"] > 0) {
    foreach(travelsoft\booking\stores\Rooms::get(array(
        "filter" => array("UF_PLACEMENT" => $_REQUEST["id"]),
        "select" => array("ID", "UF_NAME")
    )) as $arr_room) {
        $response[] = array(
            "id" => $arr_room["ID"],
            "name" => $arr_room["UF_NAME"]
        );
    }
    
   \travelsoft\booking\crm\Utils::sendJsonResponse(json_encode($response));
    
} 