<?php

require_once 'header.php';

if ($_REQUEST["id"] > 0) {
    foreach (travelsoft\booking\stores\PackageTour::get(array(
        "filter" => array("ID" => $_REQUEST["id"])
    )) as $arr_package_tour) {
        $response["placement"] = $arr_package_tour["UF_PLACEMENT"];
        $response["transfer"] = $arr_package_tour["UF_TRANSFER"];
        $response["transferback"] = $arr_package_tour["UF_TRANSFER_BACK"];
        foreach (travelsoft\booking\stores\Rooms::get(array(
            "filter" => array("UF_PLACEMENT" => $arr_package_tour["UF_PLACEMENT"]),
            "select" => array("ID", "UF_NAME")
        )) as $arr_room) {
            $response["rooms"][] = array(
                "id" => $arr_room["ID"],
                "name" => $arr_room["UF_NAME"]
            );
        }
    }

    \travelsoft\booking\crm\Utils::sendJsonResponse(json_encode($response));
} 