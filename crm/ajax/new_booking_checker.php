<?php

require_once 'header.php';

$arResponse['result']['content'] = '';

if ($_REQUEST['last_id'] > 0 && bitrix_sessid()) {

    $LAST_ID = intVal($_REQUEST['last_id']);

    $arBookings = travelsoft\booking\stores\Bookings::get(array(
                'filter' => array('>ID' => $LAST_ID),
                'order' => array('ID' => 'DESC')
    ));

    if (!empty($arBookings)) {

        $arHeaders = \travelsoft\booking\crm\Utils::getOrdersTableHeaders();

        $list = new CAdminList(\travelsoft\booking\crm\Settings::BOOKINGS_HTML_TABLE_ID);
        
        $list->AddHeaders($arHeaders);

        $list->AddGroupActionTable(Array(
            "delete" => "Удалить"
        ));
        
        foreach ($arBookings as $ID => $arBooking) {
        
            $row = new CAdminListRow($arHeaders, \travelsoft\booking\crm\Settings::BOOKINGS_HTML_TABLE_ID);

            $row->id = $arBooking['ID'];
            $row->arRes = $arBooking;
            $row->pList = &$list;
            $row->pList->bShowActions = true;

            $arStatus = \travelsoft\booking\stores\Statuses::getById($arBooking['UF_STATUS_ID']);

            \travelsoft\booking\crm\BookingsUtils::prepareRowForBookingsTable($row, array(
                'ORDER' => $arBooking,
                'STATUSES' => array($arStatus['ID'] => $arStatus)
            ));
            
            ob_start();
            $row->Display();
            $arResponse['result']['content'] .= ob_get_clean();
            
        }
            
        $arResponse['result']['last_id'] = travelsoft\booking\stores\Bookings::getLastId();
    }
}

echo json_encode($arResponse);
