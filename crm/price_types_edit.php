<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\crm\Utils;

require_once 'header.php';

?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Добавление типа цены';
    if ($ID > 0) {

        $arPriceType = current(\travelsoft\booking\stores\PriceTypes::get(array('filter' => array('ID' => $ID))));

        if (!$arPriceType['ID']) {

            throw new Exception('Тип цены с ID="' . $ID . '" не найден');
        }

        $title = 'Редактирование типа цены #' . $arPriceType['ID'];
    }

    $APPLICATION->SetTitle($title);

    $arResult = travelsoft\booking\crm\PriceTypesUtils::processingEditForm();

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }
    
    Utils::showEditForm(array(
        'action' => $APPLICATION->GetCurPageParam(),
        'name' => 'price_types_form',
        'id' => 'price_types_form',
        'tabs' => array(
            array(
                "DIV" => "PRICE_TYPES",
                "TAB" => 'Поля для заполнения типа цены',
                "FIELDS" => travelsoft\booking\crm\PriceTypesUtils::getEditFormFields($arPriceType)
            )
        ),
        'buttons' => array(
            array(
                'class' => 'adm-btn-save',
                'name' => 'SAVE',
                'value' => 'Сохранить'
            ),
            array(
                'name' => 'CANCEL',
                'value' => 'Отменить'
            )
        )
    ));
    
} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

