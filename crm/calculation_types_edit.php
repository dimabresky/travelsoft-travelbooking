<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\crm\Utils;

require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS."/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/common.js?".randString(7)."'></script>");
?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Добавление типа расчета';
    if ($ID > 0) {

        $arCalculationType = current(\travelsoft\booking\stores\CalculationTypes::get(array('filter' => array('ID' => $ID))));

        if (!$arCalculationType['ID']) {

            throw new Exception('Тип расчета с ID="' . $ID . '" не найден');
        }

        $title = 'Редактирование типа расчета #' . $arCalculationType['ID'];
    }

    $APPLICATION->SetTitle($title);

    $arResult = travelsoft\booking\crm\CalculationTypesUtils::processingEditForm();

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }
    
    Utils::showEditForm(array(
        'action' => $APPLICATION->GetCurPageParam(),
        'name' => 'calculation_types_form',
        'id' => 'calculation_types_form',
        'tabs' => array(
            array(
                "DIV" => "CALCULATION_TYPES",
                "TAB" => 'Поля для заполнения типа расчета',
                "FIELDS" => travelsoft\booking\crm\CalculationTypesUtils::getEditFormFields($arCalculationType)
            )
        ),
        'buttons' => array(
            array(
                'class' => 'adm-btn-save',
                'name' => 'SAVE',
                'value' => 'Сохранить'
            ),
            array(
                'name' => 'CANCEL',
                'value' => 'Отменить'
            )
        )
    ));
    
} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

