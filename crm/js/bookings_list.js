
/**
 * Object of utilites and page configuration
 * @type Object
 */
window.CRM = {

    /**
     * Utilites
     * @type Object
     */
    utils: {

        /**
         * Display notify
         * @param {Object} config
         * @returns {undefined}
         */
        showNotify: function (config) {

            var options = {

                body: typeof config.body === 'string' ? config.body : '',
                icon: typeof config.icon === 'string' ? config.icon : '',
            };

            var notifyWindow = new Notification(typeof config.title === 'string' ? config.title : '', options);
            notifyWindow.onclick = function (e) {

            };

            notifyWindow.onshow = function (e) {

                if (typeof config.sound === 'string') {

                    var audio = new Audio();
                    audio.src = config.sound;
                    audio.autoplay = true;

                }
            };

        },
        
        /**
         * Show alert message
         * @param {Object} data
         * @returns {Boolean}
         */
        showAlert: function (data) {

            if (typeof data.error === 'string') {

                alert(data.error);
                return true;
            }

            return false;
        }

    },

    /**
     * Page configuration
     * @type Object
     */
    config: {}

}

$(document).ready(function () {

    function newBookingChecker(withNotify) {

        setInterval(function () {
            
            $.post("/local/modules/travelsoft.travelbooking/crm/ajax/new_booking_checker.php", {
                
                sessid: CRM.config.sessid,
                last_id: CRM.config.last_id
                
            }, function (data) {

                if (!CRM.utils.showAlert(data)) {

                    if (typeof data.result === 'object' && data.result) {

                        if (typeof data.result.content === 'string' && typeof data.result.content != '') {

                            // add table row new order
                            $("#" + CRM.config.table_id).prepend(data.result.content);
                            CRM.config.last_id = data.result.last_id;
                        }

                        if (data.result.last_id && withNotify) {

                            // show notify
                            CRM.utils.showNotify({

                                title: CRM.config.notifyTitle,
                                icon: CRM.config.notifyIcon,
                                sound: CRM.config.notifySound
                            });
                        }

                    }

                }

            });

        }, CRM.config.time_interval);

    }

    if (typeof CRM.config.table_id === 'string') {

        if (!("Notification" in window) || Notification.permission === 'denied') {

            newBookingChecker(false);

        } else if (Notification.permission === 'granted') {

            newBookingChecker(true);

        } else {

            Notification.requestPermission(function (permission) {

                if (permission === "granted") {

                    newBookingChecker(true);
                } else {

                    newBookingChecker(false);
                }
            });

        }
    }
});