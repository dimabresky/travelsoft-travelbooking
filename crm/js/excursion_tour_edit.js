
$(document).ready(function () {
    
    'use strict';
    
    $(".select2").on("change", function () {
        
        var $this = $(this);
        $("input[name=UF_NAME]").val($this.find(`option[value=${$this.val()}]`).text());
    });
});
