<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Vouchers;
use Bitrix\Main\Entity\ExpressionField;
use travelsoft\booking\crm\Settings;

require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS."/vouchers-list.css'>");

$sort = new CAdminSorting(Settings::VOUCHERS_LIST_HTML_TABLE_ID, "ID", "DESC");
$list = new CAdminList(Settings::VOUCHERS_LIST_HTML_TABLE_ID, $sort);

if ($arVouchersId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arVouchersId = array_keys(Vouchers::get(array('select' => array('ID'))));
    }

    foreach ($arVouchersId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                Vouchers::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}

$getParams = array("order" => array($by => $order));

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        Settings::VOUCHERS_LIST_HTML_TABLE_ID, array('nPageSize' => 20)
        ));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {

    $totalCount = Vouchers::get(array('select' => array(new ExpressionField('CNT', 'COUNT(1)'))), false)->fetch();

    $totalCount = (int) $totalCount['CNT'];

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arVouchers = Vouchers::get($getParams);

$dbResult = new CAdminResult($arVouchers, Settings::VOUCHERS_LIST_HTML_TABLE_ID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "Номер",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_DATE_CREATE",
        "content" => "Дата создания",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_СLIENT",
        "content" => "Клиент",
        "align" => "center",
        "default" => true
    ),
//    array(
//        "id" => "UF_MANAGER",
//        "content" => "Менеджер",
//        "align" => "center",
//        "default" => true
//    )
));

while ($arVoucher = $dbResult->Fetch()) {
    $row = $list->AddRow($arVoucher["ID"], $arVoucher);
    \travelsoft\booking\crm\VouchersUtils::prepareRowForVochersTable($row, $arVoucher);
    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => $list->ActionRedirect(Settings::VOUCHER_EDIT_URL ."?ID=" . $arVoucher["ID"])
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить путевку')) " . $list->ActionDoGroup($arVoucher["ID"], "delete")
        )
    ));
}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
        'TEXT' => "Создать путевку",
        'TITLE' => "Создание путевки",
        'LINK' => Settings::VOUCHER_EDIT_URL .'?lang=' . LANG,
        'ICON' => 'btn_new'
)));

$list->CheckListMode();

$APPLICATION->SetTitle("Список путевок");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");


$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
