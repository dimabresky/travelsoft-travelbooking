<?php

return array(
    "table" => "ts_buses",
    "table_data" => array(
        "NAME" => "TSBUSES",
        "ERR" => "Ошибка при создании highloadblock'a атовбусная база",
        "LANGS" => array(
            "ru" => 'Таблица Автобусная база',
            "en" => "Buses"
        ),
        "OPTION_PARAMETER" => "BUSES_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Название" ',
                'en' => 'An error in completing the field "Name"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_PICTURE",
            "USER_TYPE_ID" => 'file',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Фото',
                'en' => 'Photo',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Фото',
                'en' => 'Photo',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Фото',
                'en' => 'Photo',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Фото" ',
                'en' => 'An error in completing the field "Photo"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_MAP",
            "USER_TYPE_ID" => 'file',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Схема автобуса',
                'en' => 'Bus map',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Схема автобуса',
                'en' => 'Bus map',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Схема автобуса',
                'en' => 'Bus map',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Схема автобуса" ',
                'en' => 'An error in completing the field "Bus map"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
