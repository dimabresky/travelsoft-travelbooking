<?php



return array(
    "table" => "ts_duration",
    "table_data" => array(
        "NAME" => "TSDURATION",
        "ERR" => "Ошибка при создании highloadblock'a Продолжительность",
        "LANGS" => array(
            "ru" => 'Таблица Продолжительность',
            "en" => "Duration"
        ),
        "OPTION_PARAMETER" => "DURATION_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DURATION",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Продолжительность',
                'en' => 'Duration',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Продолжительность',
                'en' => 'Duration',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Продолжительность',
                'en' => 'Duration',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Продолжительность" ',
                'en' => 'An error in completing the field "Duration"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICE_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Услуга',
                'en' => 'Service',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Услуга',
                'en' => 'Service',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Услуга',
                'en' => 'Service',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Услуга" ',
                'en' => 'An error in completing the field "Service"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICE_TYPE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тип услуги" ',
                'en' => 'An error in completing the field "Service type"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATE",
            "USER_TYPE_ID" => 'date',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Дата',
                'en' => 'Date',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Дата',
                'en' => 'Date',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Дата',
                'en' => 'Date',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Дата" ',
                'en' => 'An error in completing the field "Date"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
