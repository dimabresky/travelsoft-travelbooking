<?php



return array(
    "table" => "ts_statuses",
    "table_data" => array(
        "NAME" => "TSSTATUSES",
        "ERR" => "Ошибка при создании highloadblock'a статусы",
        "LANGS" => array(
            "ru" => 'Таблица Статусы',
            "en" => "Statuses"
        ),
        "OPTION_PARAMETER" => "STATUSES_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Название" ',
                'en' => 'An error in completing the field "Name"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CODE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Код',
                'en' => 'Code',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Код',
                'en' => 'Code',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Код',
                'en' => 'Code',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Код" ',
                'en' => 'An error in completing the field "Code"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    ),
    "items" => array(
        array(
            "UF_NAME" => "Создан",
            "UF_CODE" => "CR"
        ),
        array(
            "UF_NAME" => "В работе",
            "UF_CODE" => "WR"
        ),
        array(
            "UF_NAME" => "Оплачен",
            "UF_CODE" => "P"
        ),
        array(
            "UF_NAME" => "Запрос на аннуляцию",
            "UF_CODE" => "RC"
        ),
        array(
            "UF_NAME" => "Аннуляция",
            "UF_CODE" => "CL"
        )
    )
);
