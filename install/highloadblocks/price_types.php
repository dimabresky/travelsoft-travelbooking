<?php



return array(
    "table" => "ts_price_types",
    "table_data" => array(
        "NAME" => "TSPRICETYPES",
        "ERR" => "Ошибка при создании highloadblock'a типы цен",
        "LANGS" => array(
            "ru" => 'Таблица Типы цен',
            "en" => "Price types"
        ),
        "OPTION_PARAMETER" => "PRICE_TYPES_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Название" ',
                'en' => 'An error in completing the field "Name"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CALC_TYPE",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Метод расчета',
                'en' => 'Method of calculation',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Метод расчета',
                'en' => 'Method of calculation',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Метод расчета',
                'en' => 'Method of calculation',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Метод расчета" ',
                'en' => 'An error in completing the field "Method of calculation"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_MIN_AGE",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Минимальный возраст',
                'en' => 'Min age',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Минимальный возраст',
                'en' => 'Min age',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Минимальный возраст',
                'en' => 'Min age',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Минимальный возраст" ',
                'en' => 'An error in completing the field "Min age"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_MAX_AGE",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Максимальный возраст',
                'en' => 'Max age',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Максимальный возраст',
                'en' => 'Max age',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Максимальный возраст',
                'en' => 'Max age',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Максимальный возраст" ',
                'en' => 'An error in completing the field "Max age"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    ),
    "items" => array(
        array(
            "UF_NAME" => "Взрослый на основном месте",
            "UF_CALC_TYPE" => 1
        ),
        array(
            "UF_NAME" => "Взрослый на допоплнительном месте",
            "UF_CALC_TYPE" => 2
        ),
        array(
            "UF_NAME" => "Ребенок на основном месте(3-12 лет)",
            "UF_CALC_TYPE" => 3,
            "UF_MIN_AGE" => 3,
            "UF_MAX_AGE" => 12,
        ),
        array(
            "UF_NAME" => "Ребенок на дополнительном месте(3-12 лет)",
            "UF_CALC_TYPE" => 4,
            "UF_MIN_AGE" => 3,
            "UF_MAX_AGE" => 12,
        ),
        array(
            "UF_NAME" => "Одноместное размещение",
            "UF_CALC_TYPE" => 10
        ),
        array(
            "UF_NAME" => "Проезд взрослый",
            "UF_CALC_TYPE" => 7
        ),
        array(
            "UF_NAME" => "Проезд ребенок (0-16 лет)",
            "UF_CALC_TYPE" => 8,
            "UF_MIN_AGE" => 0,
            "UF_MAX_AGE" => 16,
        ),
    )
);
