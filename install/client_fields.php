<?php

return array(
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_PASS_NUMBER",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Номер паспорта',
            'en' => 'Passport number',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Номер паспорта',
            'en' => 'Passport number',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Номер паспорта',
            'en' => 'Passport number',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Номер паспорта" ',
            'en' => 'An error in completing the field "Passport number"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_PASS_SERIES",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Серия паспорта',
            'en' => 'Passport series',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Серия паспорта',
            'en' => 'Passport series',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Серия паспорта',
            'en' => 'Passport series',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Серия паспорта" ',
            'en' => 'An error in completing the field "Passport series"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_ADDRESS",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Адрес проживания',
            'en' => 'Address',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Адрес проживания',
            'en' => 'Address',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Адрес проживания',
            'en' => 'Address',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Адрес проживания" ',
            'en' => 'An error in completing the field "Address"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
);
