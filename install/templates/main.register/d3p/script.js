/* 
 * Component: main.register
 * Template: d3p
 * @author dimabresky
 * @copyright 2018, travelsoft
 */
$(document).ready(function () {
   
   function scrollto ($el, delta) {
       delta = delta || 0;
       $("html, body").animate({scrollTop: $el.offset().top - delta}, 500);
   }
    
    $("[name^='UF_']").each(function () {
        $(this).addClass("form-control");
    });
    
    $("[name='is_agent']").on("click", function () {
        
        var $this = $(this);
        if ($this.val() === "Y") {
            $("#agent-fields-box").removeClass("hidden");
        } else {
            $("#agent-fields-box").addClass("hidden");
        }
        
    });
    
    $("#regform").on("submit", function (e) {
        
        var $this = $(this);
        var $email = $this.find("[name='REGISTER[EMAIL]']");
        var $phone = $this.find("[name='REGISTER[PERSONAL_PHONE]']");
        var phone_reg = new RegExp($phone.data("phone-reg"));
        var scrollto_el = null;
        var alerts = [];
        if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($email.val())) {
            alerts.push($email.data("alert-text"));
            scrollto_el = $email;
        }
        if (!phone_reg.test($phone.val())) {
            alerts.push($phone.data("alert-text"));
            if (!scrollto_el) {
                scrollto_el = $phone;
            }
        }
        if (alerts.length) {
            alert(alerts.join("\n"));
            scrollto(scrollto_el, 100);
            e.preventDefault();
        }
        $this.find("[name='REGISTER[LOGIN]']").val($email.val());
        
    });
});


