<?php

/**
 * Компонент формы поиска предложений
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class TravelsoftBookingSearchForm extends CBitrixComponent {

    /**
     * component body
     */
    public function executeComponent() {

        Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

        try {

            $request = travelsoft\booking\Utils::makeCommonSearchRequest();
            $this->arResult["DATE_FROM"] = $request["date_from"];
            $this->arResult["DATE_TO"] = $request["date_to"];
            $this->arResult["ADULTS"] = $request["adults"];

            $this->arResult["TAB_ACTIVE"] = $this->arParams["TAB_ACTIVE"];
            $this->arResult["SHOW_PLACEMENTS_TAB"] = $this->arParams["SHOW_PLACEMENTS_TAB"] === "Y";
            $this->arResult["SHOW_PACKAGE_TOUR_TAB"] = $this->arParams["SHOW_PACKAGE_TOUR_TAB"] === "Y";
            $this->arResult["SHOW_TRAVEL_TAB"] = $this->arParams["SHOW_TRAVEL_TAB"] === "Y";

            $this->arResult["PLACEMENTS_SEARCH_PAGE"] = $this->arParams["PLACEMENTS_SEARCH_PAGE"];
            $this->arResult["PACKAGE_TOUR_SEARCH_PAGE"] = $this->arParams["PACKAGE_TOUR_SEARCH_PAGE"];
            $this->arResult["TRAVEL_SEARCH_PAGE"] = $this->arParams["TRAVEL_SEARCH_PAGE"];

            CJSCore::Init();

            $this->IncludeComponentTemplate();
        } catch (\Exception $e) {
            (new \travelsoft\booking\Logger)->write("Component booking.search_form: " . $e->getMessage());
            ShowError("System error.");
        }
    }

}
