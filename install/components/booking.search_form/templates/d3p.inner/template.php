<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

$url_parameters_to_delete = array(
    "travelbooking[services_id][]",
    "travelbooking[date_from]",
    "travelbooking[date_to]",
    "travelbooking[adults]",
    "travelbooking[children]",
    "travelbooking[children_age][]",
);
?>
<div class="offers-search-form">
    <form id="inner-offers-search-form" action="<?= $APPLICATION->GetCurPageParam("", $url_parameters_to_delete, false) ?>" method="get" class="booking-item-dates-change mb40">
        <input type="hidden" name="s2o" value="Y">
        <div class="row">

            <div class="col-md-5">
                <div class="input-daterange" data-date-format="MM d, D">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                <label><?= GetMessage("TRAVELBOOKING_DATE_FROM") ?></label>
                                <input autocomplete="off" data-date-default="<?= (strlen($_GET["travelbooking"]["date_from"]) ? htmlspecialchars($_GET["travelbooking"]["date_from"]) : $arResult["DATE_FROM"]) ?>" class="input-datepicker-inner-form form-control" name="travelbooking[date_from]" type="text" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                <label><?= GetMessage("TRAVELBOOKING_DATE_TO") ?></label>
                                <input autocomplete="off" data-date-default="<?= (strlen($_GET["travelbooking"]["date_to"]) ? htmlspecialchars($_GET["travelbooking"]["date_to"]) : $arResult["DATE_TO"]) ?>" class="input-datepicker-inner-form form-control" name="travelbooking[date_to]" type="text" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group- form-group-select-plus">
                            <label><?= GetMessage("TRAVELBOOKING_ADULTS") ?></label>
                            <? $adults = $_GET["travelbooking"]["adults"] > 0 ? intVal($_GET["travelbooking"]["adults"]) : $arResult["ADULTS"]; ?>
                            <select name="travelbooking[adults]" class="form-control">
                                <option <? if ($adults === 1): ?>selected<? endif ?> value="1">1</option>
                                <option <? if ($adults === 2): ?>selected<? endif ?> value="2">2</option>
                                <option <? if ($adults === 3): ?>selected<? endif ?> value="3">3</option>
                                <option <? if ($adults === 4): ?>selected<? endif ?> value="4">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-select-plus">
                            <label><?= GetMessage("TRAVELBOOKING_CHILDREN") ?></label>

                            <select class="form-control" name="travelbooking[children]">
                                <option <? if ((int) $_GET["travelbooking"]["children"] === 0): ?>selected<? endif ?> value="0"><?= GetMessage("TRAVELBOOKING_WITHOUT_CHILDREN") ?></option>
                                <option <? if ((int) $_GET["travelbooking"]["children"] === 1): ?>selected<? endif ?> value="1">1</option>
                                <option <? if ((int) $_GET["travelbooking"]["children"] === 2): ?>selected<? endif ?> value="2">2</option>
                                <option <? if ((int) $_GET["travelbooking"]["children"] === 3): ?>selected<? endif ?> value="3">3</option>
                                <option <? if ((int) $_GET["travelbooking"]["children"] === 4): ?>selected<? endif ?> value="4">4</option>
                            </select>
                            <div class="children-age-box hidden">
                                <div class="closer">&times;</div>
                                <? if (is_array($_GET["travelbooking"]["children_age"]) && !empty($_GET["travelbooking"]["children_age"])): ?>
                                    <? foreach ($_GET["travelbooking"]["children_age"] as $age): ?>
                                        <input class="select-age-box" type="hidden" name="travelbooking[children_age][]" value="<?= htmlspecialchars($age) ?>">
                                    <? endforeach ?>
                                <? endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 search-btn-box text-center">
                <span class="loading hidden"></span>
                <button class="search-btn btn btn-primary" type="submit"><?= GetMessage("TRAVELBOOKING_SEARCH_BTN") ?></button>
            </div>
        </div>
        
    </form>
</div>
<template id="children-age-template">
    <div class="select-age-box form-group">
        <label>Возраст ребёнка {{index}}</label>
        <select name="travelbooking[children_age][]" class="form-control">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <?/*<option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>*/?>
        </select>
    </div>
</template>
<script>
    var jsInnerSearchFormHighlightFor = <?= json_encode($arParams["HIGHLIGHT_FOR"])?>;
</script>