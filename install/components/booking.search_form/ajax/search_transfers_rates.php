<?php

/* 
 * Обработка запроса на поиск проживаний
 */

define('PUBLIC_AJAX_MODE', true);
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

header('Content-Type: application/json; charset=' . SITE_CHARSET);

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

$arRates = \travelsoft\booking\stores\Rates::get(array(
    "filter" => array("UF_NAME" => "%" .$_GET["q"]. "%", array("LOGIC" => "OR", array("UF_SERVICES_TYPE" => "transfer"), array("UF_SERVICES_TYPE" => "transferback"))),
    "select" => array("UF_NAME", "ID", "UF_SERVICES_TYPE")
));

$response = array();
if (!empty($arRates)) {
    foreach ($arRates as $arRate) {
        $response[] = array("type" => $arRate["UF_SERVICES_TYPE"], "text" => $arRate["UF_NAME"], "id" => $arRate["ID"]);
    }
}

echo json_encode($response);
