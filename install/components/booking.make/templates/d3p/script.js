
/**
 * Component booking.make
 * Template "d3p"
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */

$(document).ready(function () {

    var extData = (function (extData) {
        var data = {};
        for (var p in extData) {
            if (extData.hasOwnProperty(p)) {
                data[p] = extData[p];
            }
        }
        return data;
    })(window.jsBookingExtData);
    
    $.fn.datepicker.dates.RU = {
        days: extData.messages.calendar.days.split(", "),
        daysShort: extData.messages.calendar.days_short.split(", "),
        daysMin: extData.messages.calendar.days_min.split(", "),
        months: extData.messages.calendar.months.split(", "),
        monthsShort: extData.messages.calendar.months_short.split(", "),
        today: extData.messages.calendar.today,
        clear: extData.messages.calendar.clear,
        format: "dd.mm.yyyy",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 0
    };

    var defRequestData = {sessid: BX.bitrix_sessid()};

    /**
     * @param {$} $element
     * @param {Number} delta
     * @returns {undefined}
     */
    function scrollto($element, delta) {
        delta = delta || 0;
        $('html, body').animate({scrollTop: $element.offset().top - delta}, 500);
    }

    function initDatepicker() {
        $(".date-input").datepicker({
            todayHighlight: true,
            format: 'dd.mm.yyyy',
            autoclose: true,
            language: "RU"
        });
    }

    function initFindTourists() {

        $('input[name*="[UF_NAME]"]').each(function () {

            var $this = $(this);
            var index = $this.data("index");
            var suggestion__ = null;
            $this.typeahead({
                hint: true,
                highlight: true,
                minLength: 3,
                limit: 8
            }, {
                source: function (q, cb) {
                    return $.ajax({
                        dataType: 'json',
                        type: 'get',
                        url: extData.url + '?q=' + q + '&sessid=' + BX.bitrix_sessid() + "&client_id=" + extData.userId + "&action=get_tourists",
                        cache: false,
                        success: function (resp) {
                            var result = [];
                            $.each(resp, function (index, val) {
                                result.push({
                                    value: [val.UF_NAME, val.UF_SECOND_NAME, val.UF_LAST_NAME].join(" "),
                                    ID: val.ID,
                                    UF_NAME: val.UF_NAME,
                                    UF_SECOND_NAME: val.UF_SECOND_NAME,
                                    UF_LAST_NAME: val.UF_LAST_NAME,
                                    UF_LAT_NAME: val.UF_LAT_NAME,
                                    UF_LAT_LAST_NAME: val.UF_LAT_LAST_NAME,
                                    UF_MALE: val.UF_MALE,
                                    UF_BIRTHDATE: val.UF_BIRTHDATE,
                                    UF_PASS_SERIES: val.UF_PASS_SERIES,
                                    UF_PASS_NUMBER: val.UF_PASS_NUMBER,
                                    UF_PASS_ISSUED_BY: val.UF_PASS_ISSUED_BY,
                                    UF_PASS_DATE_ISSUE: val.UF_PASS_DATE_ISSUE
                                });
                            });
                            cb(result);
                        }
                    });
                },
                templates: {
                    empty: "<span style='padding:0 5px'>No Results Found</span>"
                }
            });
            $this.on('typeahead:selected', function (ev, suggestion) {

                var box = $(`#tourist_${index}`);

                suggestion__ = suggestion;
                box.append(`<input type="hidden" name="tourists[${index}][ID]" value=${suggestion.ID}>`);
                box.find(`input[name='tourists[${index}][UF_NAME]']`).val(suggestion.UF_NAME).trigger("change");
                box.find(`input[name='tourists[${index}][UF_SECOND_NAME]']`).val(suggestion.UF_SECOND_NAME);
                box.find(`input[name='tourists[${index}][UF_LAST_NAME]']`).val(suggestion.UF_LAST_NAME);
                box.find(`input[name='tourists[${index}][UF_LAT_NAME]']`).val(suggestion.UF_LAT_NAME);
                box.find(`input[name='tourists[${index}][UF_LAT_LAST_NAME]']`).val(suggestion.UF_LAT_LAST_NAME);
                box.find(`select[name='tourists[${index}][UF_MALE]']`).val(suggestion.UF_MALE).trigger("change");
                box.find(`input[name='tourists[${index}][UF_BIRTHDATE]']`).val(suggestion.UF_BIRTHDATE);
                box.find(`input[name='tourists[${index}][UF_PASS_SERIES]']`).val(suggestion.UF_PASS_SERIES);
                box.find(`input[name='tourists[${index}][UF_PASS_NUMBER]']`).val(suggestion.UF_PASS_NUMBER);
                box.find(`input[name='tourists[${index}][UF_PASS_ISSUED_BY]']`).val(suggestion.UF_PASS_ISSUED_BY);
                box.find(`input[name='tourists[${index}][UF_PASS_DATE_ISSUE]']`).val(suggestion.UF_PASS_DATE_ISSUE);
            });

            $this.on('blur', function () {

                if (suggestion__) {
                    $(`#tourist_${index}`).find(`input[name='tourists[${index}][UF_NAME]']`).val(suggestion__.UF_NAME);
                }

            });
        });

    }

    initDatepicker();

    $(document).on("click", ".fantom", function (e) {
        var $this = $(this);
        $this.next().removeClass("hidden");
        $this.find(".closer").removeClass("hidden");
        e.stopPropagation();
    });

    $(document).on("click", ".closer", function (e) {
        var $this = $(this);
        $this.addClass("hidden");
        $this.parent().next().addClass("hidden");
        e.stopPropagation();
    });

    $(document).on("click", ".link2basket-box", function (e) {
        e.stopPropagation();
    });

    $(document).on("click", "#tourists-box", function (e) {
        $(".closer").trigger("click");
    });

    $(".deleter").on("click", function () {

        var $this = $(this);

        $this.html("").addClass("loading");
        $.get(
                extData.url,
                $.extend({}, {position: $(this).data("position"), action: "delete_position"}, defRequestData),
                function (resp) {

                    if (resp.status === 1) {
                        extData.basketItemsCount--;
                        if (extData.basketItemsCount <= 0) {
                            $("#booking-box").html(extData.messages.empty_basket);
                        } else {
                            $this.parent().parent().remove();
                        }
                        scrollto($("#booking-box"));
                    } else {
                        $this.removeClass("loading").html("&times;");
                    }
                }
        ).fail(function () {
            $this.removeClass("loading").html("&times;");
        });

    });

    if (!extData.isAuth) {

        $("#toggle-context").on("click", function () {

            var $this = $(this);
            var form = $this.closest("form");
            var actionInput = form.find("input[name=action]");
            var button = form.find("button");
            var authFormTitle = $("#identification-form-title");
            $("#identification-errors-box").text("");
            if (actionInput.val() === "authorization") {
                actionInput.val("registration");
                $this.text(extData.messages.authorization);
                form.find("input[name=password]").parent().after($("#confirm_password_template").html());
                form.find("input[name=email]").parent().after($("#phone_template").html());
                button.text(extData.messages.registration_btn);
                authFormTitle.text(extData.messages.registration);
            } else {
                actionInput.val("authorization");
                $this.text(extData.messages.registration);
                form.find("input[name=confirm_password]").parent().remove();
                form.find("input[name=phone]").parent().remove();
                button.text(extData.messages.authorization_btn);
                authFormTitle.text(extData.messages.authorization);
            }
        });

        $("#identification-form").on("submit", function (e) {

            var $this = $(this);
            var formData = $this.serialize();
            var errorsBox = $("#identification-errors-box");

            var button = $this.find("button");

            var loader = $this.find(".loading");

            errorsBox.text("");

            button.addClass("hidden");
            loader.removeClass("hidden");
            $.post(extData.url, formData, function (resp) {
                if (resp.status === 3) {
                    errorsBox.text(extData.messages.authorization_fail);
                    scrollto($this);
                } else if (resp.status === 2) {
                    scrollto($this);
                    $this.replaceWith($("#ok_authorization_template").html());
                    extData.isAuth = true;
                    extData.userId = resp.user_id;
                    extData.isAvailBooking = resp.booking_is_avail;
                    if (resp.user_is_agent) {
                        $(".is-buyer-radio-box").remove();
                        $("#buyer-fields-box").remove();
                    } else {
                        $("input[name='buyer[PERSONAL_PHONE]']").val(resp.user_phone);
                    }
                    initFindTourists();
                } else if (resp.status === 5) {
                    errorsBox.text(extData.messages.email_already_exists);
                    scrollto($this);
                } else if (resp.status === 4) {
                    errorsBox.html(resp.message);
                    scrollto($this);
                } else if (resp.status === 6) {
                    scrollto($this);
                    $this.replaceWith($("#ok_registration_template").html());
                    extData.isAuth = true;
                    extData.userId = resp.user_id;
                    extData.isAvailBooking = resp.booking_is_avail;
                    if (resp.user_is_agent) {
                        $(".is-buyer-radio-box").remove();
                        $("#buyer-fields-box").remove();
                    } else {
                        $("input[name='buyer[PERSONAL_PHONE]']").val(resp.user_phone);
                    }
                } else if (resp.status === 7) {
                    errorsBox.html(extData.messages.wrong_phone);
                    scrollto($this);
                }
                button.removeClass("hidden");
                loader.addClass("hidden");
            }).fail(function () {
                button.removeClass("hidden");
                loader.addClass("hidden");
            });

            e.preventDefault();
        });
    } else {
        initFindTourists();
    }

    if (extData.peopleCount) {
        $("#add-tourist").on("click", function (e) {
            var $this = $(this);
            $this.closest("form").find("#tourists-box").append($("#tourists_template").html().split("{{index}}").join(extData.lastTouristIndex++));
            initDatepicker();
            extData.peopleCount--;
            if (extData.peopleCount <= 0) {
                $this.parent().parent().remove();
            }
            e.preventDefault();
        });
    }

    $("#booking-form").on("submit", function (e) {

        e.preventDefault();

        var $this = $(this);

        var button = $("#booking-btn");

        var loader = $this.find(".loading");

        $(".errors-box").each(function () {
            var $this = $(this);
            $this.text("");
            $this.addClass("hidden");
        });

        loader.removeClass("hidden");
        button.addClass("hidden");
        if (!extData.isAuth || !extData.isAvailBooking) {

            alert(!extData.isAuth ? extData.messages.booking_not_avail : extData.messages.booking_not_avail2);
            loader.addClass("hidden");
            button.removeClass("hidden");
            return;
        }
        
        $.post(extData.url, $this.serialize(), function (resp) {

            var errorBox;
            if (resp.result) {
                window.location = extData.voucher_detail_page.replace("{{voucher_id}}", resp.voucher_id);
            } else {
                loader.addClass("hidden");
                button.removeClass("hidden");
                if (typeof resp.errors === "object") {
                    if (typeof resp.errors.TOURISTS === "object" && resp.errors.TOURISTS) {

                        for (var i in resp.errors.TOURISTS) {
                            for (var fname in resp.errors.TOURISTS[i]) {
                                errorBox = $this.find(`[data-error-for=${fname}-${i}]`);
                                errorBox.text(extData.messages.err[resp.errors.TOURISTS[i][fname]]);
                            }

                        }
                    }
                    
                    if (typeof resp.errors.BUYER === "object" && resp.errors.BUYER) {

                        for (var fname in resp.errors.BUYER) {
                            errorBox = $this.find(`[data-error-for="${fname}"]`);
                            errorBox.text(extData.messages.err[resp.errors.BUYER[fname]]);

                        }
                    } 
                    
                    if (resp.errors.NOT_AVAIL_BASKET_ITEMS === true) {
                        alert(extData.messages.not_avail_basket_items);
                    }
                    
                    if (resp.errors.USER_NOT_AUTH === true) {
                        alert(extData.messages.booking_not_avail);
                    }
                    
                    if (resp.errors.BOOKING_NOT_AVAIL === true) {
                        alert(extData.messages.booking_not_avail2);
                    }

                    $(".errors-box").each(function () {
                        $(this).removeClass("hidden");
                    });

                    scrollto($(".errors-box"));
                }
            }

        });
    });
    
    $(".is-buyer-radio").on("click", function () {
        
        var index = $(this).val();
        $(`input[name="buyer[NAME]"]`).val($(`input[name="tourists[${index}][UF_NAME]"]`).val());
        $(`input[name="buyer[LAST_NAME]"]`).val($(`input[name="tourists[${index}][UF_LAST_NAME]"]`).val());
        $(`input[name="buyer[SECOND_NAME]"]`).val($(`input[name="tourists[${index}][UF_SECOND_NAME]"]`).val());
        $(`input[name="buyer[PERSONAL_BIRTHDAY]"]`).val($(`input[name="tourists[${index}][UF_BIRTHDATE]"]`).val());
        $(`input[name="buyer[UF_PASS_SERIES]"]`).val($(`input[name="tourists[${index}][UF_PASS_SERIES]"]`).val());
        $(`input[name="buyer[UF_PASS_NUMBER]"]`).val($(`input[name="tourists[${index}][UF_PASS_NUMBER]"]`).val());
        $(`input[name="buyer[UF_PASS_DATE_ISSUE]"]`).val($(`input[name="tourists[${index}][UF_PASS_DATE_ISSUE]"]`).val());
        $(`input[name="buyer[UF_PASS_ISSUED_BY]"]`).val($(`input[name="tourists[${index}][UF_PASS_ISSUED_BY]"]`).val());
    })
});
