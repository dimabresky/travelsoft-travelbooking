<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

if (empty($arResult["VOUCHERS"])):
    ?>
    <div class="row" id="voucher-list-box">
        <span id="empty-voucher-list-message"><?= GetMessage("TRAVELBOOKING_EMPTY_VOUCHER_LIST_MESSAGE") ?></span>
    </div>
    <? return;
endif; ?>
<div class="row" id="voucher-list-box">
    <table class="table table-striped table-responsive">
        <thead>
            <tr>
                <th><?= GetMessage("TRAVELBOOKING_VOUCHER_TITLE")?></th>
                <th class="hidden-xs"><?= GetMessage("TRAVELBOOKING_DATE_CREATE_TITLE")?></th>
                <th><?= GetMessage("TRAVELBOOKING_VOUCHER_COST_TITLE")?></th>
                <th class="hidden-xs"><?= ($arResult["IS_AGENT"] ? GetMessage("TRAVELBOOKING_AGENT_DISCOUNT_TITLE") : GetMessage("TRAVELBOOKING_DISCOUNT_TITLE"))?></th>
                <th class="hidden-xs"><?= GetMessage("TRAVELBOOKING_PAID_TITLE")?></th>
                <th><?= GetMessage("TRAVELBOOKING_TO_PAY_TITLE")?></th>
                <?if ($arResult["IS_ADMIN"]):?>
                <th><?= GetMessage("TRAVELBOOKING_CLIENT_TITLE")?></th>
                <?endif?>
                <th></th>
                <?if ($arResult["IS_ADMIN"]):?>
                <th></th>
                <?endif?>
            </tr>
        </thead>
        <tbody>
<? foreach ($arResult["VOUCHERS"] as $arr_voucher): ?>
                <tr>
                    <td><?= $arr_voucher["ID"] ?></td>
                    <td class="hidden-xs"><?= $arr_voucher["UF_DATE_CREATE"]->format(travelsoft\booking\Settings::DATE_FORMAT) ?></td>
                    <td><?= $arr_voucher["TOTAL_COST_WITHOUT_DISCOUNT_FORMATTED"] ?></td>
                    <td class="hidden-xs"><?= $arr_voucher["DISCOUNT_FORMATTED"] ?></td>
                    <td class="hidden-xs"><?= $arr_voucher["PAID_FORMATTED"] ?></td>
                    <td><?= $arr_voucher["TO_PAY_FORMATTED"] ?></td>
                    <?if ($arResult["IS_ADMIN"]):?>
                    <td><?= $arResult["CLIENTS"][$arr_voucher["UF_CLIENT"]]["FULL_NAME_WITH_EMAIL"] . "(".$arResult["CLIENTS"][$arr_voucher["UF_CLIENT"]]["PERSONAL_PHONE"].")"?></td>
                    <?endif?>
                    <td><a class="btn btn-primary" href="/personal/vouchers/detail.php?voucher_id=<?= $arr_voucher["ID"] ?>"><?= GetMessage("TRAVELBOOKING_DETAIL_TITLE")?></a></td>
                    <?if ($arResult["IS_ADMIN"]):?>
                    <td><a onclick="return confirm('<?= GetMessage("TRAVELBOOKING_DELETE_QUESTION")?>')" class="btn btn-primary" href="<?= $APPLICATION->GetCurPageParam("sessid=".bitrix_sessid()."&delete_voucher_id=".$arr_voucher["ID"], array("delete_voucher_id", "sessid"), false)?>"><?= GetMessage("TRAVELBOOKING_DELETE_TITLE")?></a></td>
                    <?endif?>
                </tr>
<? endforeach ?>
        </tbody>
    </table>
    <div id="pagination-box">
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:main.pagenavigation", "modern", array(
            "NAV_OBJECT" => $arResult["NAV"],
            "SEF_MODE" => "N",
                ), false
        );
        ?>
    </div>
</div>

