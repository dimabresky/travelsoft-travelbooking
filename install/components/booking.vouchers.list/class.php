<?php

/**
 * Компонент списка путевок
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class TravelsoftBookingVouchersList extends CBitrixComponent {

    /**
     * component body
     */
    public function executeComponent() {

        \Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

        try {
            
            $this->arResult["IS_AGENT"] = travelsoft\booking\adapters\User::isAgent();
            $this->arResult["IS_ADMIN"] = travelsoft\booking\adapters\User::isAdmin();
            
            if (check_bitrix_sessid() && $_REQUEST["delete_voucher_id"] > 0 && $this->arResult["IS_ADMIN"]) {
                travelsoft\booking\stores\Vouchers::delete($_REQUEST["delete_voucher_id"]);
                LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam("",array("delete_voucher_id", "sessid"), false));
            }
            
            $this->arResult["NAV"] = new \Bitrix\Main\UI\PageNavigation("page");
            $this->arResult["NAV"]->allowAllRecords(true)
                    ->setPageSize(10)
                    ->initFromUri();
            
            
            $arr_filter = array();
            if (!$this->arResult["IS_ADMIN"]) {
                $arr_filter = array("UF_CLIENT" => \travelsoft\booking\adapters\User::id());
            }
            
            $total_cnt = (int) travelsoft\booking\stores\Vouchers::get(array(
                'select' => array(new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(1)')),
                "filter" => $arr_filter), false)->fetch()["CNT"];
            
            $this->arResult["NAV"]->setRecordCount($total_cnt);
            
            $this->arResult["VOUCHERS"] = travelsoft\booking\stores\Vouchers::get(array(
                        "filter" => $arr_filter,
                        "order" => array("ID" => "DESC"),
                        "offset" => $this->arResult["NAV"]->getOffset(),
                        "limit" => $this->arResult["NAV"]->getLimit()
                            )
            );

            $this->arResult["CONVERTER"] = new travelsoft\booking\adapters\CurrencyConverter;

            if ($this->arResult["IS_ADMIN"]) {
                $this->arResult["CLIENTS"] = array();
                foreach ($this->arResult["VOUCHERS"] as $arr_voucher) {
                    if ($arr_voucher["UF_CLIENT"] > 0 && !isset($this->arResult["CLIENTS"][$arr_voucher["UF_CLIENT"]])) {
                        $this->arResult["CLIENTS"][$arr_voucher["UF_CLIENT"]] = current(travelsoft\booking\stores\Users::get(array(
                            "filter" => array("ID" => $arr_voucher["UF_CLIENT"])
                        )));
                    }
                }
            }
            
            $this->IncludeComponentTemplate();
        } catch (\Exception $e) {
            (new \travelsoft\booking\Logger)->write("Component booking.vouchers.detail: " . $e->getMessage());
            ShowError("System error.");
        }
    }

}
