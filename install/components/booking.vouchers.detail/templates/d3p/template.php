<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$APPLICATION->SetTitle(GetMessage("TRAVELBOOKING_VOUCHER_TITLE", array("#VOUCHER_ID#" => $arResult["VOUCHER"]["ID"])));
$APPLICATION->AddChainItem(GetMessage("TRAVELBOOKING_VOUCHER_TITLE", array("#VOUCHER_ID#" => $arResult["VOUCHER"]["ID"])));
$this->setFrameMode(true);

$discountTitle = travelsoft\booking\adapters\User::isAgent() ? GetMessage("TRAVELBOOKING_AGENT_DISCOUNT_TITLE") : GetMessage("TRAVELBOOKING_DISCOUNT_TITLE");
?>
<div class="container">
    <div class="row" id="voucher-box">
        <? if ($arResult["SHOW_OK_BOOKING_MESSAGE"]): ?>
            <div class="col-md-12">
                <span id="ok-booking-message">
                    <?= GetMessage("TRAVELBOOKING_OK_BOOKING_MESSAGE", array("#VOUCHER_ID#" => $arResult["VOUCHER"]["ID"])) ?>
                </span>
            </div>
        <? endif ?>
        <div class="col-md-12"><span id="bookings-title"><?= GetMessage("TRAVELBOOKING_SERVICES_LIST_TITLE") ?></span></div>
        <? foreach ($arResult["VOUCHER"]["BOOKINGS_DETAIL"] as $arr_booking):?>
            <div class="col-md-6 booking-box" id="booking-box-<?= randString(7) ?>">
                <ul class="service-flist">
                    <?if ($arr_booking["UF_EXCUR_TOUR_RATE"]):?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_TOUR_TITLE") ?></span>: <span class="fvalue"><?= $arr_booking["TOUR_NAME"] ?></span></li>
                    <?endif;?>
                    <? if (strlen($arr_booking["TRANSFER_NAME"]) > 0): ?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_TRAVEL_NAME") ?></span>: <span class="fvalue"><?= $arr_booking["TRANSFER_NAME"] ?></span></li>
                    <? endif ?>
                    <? if (strlen($arr_booking["PLACEMENT_NAME"]) > 0): ?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_PLACEMENT_NAME") ?></span>: <span class="fvalue"><?= $arr_booking["PLACEMENT_NAME"] ?></span></li>
                    <? endif ?>
                    <? if (strlen($arr_booking["ROOM_NAME"]) > 0): ?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_ROOM_NAME") ?></span>: <span class="fvalue"><?= $arr_booking["ROOM_NAME"] ?></span></li>
                    <? endif ?>
                    <? if (strlen($arr_booking["RATE_NAME"]) > 0): ?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_RATE_NAME") ?></span>: <span class="fvalue"><?= $arr_booking["RATE_NAME"] ?></span></li>
                    <? endif ?>
                    <? if ($arr_booking["UF_DATE_FROM_TRANS"]): ?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_DATE_FROM_TRAVEL_TITLE") ?></span>: <span class="fvalue"><?= $arr_booking["UF_DATE_FROM_TRANS"] ?></span></li>
                    <? endif ?>
                    <? if ($arr_booking["UF_DATE_FROM_PLACE"]): ?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_DATE_FROM_PLACE_TITLE") ?></span>: <span class="fvalue"><?= $arr_booking["UF_DATE_FROM_PLACE"] ?></span></li>
                    <? endif ?>
                    <? if ($arr_booking["UF_DATE_TO_PLACE"]): ?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_DATE_TO_PLACE_TITLE") ?></span>: <span class="fvalue"><?= $arr_booking["UF_DATE_TO_PLACE"] ?></span></li>
                    <? endif ?>
                    <? if ($arr_booking["UF_DATE_BACK_TRANS"]): ?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_DATE_BACK_TRAVEL_TITLE") ?></span>: <span class="fvalue"><?= $arr_booking["UF_DATE_BACK_TRANS"] ?></span></li>
                    <? endif ?>
                    <?if ($arr_booking["UF_EXCUR_TOUR_RATE"]):?>
                        <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_DATE_TOUR_TITLE") ?></span>: <span class="fvalue"><?= $arr_booking["UF_DATE_FROM"] ?></span></li>
                    <?endif;?>
                    <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_ADULTS_TITLE") ?></span>: <span class="fvalue"><?= $arr_booking["UF_ADULTS"] ?></span></li>
                    <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_CHILDREN_TITLE") ?></span>: <span class="fvalue"><?= $arr_booking["UF_CHILDREN"] ?></span></li>
                    <li><span class="fname"><?= GetMessage("TRAVELBOOKING_BOOKING_TOURISTS_TITLE") ?></span>: 
                        <span class="fvalue">
                            <?= $arResult["TOURISTS"] ?>
                        </span></li>
                </ul>
            </div>
        <? endforeach; ?>
        <div class="col-md-6">
            <ul id="total-box">
                <li>
                    <span >Статус</span>: 
                    <? if ($arResult["IS_ADMIN"] && $arResult["VOUCHER"]["UF_STATUS"] != travelsoft\booking\Settings::cancellationStatus()): ?>
                        <form id="change-status-form" action="<?= $APPLICATION->GetCurPage(false) ?>">
                            <?= bitrix_sessid_post() ?>
                            <input type="hidden" value="Y" name="change_status_request">
                            <input type="hidden" value="<?= $arResult["VOUCHER"]["ID"] ?>" name="voucher_id">

                            <select onchange="document.getElementById('change-status-form').submit();" name="status">
                                <? foreach ($arResult["STATUSES"] as $arr_status): ?>
                                    <option <? if ($arr_status["ID"] === $arResult["VOUCHER"]["STATUS"]["ID"]): ?>selected<? endif; ?> value="<?= $arr_status["ID"] ?>"><?= $arr_status["UF_NAME"] ?></option>
                                <? endforeach; ?>
                            </select>
                        </form>
                    <? else: ?>
                        <span><?= $arResult["VOUCHER"]["STATUS"]["UF_NAME"] ?></span>
                    <? endif ?>
                </li>
                <? if (!empty($arResult["DOCS_FOR_PRINT"]) || $arResult["IS_ADMIN"]): ?>
                    <li>
                        <span><?= GetMessage("TRAVELBOOKING_BOOKING_DOCUMENTS_TITLE") ?></span>: 
                        <? foreach ($arResult["DOCS_FOR_PRINT"] as $arr_doc) { ?>
                            <a target="__blank" href="<?= $APPLICATION->GetCurPageParam("print=" . $arr_doc["ID"], array("print"), false) ?>"><?= $arr_doc["UF_NAME"] ?></a>    
                        <? } ?>
                    </li>
                <? endif ?>
                <li>
                    <span><?= GetMessage("TRAVELBOOKING_BOOKING_TOTAL_TITLE") ?></span>: <span class="total-cost-value"><?= $arResult["VOUCHER"]["TOTAL_COST_WITHOUT_DISCOUNT_FORMATTED"] ?></span>
                </li>
                <li>
                    <span><?= $discountTitle ?></span>: <span class="total-cost-value"><?= $arResult["VOUCHER"]["DISCOUNT_FORMATTED"] ?></span>
                </li>
                <li>
                    <span><?= GetMessage("TRAVELBOOKING_PAID_TITLE") ?></span>: <span class="total-cost-value"><?= $arResult["VOUCHER"]["PAID_FORMATTED"] ?></span>
                </li>
                <li>
                    <span><?= GetMessage("TRAVELBOOKING_TO_PAY_TITLE") ?></span>: <span class="total-cost-value"><?= $arResult["VOUCHER"]["TO_PAY_FORMATTED"] ?></span>
                </li>
                <? if ($arResult["VOUCHER"]["UF_STATUS"] != travelsoft\booking\Settings::cancellationRequestStatus() &&
                        $arResult["VOUCHER"]["UF_STATUS"] != travelsoft\booking\Settings::cancellationStatus()):
                    ?>
    <? if (!$arResult["IS_ADMIN"]): ?>
                        <li>
                            <form action="<?= $APPLICATION->GetCurPage(false) ?>">
        <?= bitrix_sessid_post() ?>
                                <input type="hidden" value="Y" name="cancellation_request">
                                <input type="hidden" value="<?= $arResult["VOUCHER"]["ID"] ?>" name="voucher_id">
                                <button class="btn btn-primary"><?= GetMessage("TRAVELBOOKING_BOOKING_CANCEL_TITLE") ?></button>
                            </form>

                        </li>
                    <? endif ?>
<? endif ?>
            </ul>
        </div>
    </div>
</div>