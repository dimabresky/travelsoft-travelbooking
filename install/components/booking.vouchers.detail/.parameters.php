<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

$arr_docs = array();
foreach (\travelsoft\booking\stores\Documents::get() as $arr_doc) {
    $arr_docs[$arr_doc["ID"]] = $arr_doc["UF_NAME"];
}

$arComponentParameters = array(
    "PARAMETERS" => array(
        "VOUCHER_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "ID путевки",
            "TYPE" => "STRING",
            "DEFAULT" => '={$_REQUEST["voucher_id"]}'
        ),
        "DOCS_FOR_AGENTS" => array(
            "PARENT" => "BASE",
            "NAME" => "Выводить на печать документы для агентов",
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arr_docs
        ),
        "DOCS_FOR_CLIENTS" => array(
            "PARENT" => "BASE",
            "NAME" => "Выводить на печать документы для клиентов",
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arr_docs
        ),
    )
);

?>