<?php

return array(
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_ATT_NUMBER",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Номер доверенности (например №18 от 18.04.2017)',
            'en' => 'Att. number',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Номер доверенности (например №18 от 18.04.2017)',
            'en' => 'Att. number',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Номер доверенности (например №18 от 18.04.2017)',
            'en' => 'Att. number',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Номер доверенности (например №18 от 18.04.2017)" ',
            'en' => 'An error in completing the field "Att. number"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        )
    ),
);
