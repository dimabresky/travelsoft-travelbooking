<?php

return array(
    "NAME" => "Бронирование туристической услуги на сайте #SITE_NAME#",
    "SUBJECT" => "Бронирование туристической услуги на сайте #SITE_NAME#",
    "MESSAGE" => 'На сайте забронирована <a href="https://#SERVER_NAME#/bitrix/admin/travelsoft_crm_booking_voucher_edit.php?ID=#VOUCHER_ID#&lang=ru">туристическая услуга</a>.
<br>
<br>
Информация по заказу:<br>
Клиент: #CLIENT#<br>
Телефон: #PHONE#<br>
Проезд: #TRANSFER#<br>
Размещение: #PLACEMENT#<br>
Номер: #ROOM#<br>
Тариф: #RATE#<br>
Выезд туда: #DATE_FROM_TRANSFER#<br>
Заселение: #DATE_FROM_PLACEMENT#<br>
Выселение: #DATE_TO_PLACEMENT#<br>
Выезд обратно: #DATE_BACK_TRANSFER#<br>
Кол-во: #ADULTS#<br>
Кол-во: #CHILDREN#<br>
Возраст детей: #CHILDREN_AGE#<br>
Туристы: #TOURISTS#<br>
Стоимость: #COST#
Скидка: #DISCOUNT#
К оплате: #TO_PAY#
<br>---------------------------------------------------------------------<br>
Сообщение сгенерировано автоматически.',
    "OPTION_PARAMETER" => "MANAGER_BOOKING_MAIL_ID",
    "OPTION_NAME" => "Письмо для бронирование туристической услуги на сайте для менеджеров"
    
);
