<?php

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class travelsoft_travelbooking extends CModule {

    public $MODULE_ID = "travelsoft.travelbooking";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = "N";
    public $namespaceFolder = "travelsoft";
    public $componentsList = array(
        "booking.offers",
        "booking.search_form",
        "booking.make",
        "booking.vouchers.detail",
        "booking.vouchers.list",
    );
    public $adminFilesList = array(
        "travelsoft_crm_booking_add_prices.php",
        "travelsoft_crm_booking_cash_desks_list.php",
        "travelsoft_crm_booking_cash_desk_edit.php",
        "travelsoft_crm_booking_client_edit.php",
        "travelsoft_crm_booking_clients_list.php",
        "travelsoft_crm_booking_discount_edit.php",
        "travelsoft_crm_booking_discounts.php",
        "travelsoft_crm_booking_document_edit.php",
        "travelsoft_crm_booking_documents.php",
        "travelsoft_crm_booking_make_doc.php",
        "travelsoft_crm_booking_booking_edit.php",
        "travelsoft_crm_booking_bookings_list.php",
        "travelsoft_crm_booking_payment_history_edit.php",
        "travelsoft_crm_booking_payment_history_list.php",
        "travelsoft_crm_booking_payment_type_edit.php",
        "travelsoft_crm_booking_payments_types_list.php",
        "travelsoft_crm_booking_tourist_edit.php",
        "travelsoft_crm_booking_tourists_list.php",
        "travelsoft_crm_booking_rooms.php",
        "travelsoft_crm_booking_room_edit.php",
        "travelsoft_crm_booking_tourservices.php",
        "travelsoft_crm_booking_tourservice_edit.php",
        "travelsoft_crm_booking_calculation_types_list.php",
        "travelsoft_crm_booking_calculation_types_edit.php",
        "travelsoft_crm_booking_price_types_list.php",
        "travelsoft_crm_booking_price_types_edit.php",
        "travelsoft_crm_booking_rates_list.php",
        "travelsoft_crm_booking_rates_edit.php",
        "travelsoft_crm_booking_markup_list.php",
        "travelsoft_crm_booking_markup_edit.php",
        "travelsoft_crm_booking_package_tour_list.php",
        "travelsoft_crm_booking_package_tour_edit.php",
        "travelsoft_crm_booking_voucher_edit.php",
        "travelsoft_crm_booking_voucher_list.php",
        "travelsoft_crm_booking_buses_list.php",
        "travelsoft_crm_booking_bus_edit.php",
        "travelsoft_crm_booking_excursion_tour_list.php",
        "travelsoft_crm_booking_excursion_tour_edit.php"
    );
    public $highloadblocksFiles = array();
    public $emailsFiles = array();
    public $eventType = "TRAVELSOFT_TRAVELBOOKING";

    function __construct() {
        $arModuleVersion = array();
        $path_ = str_replace("\\", "/", __FILE__);
        $path = substr($path_, 0, strlen($path_) - strlen("/index.php"));
        include($path . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "Бронирование туристических услуг";
        $this->MODULE_DESCRIPTION = "Операторская система бронирования туристических услуг";
        $this->PARTNER_NAME = "dimabresky";
        $this->PARTNER_URI = "https://github.com/dimabresky/";

        Loader::includeModule('iblock');
        Loader::includeModule("highloadblock");
        $this->__loadHigloadblockFiles();
        $this->__loadMailFiles();
        set_time_limit(0);
    }

    public function copyFiles() {
        
        foreach ($this->adminFilesList as $file) {
            CopyDirFiles(
                    $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/admin/" . $file, $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/" . $file, true, true
            );
        }
        
        foreach ($this->componentsList as $componentName) {
            CopyDirFiles(
                    $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/components/" . $componentName, $_SERVER["DOCUMENT_ROOT"] . "/local/components/" . $this->namespaceFolder . "/" . $componentName, true, true
            );
        }
    }

    public function deleteFiles() {
        foreach ($this->adminFilesList as $file) {
            DeleteDirFilesEx("/bitrix/admin/" . $file);
        }
        foreach ($this->componentsList as $componentName) {
            DeleteDirFilesEx("/local/components/" . $this->namespaceFolder . "/" . $componentName);
        }
        if (!glob($_SERVER["DOCUMENT_ROOT"] . "/local/components/" . $this->namespaceFolder . "/*")) {
            DeleteDirFilesEx("/local/components/" . $this->namespaceFolder);
        }
        return true;
    }

    public function DoInstall() {
        try {

            # проверка зависимостей модуля
            if (!ModuleManager::isModuleInstalled("travelsoft.currency")) {
                $errors[] = "Для работы модуля необходимо наличие установленного модуля <a target='_blank' href='https://github.com/dimabresky/travelsoft.currency'>travelsoft.currency</a>";
            }

            if (isset($errors) && !empty($errors)) {
                throw new Exception(implode("<br>", $errors));
            }

            # регистрируем модуль
            ModuleManager::registerModule($this->MODULE_ID);

            # создание higloadblock модуля
            $this->createHighloadblockTables();

            # создание почтовых сообщений
            $this->createMailMessages();
            
            # добавление полей для менеджера
            $this->addManagerFields();
            
            # добавление полей для агента
            $this->addAgentFields();
            
            # добавление полей для клиента
            $this->addClientFields();
            
            # добавление группы "Клиенты"
            $this->addClientGroup();
            
            # добавление группы "Клиенты" в группу для регистрации по-умолчанию
            $this->addDefaultClientGroup();
            
            # добавление группы "Агенты"
            $this->addAgentGroup();
            
            # добавление группы "Менеджер"
            $this->addManagerGroup();
            
            # добавление группы "Кассир"
            $this->addCasherGroup();
            
            # копирование файлов
            $this->copyFiles();
            
            # добавление зависимостей модуля
            $this->addModuleDependencies();
            
            # добавление параметров выбора для модуля
            $this->addOptions();
        } catch (Exception $ex) {

            $GLOBALS["APPLICATION"]->ThrowException($ex->getMessage());

            $this->DoUninstall();

            return false;
        }

        return true;
    }

    public function DoUninstall() {
        
        # удаляем зависимости модуля
        $this->deleteModuleDependencies();
        
        # удаление почтовых сообщений
        $this->deleteMailMessages();

        # удаление таблиц higloadblock
        $this->deleteHighloadblockTables();
        
        # удаление полей для менеджера
        $this->deleteManagerFields();
        
        # удаление полей для агента
        $this->deleteAgentFields();
        
        # удаление полей для клиента
        $this->deleteClientFields();
        
        # удаление группы "Клиенты" из группы для регистрации по-умолчанию
        $this->deleteDefaultClientGroup();
        
        # удаление группы "Клиенты"
        $this->deleteClientGroup();

        # удаление группы "Агенты"
        $this->deleteAgentGroup();
        
        # удаление группы "Менеджер"
        $this->deleteManagerGroup();
        
        # удаление группы "Кассир"
        $this->deleteCasherGroup();
        
        # удаление файлов
        $this->deleteFiles();

        # удаление параметров модуля
        $this->deleteOptions();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        return true;
    }
    
    public function addModuleDependencies () {
        
        RegisterModuleDependences("main", "OnBuildGlobalMenu", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "addGlobalAdminMenuItem");
        RegisterModuleDependences("main", "OnAfterUserRegister", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterUserRegister");
        RegisterModuleDependences("main", "OnBeforeUserUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onBeforeUserUpdate");
        RegisterModuleDependences("main", "OnAfterUserUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterUserUpdate");
        RegisterModuleDependences("", "TSVOUCHERSOnAfterAdd", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterVoucherAdd");
        RegisterModuleDependences("", "TSVOUCHERSOnBeforeUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onBeforeVoucherUpdate");
        RegisterModuleDependences("", "TSVOUCHERSOnAfterUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterVoucherUpdate");
        RegisterModuleDependences("", "TSVOUCHERSOnBeforeDelete", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onBeforeVoucherDelete");
        RegisterModuleDependences("", "TSVOUCHERSOnAfterDelete", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterVoucherDelete");
        
    }
    
    public function deleteModuleDependencies () {
        
        UnRegisterModuleDependences("main", "OnBuildGlobalMenu", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "addGlobalAdminMenuItem");
        UnRegisterModuleDependences("main", "OnAfterUserRegister", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterUserRegister");
        UnRegisterModuleDependences("main", "OnBeforeUserUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onBeforeUserUpdate");
        UnRegisterModuleDependences("main", "OnAfterUserUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterUserUpdate");
        UnRegisterModuleDependences("", "TSVOUCHERSOnAfterAdd", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterVoucherAdd");
        UnRegisterModuleDependences("", "TSVOUCHERSOnBeforeUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onBeforeVoucherUpdate");
        UnRegisterModuleDependences("", "TSVOUCHERSOnAfterUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterVoucherUpdate");
        UnRegisterModuleDependences("", "TSVOUCHERSOnBeforeDelete", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onBeforeVoucherDelete");
        UnRegisterModuleDependences("", "TSVOUCHERSOnAfterDelete", $this->MODULE_ID, "\\travelsoft\\booking\\EventsHandlers", "onAfterVoucherDelete");
    }
    
    public function addDefaultClientGroup () {
        
        $arr_def_groups = (array)explode(",", \Bitrix\Main\Config\Option::get("main", "new_user_registration_def_group"));
        $arr_def_groups[] = Option::get($this->MODULE_ID, "CLIENT_GROUP_ID");
        \Bitrix\Main\Config\Option::set("main", "new_user_registration_def_group", implode(",", $arr_def_groups));
    }
    
    public function deleteDefaultClientGroup () {
        
        $arr_def_groups = (array)explode(",", \Bitrix\Main\Config\Option::get("main", "new_user_registration_def_group"));
        $client_group_id = Option::get($this->MODULE_ID, "CLIENT_GROUP_ID");
        if (in_array($client_group_id, $arr_def_groups)) {
            $key = array_search($client_group_id, $arr_def_groups);
            if ($key !== false) {
                unset($arr_def_groups[$key]);
            }
        }
        \Bitrix\Main\Config\Option::set("main", "new_user_registration_def_group", implode(",", $arr_def_groups));
    }
    
    public function addOptions() {
        Option::set($this->MODULE_ID, "TRAVEL_STORAGE_ID");
        Option::set($this->MODULE_ID, "EXCURSIONS_TOURS_IB_STORAGE_ID");
        Option::set($this->MODULE_ID, "PLACEMENTS_STORAGE_ID");
        Option::set($this->MODULE_ID, "FOOD_STORAGE_ID");
        Option::set($this->MODULE_ID, "STATUS_ID_FOR_ORDER_CREATION");
        Option::set($this->MODULE_ID, "STATUS_ID_FOR_ORDER_CANCELLATION");
        Option::set($this->MODULE_ID, "STATUS_ID_FOR_ORDER_REQUEST_CANCELLATION");
        Option::set($this->MODULE_ID, "EMAIL_FOR_NOTIFICATION");
        Option::set($this->MODULE_ID, "ACCOUNTING_CURRENCY", "USD");
        Option::set($this->MODULE_ID, "VOUCHER_CURRENCY", "BYN");
        Option::set($this->MODULE_ID, "SINGLE_OCCUPANCY_PRICE_TYPE_ID", "5");
    }

    public function deleteOptions() {
        Option::delete($this->MODULE_ID, array("name" => "TRAVEL_STORAGE_ID"));
        Option::delete($this->MODULE_ID, array("name" => "EXCURSIONS_TOURS_IB_STORAGE_ID"));
        Option::delete($this->MODULE_ID, array("name" => "PLACEMENTS_STORAGE_ID"));
        Option::delete($this->MODULE_ID, array("name" => "FOOD_STORAGE_ID"));
        Option::delete($this->MODULE_ID, array("name" => "STATUS_ID_FOR_ORDER_CREATION"));
        Option::delete($this->MODULE_ID, array("name" => "STATUS_ID_FOR_ORDER_CANCELLATION"));
        Option::delete($this->MODULE_ID, array("name" => "STATUS_ID_FOR_ORDER_REQUEST_CANCELLATION"));
        Option::delete($this->MODULE_ID, array("name" => "ACCOUNTING_CURRENCY"));
        Option::delete($this->MODULE_ID, array("name" => "VOUCHER_CURRENCY"));
        Option::delete($this->MODULE_ID, array("name" => "SINGLE_OCCUPANCY_PRICE_TYPE_ID"));
    }
    
    public function deleteManagerFields() {

        $arr_fields = include "manager_fields.php";

        $oUserTypeEntity = new CUserTypeEntity();

        $arFilter = array("ENTITY_ID" => "USER");

        foreach ($arr_fields as $arr_field) {

            $arFilter["FIELD_NAME"][] = $arr_field["FIELD_NAME"];
        }

        $dbList = $oUserTypeEntity->GetList(array($by => $order), $arFilter);
        while ($arField = $dbList->Fetch()) {
            $oUserTypeEntity->Delete($arField["ID"]);
        }
    }
    
    public function deleteAgentFields() {

        $arr_fields = include "agent_fields.php";

        $oUserTypeEntity = new CUserTypeEntity();

        $arFilter = array("ENTITY_ID" => "USER");

        foreach ($arr_fields as $arr_field) {

            $arFilter["FIELD_NAME"][] = $arr_field["FIELD_NAME"];
        }

        $dbList = $oUserTypeEntity->GetList(array($by => $order), $arFilter);
        while ($arField = $dbList->Fetch()) {
            $oUserTypeEntity->Delete($arField["ID"]);
        }
    }
    
    public function deleteClientFields() {

        $arr_fields = include "client_fields.php";

        $oUserTypeEntity = new CUserTypeEntity();

        $arFilter = array("ENTITY_ID" => "USER");

        foreach ($arr_fields as $arr_field) {

            $arFilter["FIELD_NAME"][] = $arr_field["FIELD_NAME"];
        }

        $dbList = $oUserTypeEntity->GetList(array($by => $order), $arFilter);
        while ($arField = $dbList->Fetch()) {
            $oUserTypeEntity->Delete($arField["ID"]);
        }
    }
    
    public function addManagerFields() {

        $arr_fields = include "manager_fields.php";

        $oUserTypeEntity = new CUserTypeEntity();

        foreach ($arr_fields as $arr_field) {

            if (!$oUserTypeEntity->Add($arr_field)) {
                throw new Exception("Возникла ошибка при добавлении свойства " . $arr_field["ENTITY_ID"] . "[" . $arr_field["FIELD_NAME"] . "]" . $oUserTypeEntity->LAST_ERROR);
            }
        }
    }
    
    public function addAgentFields() {

        $arr_fields = include "agent_fields.php";

        $oUserTypeEntity = new CUserTypeEntity();

        foreach ($arr_fields as $arr_field) {

            if (!$oUserTypeEntity->Add($arr_field)) {
                throw new Exception("Возникла ошибка при добавлении свойства " . $arr_field["ENTITY_ID"] . "[" . $arr_field["FIELD_NAME"] . "]" . $oUserTypeEntity->LAST_ERROR);
            }
        }
    }
    
    public function addClientFields() {

        $arr_fields = include "client_fields.php";

        $oUserTypeEntity = new CUserTypeEntity();

        foreach ($arr_fields as $arr_field) {

            if (!$oUserTypeEntity->Add($arr_field)) {
                throw new Exception("Возникла ошибка при добавлении свойства " . $arr_field["ENTITY_ID"] . "[" . $arr_field["FIELD_NAME"] . "]" . $oUserTypeEntity->LAST_ERROR);
            }
        }
    }

    public function addClientGroup() {
 
        Option::set($this->MODULE_ID, "CLIENT_GROUP_ID", $this->__addGroup(array(
                    "NAME" => "Клиент",
                    "STRING_ID" => "client"
        )));
    }

    public function deleteClientGroup() {
        
        $this->__deleteGroup(Option::get($this->MODULE_ID, "CLIENT_GROUP_ID"));
        Option::delete($this->MODULE_ID, array("name" => "CLIENT_GROUP_ID"));
    }
    
    public function addManagerGroup () {
        
        Option::set($this->MODULE_ID, "MANAGER_GROUP_ID", $this->__addGroup(array(
                    "NAME" => "Менеджер",
                    "STRING_ID" => "manager"
        )));
    }
    
    public function deleteManagerGroup () {
        $this->__deleteGroup(Option::get($this->MODULE_ID, "MANAGER_GROUP_ID"));
        Option::delete($this->MODULE_ID, array("name" => "MANAGER_GROUP_ID"));
    }
    
    public function addCasherGroup () {
        
        Option::set($this->MODULE_ID, "CASHER_GROUP_ID", $this->__addGroup(array(
                    "NAME" => "Кассир",
                    "STRING_ID" => "casher"
        )));
    }
    
    public function deleteCasherGroup () {
        $this->__deleteGroup(Option::get($this->MODULE_ID, "CASHER_GROUP_ID"));
        Option::delete($this->MODULE_ID, array("name" => "CASHER_GROUP_ID"));
    }

    public function addAgentGroup() {
        
        Option::set($this->MODULE_ID, "AGENT_GROUP_ID", $this->__addGroup(array(
                    "NAME" => "Агент",
                    "STRING_ID" => "agent"
        )));
        
    }

    public function deleteAgentGroup() {
        $this->__deleteGroup(Option::get($this->MODULE_ID, "AGENT_GROUP_ID"));
        Option::delete($this->MODULE_ID, array("name" => "AGENT_GROUP_ID"));
    }

    public function createHighloadblockTables() {

        foreach ($this->highloadblocksFiles as $file) {

            $arr = include "highloadblocks/" . $file;

            $result = Bitrix\Highloadblock\HighloadBlockTable::add(array(
                        'NAME' => $arr["table_data"]["NAME"],
                        'TABLE_NAME' => $arr["table"],
//                        'LANGS' => $arr["table_data"]["LANGS"]
            ));

            if (!$result->isSuccess()) {
                throw new Exception($arr["table_data"]['ERR'] . "<br>" . implode("<br>", (array) $result->getErrorMessages()));
            }

            $table_id = $result->getId();

            Option::set($this->MODULE_ID, $arr["table_data"]["OPTION_PARAMETER"], $table_id);

            $arr_fields = $arr["fields"];

            $oUserTypeEntity = new CUserTypeEntity();

            foreach ($arr_fields as $arr_field) {

                $arr_field["ENTITY_ID"] = str_replace("{{table_id}}", $table_id, $arr_field["ENTITY_ID"]);

                if (!$oUserTypeEntity->Add($arr_field)) {
                    throw new Exception("Возникла ошибка при добавлении свойства " . $arr_field["ENTITY_ID"] . "[" . $arr_field["FIELD_NAME"] . "]" . $oUserTypeEntity->LAST_ERROR);
                }
            }
            
            if (isset($arr["items"]) && !empty($arr["items"])) {
                
                $entity   = Bitrix\Highloadblock\HighloadBlockTable::compileEntity( 
                        Bitrix\Highloadblock\HighloadBlockTable::getById($table_id)->fetch());
                $class = $entity->getDataClass();
                foreach ($arr["items"] as $item) {
                    $class::add($item);
                }
            }
        }
    }

    public function deleteHighloadblockTables() {

        foreach ($this->highloadblocksFiles as $file) {

            $arr = include "highloadblocks/" . $file;

            $table_id = Option::get($this->MODULE_ID, $arr["table_data"]["OPTION_PARAMETER"]);
            if ($table_id > 0) {
                Bitrix\Highloadblock\HighloadBlockTable::delete($table_id);
            }
            Option::delete($this->MODULE_ID, array("name" => $arr["table_data"]["OPTION_PARAMETER"]));
        }
    }

    public function createMailMessages() {

        $et = new CEventType;
        if (!$et->Add(array(
                    "LID" => "ru",
                    "EVENT_NAME" => $this->eventType,
                    "NAME" => "Сообщения модуля бронирования туристических услуг",
                    "DESCRIPTION" => ""
                ))
        ) {
            throw new Exception($et->LAST_ERROR);
        }

        $arFields = array(
            "ACTIVE" => "Y",
            "EVENT_NAME" => $this->eventType,
            "LID" => $this->getSiteId(),
            "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
            "EMAIL_TO" => "#EMAIL_TO#",
            "BODY_TYPE" => "html",
            "BCC" => '',
            "CC" => '',
            "REPLY_TO" => '',
            "IN_REPLY_TO" => '',
            "PRIORITY" => '',
            "FIELD1_NAME" => '',
            "FIELD1_VALUE" => '',
            "FIELD2_NAME" => '',
            "FIELD2_VALUE" => '',
            "SITE_TEMPLATE_ID" => '',
            "ADDITIONAL_FIELD" => array(),
            "LANGUAGE_ID" => ''
        );

        $emess = new CEventMessage;
        foreach ($this->emailsFiles as $file) {

            $arr = include "emails/" . $file;

            $arFields["MESSAGE"] = $arr["MESSAGE"];
            $arFields["SUBJECT"] = $arr["SUBJECT"];

            if (!($id = $emess->Add($arFields))) {
                throw new Exception($emess->LAST_ERROR);
            }

            Option::set($this->MODULE_ID, $arr["OPTION_PARAMETER"], $id);
        }
    }

    public function deleteMailMessages() {

        $emess = new CEventMessage;
        $dbMess = $emess->GetList($by = "site_id", $order = "desc", array("TYPE_ID" => $this->eventType));
        while ($mess = $dbMess->Fetch()) {
            $emess->Delete($mess['ID']);
        }
        $et = new CEventType;
        $et->Delete($this->eventType);


        foreach ($this->emailsFiles as $file) {

            $arr = include "emails/" . $file;

            Option::delete($this->MODULE_ID, array('name' => $arr["OPTION_PARAMETER"]));
        }
    }

    public function getSiteId() {

        static $arSites = array();

        if (!empty($arSites)) {
            return $arSites;
        }

        $dbSites = CSite::GetList($by = "sort", $order = "asc");

        while ($arSite = $dbSites->Fetch()) {
            $arSites[] = $arSite['ID'];
        }

        return $arSites;
    }

    private function __loadHigloadblockFiles() {

        $this->highloadblocksFiles = $this->__loadFiles("highloadblocks");
    }

    private function __loadMailFiles() {

        $this->emailsFiles = $this->__loadFiles("emails");
    }

    private function __loadFiles($dirName) {

        $directory = __DIR__ . "/" . $dirName;
        return array_diff(scandir($directory), array('..', '.'));
    }
    
    private function __addGroup ($arFields = array()) {

         $group = new CGroup;
         $arFields__ = Array(
            "ACTIVE" => "Y",
            "C_SORT" => 100
        );
         
        $NEW_GROUP_ID = $group->Add(array_merge($arFields__, $arFields));
        if (strlen($group->LAST_ERROR) > 0) {
            throw new Exception($group->LAST_ERROR);
        }
        
        return $NEW_GROUP_ID;
    }
    
    private function __deleteGroup ($id) {
        
        $group = new CGroup;
        $group->Delete($id);
    }
}
